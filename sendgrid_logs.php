<?php
$logFolder = "sendgridLogs";

//Check if directory exists. If not, create it recursively
function checkDir($dir){
	if(!file_exists($dir)){
		mkdir($dir, 0700, true); //Create Directory recursively
	}
}

//We loop through PriaryCache and dump all items into their log files.
function dumpCache($primaryCache){
	GLOBAL $logFolder;
	foreach ($primaryCache as $path => $cache) {
		// $logFolderPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . $logFolder
		// 									. DIRECTORY_SEPARATOR . $path);

		$logFolderPath = __DIR__ . DIRECTORY_SEPARATOR . $logFolder
											. DIRECTORY_SEPARATOR . $path;

		checkDir($logFolderPath); //Ensure the necessary path is created to the logs.

		//Get current summary contents or create new array if none exists.
		if(!file_exists($logFolderPath."/summary.log")){
			$summary = array();
		}else{
			$summary = file_get_contents($logFolderPath."/summary.log");
			$summary = json_decode($summary, true);
		}
		/*====================SUMMARY.LOG===================================*/
		//Run checks to see if each item in array exists in summary first.
		//After we get to the last level in array, either increment the event
		//count or create it. We start by checking if campaign element exists.
		foreach ($cache["summary"] as $campaignName => $campaignInfo) {
			if(!array_key_exists($campaignName, $summary)){
				$summary[$campaignName] = array();
			}
			//Check if this date already exists in summary.log
			//There should usually always be one date in each batch...
			foreach ($campaignInfo as $date => $events) {
				if(!array_key_exists($date, $summary[$campaignName])){
					$summary[$campaignName][$date] = array();
				}
				//Check if this event already exists in summary.log
				foreach ($events as $eventName => $eventCount) {
					if(!array_key_exists($eventName, $summary[$campaignName][$date])){
						$summary[$campaignName][$date][$eventName] = $eventCount;
					}else{
						$summary[$campaignName][$date][$eventName] += $eventCount;
					}
				}
			}
		}
		/*====================END: SUMMARY.LOG===============================*/

		/*====================Details.LOG===================================*/
		$detailedLogFileHandeler = fopen($logFolderPath.'/detailedLog.log', 'a+');
		if($detailedLogFileHandeler)
		{
			//Get rid of the '{' and '}'
			$detailedLogItems = substr(json_encode($cache["details"]), 1, -1);
			fwrite($detailedLogFileHandeler, $detailedLogItems.", \r\n");
			fclose($detailedLogFileHandeler);
		}
		/*====================END: Details.LOG===============================*/

		//Write to Summary log
		$summaryFileHandler = fopen($logFolderPath.'/summary.log', 'w'); //Needs to be w so that we clear off the file and write back to it again.
		fwrite($summaryFileHandler, json_encode($summary, true));
		fclose($summaryFileHandler);
	}
}

date_default_timezone_set ("Pacific/Auckland");

//Prodcution source:
$rawSendGridItems = file_get_contents("php://input");

//DEBUG--comment out.
//$rawSendGridItems = file_get_contents("sendgdOrigins/logs.log");


$threadLockerFile = fopen("!sendGridLockFile_DoNotDelete.php", 'a+');
$processComplete = false;

while(!$processComplete){
	if(flock($threadLockerFile, LOCK_EX)) {
		//File locked successfully
		$unsubscribeLinkSubstring = "page=unsubscribeUser";
		//Events to add to Detailed Log.
		$sendGridItemPropertiesToTrack = array("client", "ip", "subject", "campaign", "event", "sg_event_id", "email", "timestamp", "timestamp", "url");

		$sendGridJsonItems = json_decode($rawSendGridItems);
		if(count($sendGridJsonItems) > 0){
                  //record the correct number of clicks???
			//=============Primary Cache Summary: ===========================================\\
			//a) PrimaryCache is an ARRAY - each child will have a name.
				//The name will be the complete path to the log.
			//b) Inside the array we will have summary and details array();
			//c) Each summary array will have inner arrays containing campaigns
			//d) Each campaign array will have an array containing dates
			//e) Each date will contain "event":numberOfEvents type of format
			//f) Will call the dumpCache() only once.
			$primaryCache = array();
			$currentMonthYearDirName = date("M_Y");
			$currentDay = date("d");

			//Start looping through each item in the sendgrid batch
			foreach ($sendGridJsonItems as $sendGridJsonItem) {
				//Client Check
				if(!property_exists($sendGridJsonItem, "client") || $sendGridJsonItem->client == ""){
					$sendGridJsonItem->client = "noClientDetermined";
				}
				if(!property_exists($sendGridJsonItem, "campaign") || $sendGridJsonItem->campaign == ""){
					$sendGridJsonItem->campaign = "NONE";
				}

				//Identify what the name of the array item should be.
				//The name of the array is also the 'path' to the log
				$primaryCacheItemName = "$sendGridJsonItem->client" . DIRECTORY_SEPARATOR . "$currentMonthYearDirName";
				//If the path does not yet exist we create a new one.
				if(!array_key_exists($primaryCacheItemName, $primaryCache)){
					$primaryCache[$primaryCacheItemName] = array();
					$primaryCache[$primaryCacheItemName]["summary"] = array();
					$primaryCache[$primaryCacheItemName]["details"] = array();
				}

				//========Detail.log accumulation starts ============//
				//logItem represents one item in the details.log
				$logItem = new StdClass;
				$bodyHasContent = false;
				//Extract only the properties we need from the Sendgrid Item to
				//put into details.log item.
				foreach ($sendGridItemPropertiesToTrack as $property) {
					if(property_exists($sendGridJsonItem, $property)){
						$logItem->{$property} = $sendGridJsonItem->{$property};
						$bodyHasContent = true;
					}
				}
				if($bodyHasContent){
					//Add the detailed log item to the cache...
					array_push($primaryCache[$primaryCacheItemName]["details"], $logItem);
				}
				//========Detail.log work ends here ============//

				//========Summary.log starts ============//
				//Check if campaign exists on current cache. If not, create it.
				if(!array_key_exists($sendGridJsonItem->campaign, $primaryCache[$primaryCacheItemName]["summary"])){
					$primaryCache[$primaryCacheItemName]["summary"][$sendGridJsonItem->campaign] = array();
				}

				if(property_exists($sendGridJsonItem, "event") && $sendGridJsonItem->event != "" ){
					//Check if the date already exists in the Summary cache. If not, create it.
					if(!array_key_exists($currentDay, $primaryCache[$primaryCacheItemName]["summary"][$sendGridJsonItem->campaign])){
						$primaryCache[$primaryCacheItemName]["summary"][$sendGridJsonItem->campaign][$currentDay] = array();
					}
					//Check if event exists in Summary cache. If not, create it.
					if(!array_key_exists($sendGridJsonItem->event, $primaryCache[$primaryCacheItemName]["summary"][$sendGridJsonItem->campaign][$currentDay])){
						$primaryCache[$primaryCacheItemName]["summary"]
							[$sendGridJsonItem->campaign][$currentDay][$sendGridJsonItem->event] = 1;
					}else{
						$primaryCache[$primaryCacheItemName]["summary"]
							[$sendGridJsonItem->campaign][$currentDay][$sendGridJsonItem->event]++;
					}
					if($sendGridJsonItem->event=="click" && property_exists($sendGridJsonItem, "url") && strpos($sendGridJsonItem->url, $unsubscribeLinkSubstring) !== false){
						//It's a click but it's an UNSUBSCRIBE
						$sendGridJsonItem->event = "unsubscribe";
						//Log this event once more but now as an unsubscribe.
						if(!array_key_exists($sendGridJsonItem->event, $primaryCache[$primaryCacheItemName]["summary"][$sendGridJsonItem->campaign][$currentDay])){
							$primaryCache[$primaryCacheItemName]["summary"]
								[$sendGridJsonItem->campaign][$currentDay][$sendGridJsonItem->event] = 1;
						}else{
							$primaryCache[$primaryCacheItemName]["summary"]
								[$sendGridJsonItem->campaign][$currentDay][$sendGridJsonItem->event]++;
						}
					}
				}
				//========Summary.log ends ============//
			}//End entire foreach
		}
		//Write cache to their respective files.
		dumpCache($primaryCache);
		$processComplete = true;
		flock($threadLockerFile, LOCK_UN);
		fclose($threadLockerFile);
	}else{
		sleep(1); //Thread File locked. Retry in 1 sec.
	}
}

?>

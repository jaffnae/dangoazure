<?php
header('Access-Control-Allow-Origin: *');

//=======================-DOCUMENTATION-====================================\\
//How these functions work...
/*-------------------------------------------------------------------------*\


--------------------------------getSendGridLogs----------------------------
getSendGridLogs($startDateTimestamp, $endDateTimestamp,
									$sendGridEventsToTrack, $optionalAdditionalEvents,
									$nonChartEvents, $getOverallData,
									$getIndividualCampaignData,
									$startDateIndivCampaignTimestamp)
----------------------------------------------------------------------------



--------------------------getIndividualCampaigns---------------------------
getIndividualCampaigns($numberOfMaxCampaignsExpected,
								$startDateIndivCampaignTimestamp, $dbName)

*******PARAMS*******
$numberOfMaxCampaignsExpected --> How many campaigns the return obj will have
$startDateIndivCampaignTimestamp --> Timestamp to start searching backwards
												 from...
$dbName --> Name of the client/folder containing the logs/DB Client Name.
*****END PARAMS*****

*****DESCRIPTION****
//NOTE: Campaign Title MUST be a numeric timestamp to show up on Individual
		//campaign list. If it isn't then we will ignore it.
1) Loop through each month from latest to oldest (e.g. Apr -> Mar -> Feb)
2) Each month, go therough each campaign.
3) Loop through each day, again latest to oldest (31-30-29) etc
4) Loop through each event. If we come across a 'Delivered' event
		We add a campaign to our return object.
	4a) If the Delivered event was on the 1st of the month, we go back
		to the prevoius month. If the same campaign is present in the previous
		month we look at the last day of that campaign and add it to our stats.
5) From here we continue to gather events for the next X days where
		X is $numberofDaysToSearchEventsFor - non-configurable var defined
		inside the function.
	5a) If during event gathering we have reached end of the month then we
		go into the next month (if it exists). If there are events on
		1st/2nd etc we will record those. But if the starting days aren't
		there we won't record those even if campaign data exists.
		As an example if $numberofDaysToSearchEventsFor is 4 and we saw a
		delivered event on the 29th March then we will look for events on
		the 29th, 30th and 31st of March and then look into 1st of April.
**END DESCRIPTION***

----------------------------------------------------------------------------

\*--------------------------------------------------------------------------*/

//======BEFORE GOING LIVE========\\
//TODO: will need to perf-test the sendgrid_logs.
//TODO: I think when we tried this in PROD for some reason we couldn't
		//Get clicks or other events from sendgrid_logs???
//TODO: Go over all current Summaries and add campaign as NONE (or maybe better
			//as their timestamps).

date_default_timezone_set ("Pacific/Auckland");
include_once("vars.php");
include_once("common.php");

$token = "8b2082a0-2664-4468-ae23-5bbcc8f89213";
$dataSource = "sendgridLogs";

if($globalDebug){
	$dataSource = "/Applications/MAMP/htdocs/dangoAzure/sendgridLogs";
}

$startDateTimestamp = $_POST["startDateTimestamp"];
$endDateTimestamp = $_POST["endDateTimestamp"];
$sendGridEventsToTrack = $_POST["sendGridEventsToTrack"];
$optionalAdditionalEvents = $_POST["optionalAdditionalEvents"];
$nonChartEvents = $_POST["nonChartEvents"];
$accessingFromLocalhost = $_POST["accessingFromLocalhost"];
$getOverallData = $_POST["getOverallData"] == "yes" ? true : false;
$getIndividualCampaignData = $_POST["getIndividualCampaignData"] == "yes" ? true : false;
$startDateIndivCampaignTimestamp = $_POST["startDateIndivCampaignTimestamp"];
$numberOfMaxCampaignsExpected = $_POST["numberOfMaxCampaignsExpected"];
$dbName = $_POST["dbName"];

if(isset($_POST["token"]) && $_POST["token"] == $token){
	//API Client authenticated.
	$ret = getSendGridLogs($startDateTimestamp, $endDateTimestamp, $sendGridEventsToTrack,
									$optionalAdditionalEvents, $nonChartEvents, $getOverallData,
									$getIndividualCampaignData, $startDateIndivCampaignTimestamp);
	header('Content-Type: application/json');
   echo json_encode($ret); //Sending response.
}

//Summary - I believe this should only return ONE JSON object containing
//available data for EACH day.
//Design:
	//We need an array of folders that will be involved based on start date and end date
	//Go into each folder, summary.log
	//Loop through Summary.log
	//If key-month-year is after start date, but before end date
	//add it to our return object.
function getSendGridLogs($startDateTimestamp, $endDateTimestamp,
									$sendGridEventsToTrack, $optionalAdditionalEvents,
									$nonChartEvents, $getOverallData,
									$getIndividualCampaignData,
									$startDateIndivCampaignTimestamp){
	GLOBAL $accessingFromLocalhost;
	GLOBAL $acceptableCampaignTitles;
	GLOBAL $dataSource;
	GLOBAL $numberOfMaxCampaignsExpected;
	GLOBAL $dbName;
	//Documentation:
	//Final return Data Structure.....
		//-- return.labels = "Jan, Feb, Mar";
		//-- return.data = array({12,15,20},{3,5,1});
		//At the end of this function we will need to rewrite the way we Populate
			// nonchartdata and chartdata. We want to include a timestamp to each event.
			// this can be any timestamp as well as long as it's for that day.

	//Loop through each item in the contents.
		//We are then to create event analytic object.
			//e.g. $analytic --> {"event": "click", "timestamp": "123", "value": 3}
			//There will be one event per analytic
			//The $analytic array will be our chartData object.
		//Need to also populate nonChartData after.
			//That's simple - just need to tally up totals for each event.

		//Return $retObject - containing ChartData and NonChartData.
		//Change what we do with this return afterwards (See 'UPDATE:' comment above).

	//This is the info we return to whoever wants to query the logs.
	//Should only return what we requested...
	$months_yearDates = array();

	$returnObject = array();
	$returnObject["chartData"] = array();
	$returnObject["nonChartData"] = array();
	$returnObject["individualCampaigns"] = array();

	$nonChartEventArr = array();

	//Populate array containing all months of interest (Format Month_Year)
	if($getOverallData){
		$startDateToLoopThrough = $startDateTimestamp;
		while ($startDateToLoopThrough <= $endDateTimestamp) {
			$dateToPush = date('M_Y', $startDateToLoopThrough);
			if(!in_array($dateToPush, $months_yearDates)){
				array_push($months_yearDates, $dateToPush);
			}
			$startDateToLoopThrough = strtotime(date("d-M-Y", $startDateToLoopThrough).'+ 1 day');
		}
		foreach ($months_yearDates as $month_yearDate) {
			$location = $dataSource."/".$dbName."/".$month_yearDate."/summary.log";

			//Populate non-chart data
			foreach ($nonChartEvents as $nonChartEvent) {
				if(!isset($nonChartEventArr[$nonChartEvent])){
					$nonChartEventArr[$nonChartEvent] = 0;
				}
			}

			if(file_exists($location)){
				$contents = file_get_contents($location);
				$contents = json_decode($contents, true);
				//NOTE: This is where the rework starts.
					//Currently we have day{value} relationship.
					//Instead it's going to be an ojbect of {campaign{day{value}} relationship.
					//Get campaign-specific data

				foreach ($contents as $campaign => $days) {
					if(strpos($campaign, $acceptableCampaignTitles["NL_TIMESTAMP_"]) !== false) {
						$campaign = explode($acceptableCampaignTitles["NL_TIMESTAMP_"], $campaign);
						$campaign = $campaign[1];
					}
					foreach ($days as $day => $value) {
						foreach ($value as $eventInner => $eventNumber) {
							if((isset($optionalAdditionalEvents) && in_array($eventInner, $optionalAdditionalEvents))
								|| (isset($sendGridEventsToTrack) && in_array($eventInner, $sendGridEventsToTrack))){
									$returnItem = new StdClass;
									//Add one hour on the end so that when we match dates on the chart to this data,
									//the timestamp on this data is either more or less than the date on the chart
									//instead of being EQUAL.
									$returnItem->timestamp = strtotime($day."-".str_replace("_","-", $month_yearDate)."+ 1 hour");
									$returnItem->event = $eventInner;
									$returnItem->value = $value[$eventInner];
									array_push($returnObject["chartData"], $returnItem);
							}
							if(isset($nonChartEvents) && in_array($eventInner, $nonChartEvents)){
								//Check date here???
								if(strtotime($day."-".str_replace("_","-", $month_yearDate)) >= $startDateTimestamp
				               && strtotime($day."-".str_replace("_","-", $month_yearDate)) <= $endDateTimestamp){
										$nonChartEventArr[$eventInner] += intval($eventNumber);
								}
							}
						}
					}
				}
			}
		}

		if(!empty($nonChartEventArr)){
			$returnObject["nonChartData"] = $nonChartEventArr;
		}
	}
	if($getIndividualCampaignData){
		$indivCampaign = getIndividualCampaigns($numberOfMaxCampaignsExpected, $startDateIndivCampaignTimestamp, $dbName);
		if(count($indivCampaign) > 0){
			$returnObject["individualCampaigns"] = $indivCampaign;
		}
	}

	return $returnObject;
}

//This function will return campaign data for individual campaigns.
//We give it a limit and start/end dates and the source data.
//It will loop through the source data and return us the data needed
//to construct a graph.
function getIndividualCampaigns($numberOfMaxCampaignsExpected, $startDateIndivCampaignTimestamp, $dbName){
	GLOBAL $accessingFromLocalhost;
	GLOBAL $acceptableCampaignTitles;
	GLOBAL $dataSource;
	$campaignLimitReached = false;
	//Currently cannot be larger than 28. This is because when we loop through a month to
	//check for 'trailing' events and we come to the end of the month we go into the next
	//folder and assume that by the time we can reach the end of that next month we'll
	//be done searching for 'trailing' events.
	$numberofDaysToSearchEventsFor = 10;
	$numberOfMinDeliveriesToRecordCampaigns = 10;
	//Dir to get all available folders from.
	$directory = $dataSource."/".$dbName;
	//Get array of all folder names available under this client.
	//Get rid of folders that are hidden (start with a .) or '..' or '.' folder paths.
	$foldersToLoopThrough = array_diff(preg_grep('/^([^.])/', scandir($directory)), array('..', '.'));
	//Reset array index to start from 0.
	$foldersToLoopThrough = array_values($foldersToLoopThrough);

	//Sort array of folders into order by dates.
	//Newest is first, oldest is last.
	$orderedFoldersByTime = array();
	foreach ($foldersToLoopThrough as $folder) {
		$folderNameForDate = str_replace("_","-", $folder);
		$timestamp = strtotime($folderNameForDate);
		if($timestamp > 946684800){
			//Timestamp above is 1-jan-2000. If valid timestamp, it should be
			//larger than this limit.
			$orderedFoldersByTime[$timestamp] = $folder;
		}
	}
	//Sort array by timestamp. Currently the oldest is first. Will reverse it below
	//to have order go from newest to oldest.
	ksort($orderedFoldersByTime);

	//Reversing array so that newest is the first one.
	$orderedFoldersByTime = array_reverse($orderedFoldersByTime, true);

	//We need an array of Keys that is always in sync with the $orderedFoldersByTime
	//array. We will use this array to navigate through the above array when
	//we need to get the next or previous month. This is because we can't index
	//each item in an associative array such as $orderedFoldersByTime.
	$arrayOfKeys = getArrayOfKeys($orderedFoldersByTime);
	/***************************************DOCUMENTATION***********************************/

	//Now that we have folders associated by time,
	//we need to loop through each one. If the folder
	// (that is 1st of that month) greater than the $startDateIndivCampaignTimestamp,
	//then it's too late. We would CONTINUE to the next older folder.
	//Soon as we've reached a folder that's less than $startDateIndivCampaignTimestamp
	//We go into that folder and open up summary.log.
	//If we see a campaign then this shold be it.
	//Create a campaign object, give it a name, timestamp and data.
	//We need to figure out which of the three campaigns are the latest by "delivered" event.

	//HOW???
		//Well... We could find the first (latest) 3 campaigns that aren't named
		//NONE or whatever and if there's more than say... 2 deliveries then we will call it
		//a campaign.
		//We then go through each of those campaigns and iterate over the next...2 weeks(?)
		//of data for corresponding campaigns to see how many people interacted with it.

	/***************************************DOCUMENTATION***********************************/

	//TODO: Remember that each "object" we return that represents
	//an individual campaign chart NEEDS TO also contain a timestamp
	//as to when it was generated.
	$latestCampaigns = array();
	$iterator = 0;
	foreach ($orderedFoldersByTime as $timestamp => $folderName) {
		logger("Entering folder ". $folderName);

		//If we've hit the limit of the campaigns we are expecting
		//then stop.
		if(count($latestCampaigns) > ($numberOfMaxCampaignsExpected-1)){
			logger("Number of max campaigns reached. Exiting from outer loop.");
			break;
		}

		//If the folder (that is 1st of that month) is greater than the
		//$startDateIndivCampaignTimestamp, then it's too late.
		if($timestamp > $startDateIndivCampaignTimestamp){
			$iterator++; //Moving onto the next folder so must increment iterator.
			continue;
		}

		$location = $dataSource."/".$dbName."/".$folderName."/summary.log";
		if(file_exists($location)){
			$contents = file_get_contents($location);
			//Got the data on campaigns.
			$contents = json_decode($contents, true);
			//Order $contents (campaigns) by time - currently oldest is first
			//as that's the lowest timestamp.
			//Will need to reverse.
			ksort($contents);
			//Reversing so that newest is first and oldest is last.
			$contents = array_reverse($contents, true);
			foreach ($contents as $campaignName => $days) {
				if(strpos($campaignName, $acceptableCampaignTitles["NL_TIMESTAMP_"]) !== false) {
					$campaignName = explode($acceptableCampaignTitles["NL_TIMESTAMP_"], $campaignName);
					$campaignName = $campaignName[1];
				}
				//If we've hit the limit of the campaigns we are expecting
				//then stop.
				if(count($latestCampaigns) > ($numberOfMaxCampaignsExpected-1)){
					logger("Number of max campaigns reached. Exiting from inner loop.");
					break(2);
				}
				logger("Moving onto a new campaign: ". $campaignName);
				//If campaign it's not an actual campaign (e.g. NONE) then carry on.
				if(strtolower($campaignName) == strtolower($acceptableCampaignTitles["NONE"])
					|| strtolower($campaignName) ==  strtolower($acceptableCampaignTitles["JSONTEST"])){
							logger("Campaign ". $campaignName . "is not a campaign name we should be gathering stats for. Continuing.");
							continue;
				}
				//If campaign is not a number or if campaign name is greater than
				//the $startDateIndivCampaignTimestamp provided as an argument
				//to this function then the campaign is too new and we need
				//to continue to an older campaign.
				logger("Searching through campaign ". $campaignName . ". Requested timestamp: ".$startDateIndivCampaignTimestamp.".");
				if(!is_numeric($campaignName) || intval($campaignName) > $startDateIndivCampaignTimestamp){
					//Campaign-level loop
					logger("Campaign ". $campaignName . "is either not numeric or happened after the requested timestamp (".$startDateIndivCampaignTimestamp."). Continuing.");
					continue;
				}

				//If we've finished gathering the data for this campaign already
				//then move one. We assume that we should never leave the campaign
				//loop iteration without completely gathering all the data
				//for that one campaign.
				if(array_key_exists($campaignName, $latestCampaigns)){
					logger("Campaign ". $campaignName . "already exists. Continuing.");
					continue;
				}

				$daysLatestToOldest = array_reverse($days, true);

				//Depiction of the structure we're dealing with here:
				/*
					Month/Folder: Apr_2017
					Campaign: [TIMESTAMP]
					Days:
						31--->---30--->----29---->---28--->----27---->---26---->---25...
					clck:10  clck:9   clck:11   clck:16     clck:6    clck:9   clck:13
					open:17  open:21  open:46   open:15     open:11   open:10   open:33
					dlvr:0   dlvr:0    dlvr:0    dlvr:0    dlvr:93    dlvr:0     dlvr:0
				*/

				foreach ($daysLatestToOldest as $day => $value) {
					//Check here for campaigns in which we've 'bled' into.
					foreach ($value as $eventInner => $eventNumber) {
						//We are looping through days. And then through each event.
						//We could say as soon as we've discovered 'delivered' for
						//a campaign then we've got our campaign and we should stop
						//looking at all the following days after that.
						//This means in the for loop where we forwards, as soon
						//as we're done we should break out of these loops and move
						//onto the next campaign.
						logger("steeep " . $campaignName . " day: " . $day . "Event: ".$eventInner . " Event number: " . $eventNumber);
						if($eventInner == "delivered" && $eventNumber > $numberOfMinDeliveriesToRecordCampaigns){
							logger("delivery event detected.");
							if(count($latestCampaigns) > ($numberOfMaxCampaignsExpected-1)){
								logger("abort5");
								break(4); //Break out of the first loop where we loop
											//through folders.
							}

							if(!array_key_exists($campaignName, $latestCampaigns)){
								//If we don't have this campaign in our return object
								//Add it in.
								$latestCampaigns[$campaignName] = array();
							}

							//TODO: Should we ALWAYS check the previous day for any trailing deliveries?

							//If we are on the first day of the month where we discover 'Delivered'
							//event then we should go to the previous month to the same
							//campaign ensure to gather any more deliveries.
							if($day == "01"){
								//Check and collect events for last
								//day of previous month
								$latestCampaigns = checkAndCollectEventsForPrevMonth($orderedFoldersByTime, $arrayOfKeys,
																									$iterator, $campaignName,
																									$latestCampaigns, $dbName);
							}

							//Go forward from current day collect follow up events.
							//Only go as far as the $numberofDaysToSearchEventsFor
							//setting.
							for ($i=0; $i < $numberofDaysToSearchEventsFor; $i++) {
								$newDay = (int)$day + $i;
								if($newDay < 10){
									$newDay = "0".$newDay;
								}
								logger("Searching for day ". $newDay . " in the month: " . $folderName . ", Campaign: " . $campaignName);

								//Establishing the first day in the $daysLatestToOldest arr
								//The latest day is also the first element.
								$latestDate = null;
								foreach ($daysLatestToOldest as $keyDay => $value) {
									$latestDate = $keyDay;
									break; //Found first key. no longer need to loop.
								}

								//If the new day that we'd like to record events for
								//is newer than the first item in $daysLatestToOldest
								//i.e. if $newDay is larger then we reached the end
								//for this month. Need to check in a newer folder.
								if($latestDate != null && $newDay > $latestDate){
									logger("End of month detected.");
									if($timestamp == $arrayOfKeys[0]){
										//we're in the latest month. Stop searching.
										//break out of the loop.
										logger("Reached the latest month. Cannot get any new data for this campaign. Will search for next campaign.");
										logger("abort4");
										break(3);
									}else{
										$newerMonth = $orderedFoldersByTime[$arrayOfKeys[$iterator-1]];

										logger("Opening next month's folder ". $newerMonth);

										$newerMonthLocation = $dataSource."/".$dbName."/".$newerMonth."/summary.log";
										if(file_exists($newerMonthLocation)){

											$newerMonthContents = file_get_contents($newerMonthLocation);
											//Got the data on campaigns.
											$newerMonthContents = json_decode($newerMonthContents, true);

											if(array_key_exists($campaignName, $newerMonthContents)){
												logger("Found current campaign inside newer month. Will get stats for this campaign inside newer month.");
												$daysLeftToSearchFor = $numberofDaysToSearchEventsFor - $i;

												for($iDayNewMonth=0; $iDayNewMonth < $daysLeftToSearchFor; $iDayNewMonth++) {

													//$dayUsedToSearchArray needs to start from at least 1 because it represents
													//the DAY in our summary.log ie there's no {0: "click": 16}
													$dayUsedToSearchArray = $iDayNewMonth +1;

													//Beause our dates start with '01'
													//We need to make sure that singular
													//items become prepended with a 0.
													if($dayUsedToSearchArray < 10){
														$dayUsedToSearchArray = "0".$dayUsedToSearchArray;
													}

													if(array_key_exists($dayUsedToSearchArray, $newerMonthContents[$campaignName])){
														//There are new events in this new month.
														//Record them.

														logger("day in a new month");
														logger(json_encode($newerMonthContents[$campaignName][$dayUsedToSearchArray]));

														foreach ($newerMonthContents[$campaignName][$dayUsedToSearchArray] as $eventInNewMonth => $eventNumberNewMonth) {
															//Add the event number to our campaign.
															if(is_numeric($eventNumberNewMonth)){
																//If this event type doesn't exist assign it, else increment it.
																if(!array_key_exists($eventInNewMonth, $latestCampaigns[$campaignName])){
																	$latestCampaigns[$campaignName][$eventInNewMonth] = $eventNumberNewMonth;
																}else{
																	$latestCampaigns[$campaignName][$eventInNewMonth] += $eventNumberNewMonth;
																}
															}
														}
													}
												}
											}
											//If our campaign exists in the current campaign we loop
											//through it. Otherwise we just break because we reached
											//the end of the last month and there's no new data
											//in the newer month.
											logger("abort3");
											break(3);
										}
										//If file doesn't exist for this month then break;
										logger("abort1");
										break(3); //Newer month doesn't exist. Move onto next
													//campaign.
									}
								}//END: If the day reaches the end of the month.

								//Most of the time we'll skip through to this code
								//when recording individual campaign data.
								if(array_key_exists($newDay, $days)){
									logger("Found newDay in the month: " . $folderName . ", Campaign: " . $campaignName. ". Gathering events.");
									foreach ($days[$newDay] as $newDayEvent => $newDayEventNumber) {
										//Add the event number to our campaign.
										if(is_numeric($newDayEventNumber)){

											logger("About to Increment/Add '" . $newDayEvent . "' for the month: " . $folderName . ", Campaign: " . $campaignName);
											logger("Current campaign set:");
											logger(json_encode($latestCampaigns));

											//If this event type doesn't exist assign it, else increment it.
											if(!array_key_exists($newDayEvent, $latestCampaigns[$campaignName])){
												$latestCampaigns[$campaignName][$newDayEvent] = $newDayEventNumber;
											}else{
												$latestCampaigns[$campaignName][$newDayEvent] += $newDayEventNumber;
											}
											logger("Finished Incrementing/Adding '" . $newDayEvent . "' for the month: " . $folderName . ", Campaign: " . $campaignName);
											logger("Current campaign set:");
											logger(json_encode($latestCampaigns));
										}
									}
								}
							}
							logger("abort2");
							break(2); //Break to move onto the next campaign (top loop).
										 //At this point we should have all the numbers
										 //for one of the campaigns (assuming 1 delivery event
										 //per campaign).
						}else{
							//There were less than 2 deliveries on this day for this campaign.
							continue;
						}
					}
				}
			}
		}
		//Increment the loop iterator. This allows us to find a key in the
		//associative array when we wanna get previous or next items using our
		//'helper' array that contains all the keys.
		$iterator++;
	}
	return $latestCampaigns;
}

//Check and collect events for last
//day of previous month
function checkAndCollectEventsForPrevMonth($orderedFoldersByTime, $arrayOfKeys,
														$iterator, $campaignName,
														$latestCampaigns, $dbName){
	GLOBAL $dataSource;

	$previousMonth = $orderedFoldersByTime[$arrayOfKeys[$iterator+1]];
	logger("First of month detected. Opening folder of previous month: ". $previousMonth);
	$previousMonthMonthLocation = $dataSource."/".$dbName."/".$previousMonth."/summary.log";
	if(file_exists($previousMonthMonthLocation)){
		logger("Found previous month. Looping through contents");
		$previousMonthContents = file_get_contents($previousMonthMonthLocation);
		$previousMonthContents = json_decode($previousMonthContents, true);
		if(array_key_exists($campaignName, $previousMonthContents)){
			//Get the last day of the array.
			//NOTE: This will not return the day number to us.
			//only the array value of the day. So for
			//29: {"click": 2, "open": 23} we'll get {"click": 2, "open": 23}
			$valueOfLastDay = array_values(array_slice($previousMonthContents[$campaignName], -1))[0];

			foreach ($valueOfLastDay as $eventPrevMonth => $eventPrevMonthNumber) {
				if(!array_key_exists($eventPrevMonth, $latestCampaigns[$campaignName])){
					$latestCampaigns[$campaignName][$eventPrevMonth] = $eventPrevMonthNumber;
				}else{
					$latestCampaigns[$campaignName][$eventPrevMonth] += $eventPrevMonthNumber;
				}
			}
		}
	}
	logger("Finished process for checking last day of previous month. Current Campaign Set:");
	logger(json_encode($latestCampaigns));
	return $latestCampaigns;
}

function getArrayOfKeys($associativeArray){
	$ret = array();
	foreach ($associativeArray as $key => $value) {
		array_push($ret, $key);
	}
	return $ret;
}
?>

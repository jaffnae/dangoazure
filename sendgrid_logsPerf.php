<?php
// $logFolder = "sendgridLogs_TEST";
//
// function logger($msg){
// 	$han = fopen("sendgridPerfResults.log", 'a+');
// 	fwrite($han, "
// 	");
// 	fwrite($han, $msg);
// 	fclose($han);
// }
// $loggerIds = array();
// function logPerf($msg, $t1, $t2, $logOnce){
// 	GLOBAL $loggerIds;
// 	if($logOnce && in_array($msg, $loggerIds)){
// 		return;
// 	}
// 	logger($msg." ". ($t2 - $t1) . " seconds");
// 	array_push($loggerIds, $msg);
// }
// logger("================PERF TEST=============");
// //Time for the entire process
// $time_preGlobal = microtime(true);
//
//
// function dumpExistingCache($currentDay, $clientCache, $currentMonthYearDirName, $destinationCache, $eventCache){
// 	GLOBAL $logFolder;
// 	$summaryEvent = $eventCache["event"];
// 	$eventCount = $eventCache["count"];
//
// 	if($clientCache == null || $eventCount == 0){
// 		//There isn't enough data to dump so far...
// 		return;
// 	}
//
// 	//We have got a destination directory. Open dump.log and write to it.
// 	$logFolderPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . $logFolder);
// 	$clientPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . $logFolder)
// 					. DIRECTORY_SEPARATOR . $clientCache;
//
// 	if(!file_exists($logFolderPath)){
// 		mkdir($logFolderPath, 0700, true); //Create Directory
// 	}
//
// 	if(!file_exists($clientPath)){
// 		mkdir($clientPath, 0700, true); //Create Directory
// 	}
//
// 	if(!file_exists($destinationCache)){
// 		mkdir($destinationCache); //Create Directory
// 	}
//
// 	#Isolate events only and log them to separate log file.
// 	if(!file_exists($destinationCache."/summary.log")){
// 		$summary = array();
// 	}else{
// 		$summary = file_get_contents($destinationCache."/summary.log");
// 		$summary = json_decode($summary, true);
// 	}
//
// 	if(array_key_exists($currentDay, $summary)){
// 		//We've already got records for this day
// 		if(array_key_exists($summaryEvent, $summary[$currentDay])){
// 			$summary[$currentDay][$summaryEvent] += $eventCount;
// 		}else{
// 			$summary[$currentDay][$summaryEvent] = $eventCount;
// 		}
// 	}else{
// 		$summary[$currentDay] = array();
// 		$summary[$currentDay][$summaryEvent] = $eventCount;
// 	}
//
// 	//Write to Summary log
// 	$summaryFileHandler = fopen($destinationCache.'/summary.log', 'w'); //Needs to be w so that we clear off the file and write back to it again.
// 	fwrite($summaryFileHandler, json_encode($summary, true));
// 	fclose($summaryFileHandler);
// }
//
// function dumpDetailsCacheForClient($detailsCache, $destinationCache){
// 	$detailedLogFileHandeler = fopen($destinationCache.'/detailedLog.log', 'a+');
// 	if($detailedLogFileHandeler)
// 	{
// 		$logItem = substr(json_encode($detailsCache), 1, -1);
// 		fwrite($detailedLogFileHandeler, $logItem.", \r\n");
// 		fclose($detailedLogFileHandeler);
// 	}
// }
//
// date_default_timezone_set ("Pacific/Auckland");
//
//
// //Prodcution source:
// //$rawSendGridItems = file_get_contents("php://input");
// $rawSendGridItems = file_get_contents("sendgdOrigins/logs.log");
//
//
// $threadLockerFile = fopen("!sendGridLockFile_DoNotDelete.php", 'a+');
// $processComplete = false;
//
// while(!$processComplete){
// 	if(flock($threadLockerFile, LOCK_EX)) {
// 		//File locked successfully
// 		$unsubscribeLinkSubstring = "page=unsubscribeUser";
// 		$sendGridItemPropertiesToTrack = array("client", "ip", "subject", "subjCat", "subjCat", "event", "sg_event_id", "email", "timestamp", "timestamp", "url");
//
// 		$sendGridJsonItems = json_decode($rawSendGridItems);
// 		if(count($sendGridJsonItems) > 0){
// 			//Start entire foreach
// 			$time_preOuter = microtime(true);
//
// 			$clientCache = null;
// 			$currentMonthYearDirName = date("M_Y");
// 			$currentMonth = date("M");
// 			$currentDay = date("d");
// 			$destinationCache = null;
// 			$eventCache = array("event" => "delivered", "count" => 0);
// 			$detailsCache = array();
//
// 			foreach ($sendGridJsonItems as $sendGridJsonItem) {
// 				//Client Check
// 				if(!property_exists($sendGridJsonItem, "client") || $sendGridJsonItem->client == ""){
// 					$sendGridJsonItem->client = "noClientDetermined";
// 				}
//
// 				if($sendGridJsonItem->client != $clientCache){
// 					if($clientCache != null){
// 						dumpExistingCache($currentDay, $clientCache, $currentMonthYearDirName, $destinationCache, $eventCache);
// 						dumpDetailsCacheForClient($detailsCache, $destinationCache);
// 						$clientCache = null;
// 						$destinationCache = null;
// 						$detailsCache = array();
// 						$eventCache = array("event" => "delivered", "count" => 0);
// 					}
// 					$clientCache = $sendGridJsonItem->client;
// 					$destinationCache = realpath(__DIR__ . DIRECTORY_SEPARATOR . $logFolder)
// 										. DIRECTORY_SEPARATOR . $clientCache . DIRECTORY_SEPARATOR . $currentMonthYearDirName;
// 				}
// 				//Accumulate Details.log info...
// 				$logItem = new StdClass;
// 				$bodyHasContent = false;
// 				foreach ($sendGridItemPropertiesToTrack as $property) {
// 					if(property_exists($sendGridJsonItem, $property)){
// 						$logItem->{$property} = $sendGridJsonItem->{$property};
// 						$bodyHasContent = true;
// 					}
// 				}
// 				if($bodyHasContent){
// 					array_push($detailsCache, $logItem);
// 				}
//
// 				//Event Check
// 				if(property_exists($sendGridJsonItem, "event") && $sendGridJsonItem->event != "" ){
// 					if($sendGridJsonItem->event != $eventCache["event"]){
// 						if($eventCache["count"] > 0){
// 							dumpExistingCache($currentDay, $clientCache, $currentMonthYearDirName, $destinationCache, $eventCache);
// 							//Reset event cache but not the client cache...
// 							$eventCache = array("event" => "delivered", "count" => 0);
// 						}
// 						$eventToLog = $sendGridJsonItem->event;
// 						if($sendGridJsonItem->event=="click" && property_exists($sendGridJsonItem, "url") && strpos($sendGridJsonItem->url, $unsubscribeLinkSubstring) !== false){
// 							//It's a click but it's an UNSUBSCRIBE
// 							$eventToLog = "unsubscribe";
// 						}
// 						$eventCache["event"] = $eventToLog;
// 						$eventCache["count"] = 1;
// 					}else{
// 						$eventCache["count"]++;
// 					}
// 				}
// 				//TODO: Go back over and comment everything so that it's easy to read in the future
// 				//TODO: Check on an event batch containing multiple events and clients (jumbled up)
// 				#END: Isolate events only and log them to separate log file.
// 			}//End entire foreach
//
// 			//Final Cache dump if we have any events left over from the loop.
// 			if(!empty($detailsCache)){
// 				dumpDetailsCacheForClient($detailsCache, $destinationCache);
// 				error_log(json_encode($detailsCache));
// 			}
//
// 			if($eventCache["count"] > 0){
// 				dumpExistingCache($currentDay, $clientCache, $currentMonthYearDirName, $destinationCache, $eventCache);
// 			}
// 			$time_postOuter = microtime(true);
// 			logPerf("Entire foreach:", $time_preOuter, $time_postOuter, true);
// 		}
// 		$processComplete = true;
// 		flock($threadLockerFile, LOCK_UN);
// 		fclose($threadLockerFile);
// 	}else{
// 		sleep(2);
// 	}
// }
// $time_postGlobal = microtime(true);
// logPerf("Entire process:", $time_preGlobal, $time_postGlobal, true);

?>

<?php
$logsWanted = false;
if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['SERVER_NAME'] == "localhost"){
	$globalDebug = true;
}else{
	$globalDebug = false;
}

function logger($string){
   GLOBAL $globalDebug;
   if(isset($logsWanted) && $logsWanted && $globalDebug){
      error_log($string);
   }
}

?>

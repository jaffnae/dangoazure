
<?php 

if(!isset($error_title)){
	$error_title = 'Error';
}

if(!isset($error_imageName)){
	$error_imageName = 'error.png';
}

if(!isset($error_desc)){
	$error_desc = 'An unknown error has occured.';
}

?>

<div class="topContainer" style="padding-bottom: 150px;">
	<div class="headerContentArea centerMe">
		
		<div class="ePageContainer">
			
			<div class="ePageText">
				<h2 style="font-size: 42px; max-width: 600px; margin: 0 auto;"><?php echo $error_title; ?></h2>
				<br /><br /><br />
				<div style="width:300px; margin: 0 auto;"><img src='images/<?php echo $error_imageName; ?>' /></div>
				<br /><br /><br />
				<h2 style="font-size: 20px; max-width: 600px; margin: 0 auto; line-height: normal;"><?php echo $error_desc; ?></h2>
				
				<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>

			</div>
		</div>

	</div>
</div>

<script>
	$(function(){
		$(".refresh").click(function(){
			location.reload();
		})
	});
</script>
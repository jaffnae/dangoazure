<?php

/* _________________________________________
|                                         |
|  There's a duplicate vars.php in        |
|   the embed folder - edit that too!     |
|_________________________________________|
*/


$userWithPricesForOneCent = "100005192885393";

$defaultPointAmountRef = '4';
$defaultPointAmountTweet  = '5';
$defaultPointAmountAsk = '5';
$defaultPointAmountEmail = '7';
$defaultPointAmountPinterest = '5';
$defaultPointAmountPoll = '5';
$defaultPointAmountVisitFanpage = '3';
$defaultPointAmountVisitInstagram = '5';
$maxImageUploadsize = 2500000;

$ultimateMembership = "Ultimate";
$premProMembership = "premPro";
$premLiteMembership = "premLite";

//Braintree Plans - ultimate membership
//Adding a NEW membership?
//Step0 - add it on Braintree
//Step 1 - add it here (if it's registered as Ultimate in db)
$btNnzMembersUltimate = "NnzMembersUltimate";
$btUltimateCustomGiveaways = "UltimateCustomGiveaways";
$btNnzFreeGroupUltimate = "NnzFreeGroupUltimate";
$btUltimateDiscounted = "UltimateDiscounted";
$btUltimateYearly = "UltimateYearly";
$bt3MonthsFreeFoundationMembership = "3MonthsFreeFoundationMembership";
$btUltimate60Off = "Ultimate60Off";
$btDangoDine = "dangoDineOrig";
$newsletterService = "newsletterService"; //No plan associated with this membership
$newsletterService2k = "newsletterService2k"; //No plan associated with this membership
$newsletterService30k = "newsletterService30k"; //No plan associated with this membership
$btUltimateCustomGiveaways30Off = "UltimateCustomGiveaways30Of";
$btUltimateCollabGiveaways = "UltimateCollabGiveaways";
$btUltimateCustomGiveawaysWP = "UltimateCustomGiveawaysWP";
$btUltimateFreeFirstMonth = "ultimateFreeFirstMonth";
$btUltimateCollabGiveawaysFreeMonth = "UltimateCollabGiveawaysFreeMonth";
$btUpdatePayment = "UpdatePayment";

//Step 2 - add it here if it's Ultimate in DB.
$brainTreePlanToMembershipTypeMapping[$ultimateMembership]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$bt3MonthsFreeFoundationMembership]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btNnzFreeGroupUltimate]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateDiscounted]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateYearly]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimate60Off]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveawaysWP]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways30Off]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveaways]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateFreeFirstMonth]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveawaysFreeMonth]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btDangoDine]["dbMembership"] = $ultimateMembership;

//Step 3 - Give the new membership a give it a [title] property and a [priceDescription] property
$brainTreePlanToMembershipTypeMapping[$ultimateMembership]['title'] = "Dango Ultimate";
$brainTreePlanToMembershipTypeMapping[$ultimateMembership]['price'] = 22.99;
$brainTreePlanToMembershipTypeMapping[$ultimateMembership]['priceDescription'] = "For ".$brainTreePlanToMembershipTypeMapping[$ultimateMembership]['price']." NZD";

$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways]['title'] = "Dango Automated Giveaways";
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways]['price'] = 27.99;
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways]['priceDescription'] = "For ".$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways]['price']." NZD  p/month (no cancellation fee)";

$brainTreePlanToMembershipTypeMapping[$bt3MonthsFreeFoundationMembership]['title'] = "Dango Ultimate";
$brainTreePlanToMembershipTypeMapping[$bt3MonthsFreeFoundationMembership]['price'] = 0;
$brainTreePlanToMembershipTypeMapping[$bt3MonthsFreeFoundationMembership]['priceDescription'] = "$".$brainTreePlanToMembershipTypeMapping[$bt3MonthsFreeFoundationMembership]['price']." first 3 months, $11.50NZD afterwards";

$brainTreePlanToMembershipTypeMapping[$btNnzMembersUltimate]['title'] = "NNZ Members Only - Dango Ultimate";
$brainTreePlanToMembershipTypeMapping[$btNnzMembersUltimate]['price'] = 0;
$brainTreePlanToMembershipTypeMapping[$btNnzMembersUltimate]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btNnzMembersUltimate]['price']." (first month), half price afterwards";

$brainTreePlanToMembershipTypeMapping[$btNnzFreeGroupUltimate]['title'] = "NNZ Group Members Dango Ultimate";
$brainTreePlanToMembershipTypeMapping[$btNnzFreeGroupUltimate]['price'] = 1;
$brainTreePlanToMembershipTypeMapping[$btNnzFreeGroupUltimate]['priceDescription'] = "For $1 NZD (first month)";

$brainTreePlanToMembershipTypeMapping[$btUltimateDiscounted]['title'] = "Ultimate Membership - Discounted";
$brainTreePlanToMembershipTypeMapping[$btUltimateDiscounted]['price'] = 1.50;
$brainTreePlanToMembershipTypeMapping[$btUltimateDiscounted]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btUltimateDiscounted]['price']." NZD (first month)";

$brainTreePlanToMembershipTypeMapping[$btUltimateYearly]['title'] = "Ultimate Membership - Yearly";
$brainTreePlanToMembershipTypeMapping[$btUltimateYearly]['price'] = 275.88;
$brainTreePlanToMembershipTypeMapping[$btUltimateYearly]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btUltimateYearly]['price']." NZD";

$brainTreePlanToMembershipTypeMapping[$btUltimate60Off]['title'] = "Ultimate Membership - 60% off";
$brainTreePlanToMembershipTypeMapping[$btUltimate60Off]['price'] = 9.20;
$brainTreePlanToMembershipTypeMapping[$btUltimate60Off]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btUltimate60Off]['price']." NZD (first month)";

$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveawaysWP]['title'] = "Dango Custom Giveaways";
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveawaysWP]['price'] = 57.99;
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveawaysWP]['priceDescription'] = "For ".$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveawaysWP]['price']." NZD per month";

$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways30Off]['title'] = "Dango Automated Giveaways with Guarantee";
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways30Off]['price'] = 19.50;
$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways30Off]['priceDescription'] = "For ".$brainTreePlanToMembershipTypeMapping[$btUltimateCustomGiveaways30Off]['price']." NZD p/month";

$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveaways]['title'] = "Dango Collaborative Giveaways with Guarantee";
$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveaways]['price'] = 33.99;
$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveaways]['priceDescription'] = "For ".$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveaways]['price']." NZD p/month";


$brainTreePlanToMembershipTypeMapping[$btDangoDine]['title'] = "Dango Dine Membership";
$brainTreePlanToMembershipTypeMapping[$btDangoDine]['price'] = 44.99;
$brainTreePlanToMembershipTypeMapping[$btDangoDine]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btDangoDine]['price']." NZD  p/month (no cancellation fees)";


$brainTreePlanToMembershipTypeMapping[$btUltimateFreeFirstMonth]['title'] = "Dango Ultimate Membership";
$brainTreePlanToMembershipTypeMapping[$btUltimateFreeFirstMonth]['price'] = 0;
$brainTreePlanToMembershipTypeMapping[$btUltimateFreeFirstMonth]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btUltimateFreeFirstMonth]['price']." (first month), $22.99 for the next month.";

$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveawaysFreeMonth]['title'] = "Dango Collaborative Membership";
$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveawaysFreeMonth]['price'] = 0;
$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveawaysFreeMonth]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$btUltimateCollabGiveawaysFreeMonth]['price']." (first month), $33.99 for the next month.";

//No plan associated with this membership
$brainTreePlanToMembershipTypeMapping[$newsletterService2k]['title'] = "Dango - Newsletter Service 0-5k subscribers";
$brainTreePlanToMembershipTypeMapping[$newsletterService2k]['price'] = 35.00;
$brainTreePlanToMembershipTypeMapping[$newsletterService2k]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$newsletterService2k]['price']." NZD  p/month (no cancellation fees)";

//No plan associated with this membership
$brainTreePlanToMembershipTypeMapping[$newsletterService]['title'] = "Dango - Newsletter Service 5-7k subscribers";
$brainTreePlanToMembershipTypeMapping[$newsletterService]['price'] = 55.00;
$brainTreePlanToMembershipTypeMapping[$newsletterService]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$newsletterService]['price']." NZD  p/month (no cancellation fees)";

$brainTreePlanToMembershipTypeMapping[$newsletterService30k]['title'] = "Dango - Newsletter Service  30-40k subscribers";
$brainTreePlanToMembershipTypeMapping[$newsletterService30k]['price'] = 160.00;
$brainTreePlanToMembershipTypeMapping[$newsletterService30k]['priceDescription'] = "For $".$brainTreePlanToMembershipTypeMapping[$newsletterService30k]['price']." NZD  p/month (no cancellation fees)";

//Braintree Plans - premium pro memberships
//Adding a NEW membership?
//Step0 - add it on Braintree
//Step 1 - add it here (if it's registered as PremPro in db)
$btNnzMembersPremPro = "nnzNnzMembersPremPro";
$btPremProDiscounted = "premProDiscounted";
$bt20DPPNNZ = "20DPPNNZ";
$btPremProYearly = "premProYearly";
$btPremProFreeFirstMonth = "premProFreeFirstMonth";


//Step 2 - add it here if it's PremPro in DB.
$brainTreePlanToMembershipTypeMapping[$premProMembership]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btNnzMembersPremPro]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btPremProDiscounted]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$bt20DPPNNZ]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btPremProFreeFirstMonth]["dbMembership"] =
$brainTreePlanToMembershipTypeMapping[$btPremProYearly]["dbMembership"] = $premProMembership;

//Step 3 - Give the new membership a give it a [title] property and a [priceDescription] property
//Step 3 -pricembership a give it a [title] property and a [priceDescription] property
$brainTreePlanToMembershipTypeMapping[$premProMembership]["title"] = "Dango Premium Pro";
$brainTreePlanToMembershipTypeMapping[$premProMembership]["price"] = 16.99;
$brainTreePlanToMembershipTypeMapping[$premProMembership]["priceDescription"] = "For $".$brainTreePlanToMembershipTypeMapping[$premProMembership]["price"]." NZD";

$brainTreePlanToMembershipTypeMapping[$btNnzMembersPremPro]["title"] = "NNZ Members Only - Dango Premium Pro";
$brainTreePlanToMembershipTypeMapping[$btNnzMembersPremPro]["price"] = 0;
$brainTreePlanToMembershipTypeMapping[$btNnzMembersPremPro]["priceDescription"] = "For $".$brainTreePlanToMembershipTypeMapping[$btNnzMembersPremPro]["price"]." (first month), half price afterwards";

$brainTreePlanToMembershipTypeMapping[$bt20DPPNNZ]["title"] = "NNZ Members Only - Dango Premium Pro";
$brainTreePlanToMembershipTypeMapping[$bt20DPPNNZ]["price"] = 0;
$brainTreePlanToMembershipTypeMapping[$bt20DPPNNZ]["priceDescription"] = "$".$brainTreePlanToMembershipTypeMapping[$bt20DPPNNZ]["price"]." first 20 days, half price afterwards";

$brainTreePlanToMembershipTypeMapping[$btPremProDiscounted]["title"] = "Dango Premium Pro - Discounted";
$brainTreePlanToMembershipTypeMapping[$btPremProDiscounted]["price"] = 1;
$brainTreePlanToMembershipTypeMapping[$btPremProDiscounted]["priceDescription"] = "For $".$brainTreePlanToMembershipTypeMapping[$btPremProDiscounted]["price"]." NZD (first month)";

$brainTreePlanToMembershipTypeMapping[$btPremProYearly]["title"] = "Dango Premium Pro - Yearly";
$brainTreePlanToMembershipTypeMapping[$btPremProYearly]["price"] = 203.88;
$brainTreePlanToMembershipTypeMapping[$btPremProYearly]["priceDescription"] = "For $".$brainTreePlanToMembershipTypeMapping[$btPremProYearly]["price"]." NZD";

$brainTreePlanToMembershipTypeMapping[$btPremProFreeFirstMonth]["title"] = "Dango Premium Pro Membership";
$brainTreePlanToMembershipTypeMapping[$btPremProFreeFirstMonth]["price"] = 0;
$brainTreePlanToMembershipTypeMapping[$btPremProFreeFirstMonth]["priceDescription"] = "For $".$brainTreePlanToMembershipTypeMapping[$btPremProFreeFirstMonth]["price"]." (first month), $16.99 for the next month.";





//Braintree Plans - premium lite memberships
$brainTreePlanToMembershipTypeMapping[$premLiteMembership]["dbMembership"] = $premLiteMembership;
$brainTreePlanToMembershipTypeMapping[$premLiteMembership]["title"] = "Dango Premium Lite";
$brainTreePlanToMembershipTypeMapping[$premLiteMembership]["price"] = 14.99;
$brainTreePlanToMembershipTypeMapping[$premLiteMembership]["priceDescription"] = "For $".$brainTreePlanToMembershipTypeMapping[$premLiteMembership]["price"]." NZD";


$actionGateBecomeASubscriber = "becomeASubscriber";
$actionGateAnswerAnActionGateQuestion = "answerAnActionGateQuestion";
$actionGateAnswerPoll = "answerAPoll";
$actionGateVisitInstagram = "visitOnInstagram";

$collabEntryType_join_mail_list = "collabOption_joinMailingList";
$collabEntryType_visit_collab_fanpage = "collabOption_visitCollabFanpage";
$collabEntryType_visit_instagram = "collabOption_visitInstagram";

$growFanbaseBecomeAFan = "becomeAFan";
$growFanbaseSharePageOnDango = "sharePageOnDango";
$growFanbaseVisitFanpage = "visitFanpage";

$nl_price_of_featured_giveaway = 1.88;
$nl_featued_giveaway_start_price = 4.8;

$nl_price_of_top_giveaway = 1.21;
$nl_top_giveaway_start_price = 2.9;

$nl_price_of_standard_giveaway = 0.61;
$nl_standard_giveaway_start_price = 1.8;

$cut_off_id_for_emails_in_collabs = 2637;

$acceptableCampaignTitles = array();
$acceptableCampaignTitles["NONE"] = "NONE";
$acceptableCampaignTitles["JSONTEST"] = "JSONTEST";
?>

<?php
date_default_timezone_set ("America/Phoenix"); // -7hours GMT -- This is to keep the dgo servers in sync with the dango servers.
/*
==============================================================
||                     Deployment:                           ||
||                                                           ||
||  ./deploy.sh "commitpull" "commit Message" "" ""          ||
||  ./deploy.sh "pull"                                       ||
||  ./deploy.sh "pull" "" "dango"                            ||
||  ./deploy.sh "commitpull" "commit Message" "" "liveonly"  ||
||                                                           ||
==============================================================

============================================================
||                                                         ||
||  WARNING: Users that get a Premium GIVEAWAY             ||
||  will get to see Premium STATS of all previous          ||
||  giveaways, until they've created their first giveaways ||
||                                                         ||
============================================================


TestRig DB: https://sg2nlsmysqladm1.secureserver.net/dgrid50/3
Propper Database: https://sg2nlsmysqladm1.secureserver.net/dgrid50/5
Demo database: https://sg2nlsmysqladm1.secureserver.net/dgrid50/35


IMGUR Client ID: b276d4ab0ef473f
Secret: 97590b828d8db3fdc12f94d32d0e9a0fd6feca71
 _________________________________________
|                                         |
|  $ss_user is the ServerSide User ID     |
|    It comes from serverSideUserId.php   |
|_________________________________________|



========csSignedRequest vs SignedRequest
 ____________________________________________________________________________________________

  We need to use csSigned request on Facebook's GetLoginStatus to be able to securely
   send users details to the server.

   We need to use Signed request in all other instances, where we are dealing with Facebook.
   Signed request contains more information AND also getLoginStatus will return an error
   if the user is accessing our app as a fan page rathe than a logged in user. therefore we
   won't ever get csSignedRequest, but SignedRequest will still be POSTed.
_____________________________________________________________________________________________

*/

//Checks hostname match to determine if we are on Dango.co.nz or dgo.nz
function checkHostMatch($hostName, $hostUrl){
   $urlBeingAccessed = parse_url($hostUrl);
   $urlBeingAccessed = $urlBeingAccessed['path'];
   if(strpos($urlBeingAccessed, $hostName) !== false){
      return true;
   }
   return false;
}

function checkDebug($hostUrl){
   GLOBAL $GLOBALS;
   $arr = explode(".", $hostUrl);
   if($arr[0] == "debug"){
      $GLOBALS['debug'] = true;
      return true;
   }
   return false;
}

$planeMode = false;
$sendAutoPostsToDangopage = true;

if($planeMode){
  $jqueryLink = "js/";
  $jqueryUi = $jqueryLink;
  $jqueryUiCss = "css/";
  $fbjsExtention = "_offline";
}else{
  $jqueryLink = "//code.jquery.com/";
  $jqueryUi = "//code.jquery.com/ui/1.10.4/";
  $jqueryUiCss = "//code.jquery.com/ui/1.10.4/themes/smoothness/";
  $fbjsExtention = "";
}


$today = date("Y-m-d H:00");
$dateTime = $today;
$todayServer = $today; //Hacky for now.
$PayPalSandBoxMode    = false; // sandbox or live
$BraintreeSandboxMode   = false;

// $todayServer = strtotime($today); // -7 hours to match time at GoDaddy
// $todayServer = date("Y-m-d H:00", $todayServer);

$GLOBALS['debug'] = false;
$localDebug = false;

$accessingFromLocalhost = false;

//My FB Profiles
$allenUserId = "1080073004"; //Allen Shegay
$jamieShaw = "100005285192855"; //Jamie Shaw (dandy andy)
$johnSmithUserId = "100005192885393"; //John S'mith (Alzar Norton)
$jakeFiddlean = "100008008118246"; //Jake Fiddlean (fiddlyjake@hotmail.com / 1fiddlydoo)

$testUsersArray = array($allenUserId, $jamieShaw, $johnSmithUserId, $jakeFiddlean);
$fb_app_admins_array = array($jamieShaw); // array of admins allowed Fb user id
$giveawaysUserIsAdminOf = '';

if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['SERVER_NAME'] == "localhost"){

   $accessingFromLocalhost = true;

   /*Testing fb credentials and DB*/
   $AppId      = '669521669729944'; // Facebook Application ID .. can be got from http://developers.facebook.com/apps
   $AppSecret  = '60685b6c8859e3cc2439bdf78b2284e1'; // Facebook Application Secret key.. can be got from http://developers.facebook.com/apps
   $ServerPath = "http://localhost/dango/";
   $DangoServerPath = "http://localhost/dango/"; //Should this be //www.dango.co.nz ??

   $fullserverpath = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

   $ajaxRequestHost = 'localhost/dango/'; // The host from which the ajax request is made
   $ajaxRequestAnotherHost = 'localhost/dango/'; // The another host from which the ajax request is made

   /*Localhost DB*/
   $db_Host = 'localhost'; // Database Host name eg localhost
   $db_User = 'root'; // Database User name eg my_fblike
   $db_Pass = 'root'; // Database Password eg xfl6723!
   $db_Name = 'testRig'; // Database name eg fblike

   $localDebug = true;
   $GLOBALS['debug'] = true;

   $PayPalSandBoxMode    = true; // sandbox because we're on local
   $BraintreeSandboxMode = true;
}else{
   $AppId      = '573160819407168'; // Facebook Application ID .. can be got from http://developers.facebook.com/apps
   $AppSecret  = 'a8ef2fb1b003d6237661dfb88eed69de'; // Facebook Application Secret key.. can be got from http://developers.facebook.com/apps
   $ServerPath = "https://www.dango.co.nz/";
   $DangoServerPath = "http://www.dango.co.nz/"; //Should this be //www.dango.co.nz ??

   $swipeMerchantId = "1CECFB64166559";
   $swipeAPIKey = "5c5856dca03be3a7aae6b3cb9b0fc8970b11e31dd0b65d4aee76202e36e5f2d8";

   $fullserverpath = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

   $ajaxRequestHost = 'https://www.dango.co.nz/'; // The host from which the ajax request is made
   $ajaxRequestAnotherHost = 'https://www.dango.co.nz/'; // The another host from which the ajax request is made

   if (checkHostMatch("dgo", $_SERVER['HTTP_HOST'])) {
       //Accessing the giveaway
       $db_Host = 'localhost'; // Database Host name eg localhost
       $db_User = 'dangoUser'; // Database User name eg my_fblike
       $db_Pass = 'Nr29jm@11'; // Database Password eg xfl6723!
       $db_Name = 'dango'; // Database name eg fblike
   }else if (checkHostMatch("dango", $_SERVER['HTTP_HOST']) || $_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['SERVER_NAME'] == "localhost") {
      //Accessing merchantside (via Dango) or via Localhost
      $db_Host = '160.153.48.193'; // Database Host name eg localhost
      $db_User = 'dangoUser'; // Database User name eg my_fblike
      $db_Pass = 'Nr29jm@11'; // Database Password eg xfl6723!
      $db_Name = 'dango'; // Database name eg fblike
   }
}

if(checkDebug(($_SERVER['HTTP_HOST']))){   //if we are accessing through debug mode
   if (checkHostMatch("dgo", $_SERVER['HTTP_HOST'])) {
       //Accessing the giveaway
       $db_Host = 'localhost'; // Database Host name eg localhost
       $db_User = 'dangoUser'; // Database User name eg my_fblike
       $db_Pass = 'Nr29jm@11'; // Database Password eg xfl6723!
       $db_Name = 'dangoDebug'; // Database name eg fblike

   }else if (checkHostMatch("dango", $_SERVER['HTTP_HOST']) || $_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['SERVER_NAME'] == "localhost") {
      //Accessing merchantside (via Dango) or via Localhost
      $db_Host = '160.153.48.193'; // Database Host name eg localhost
      $db_User = 'dangoUser'; // Database User name eg my_fblike
      $db_Pass = 'Nr29jm@11'; // Database Password eg xfl6723!
      $db_Name = 'dangoDebug'; // Database name eg fblike
   }
   $PayPalSandBoxMode  = true; // sandbox because we're debugging
   $BraintreeSandboxMode = true;
}


/*Dango Page token that never expires (for posting stuff to the FB page)*/
//Documentation on getting a Long-lived page token:
//https://developers.facebook.com/docs/facebook-login/access-tokens  --- section: Extending Page Access Tokens
//Summary: Generate a User-Access-Token - retrieved from JS response.
//Exchange this token for a Long-Lived token: https://graph.facebook.com/oauth/access_token?%20client_id=[APP_ID]&%20client_secret=[APP_SECRET]&%20grant_type=fb_exchange_token&%20fb_exchange_token=[SHORT_LIVED_TOKEN]
//Exchange long lived token for permanent page token: https://graph.facebook.com/v2.2/[USER_ID]/accounts?access_token=[LONG_LIVED_TOKEN]
//======NOTE:======
//IF you UNINSTALL the app, make sure to click 'Log In' at the top of xdrGetGiveaways.php - it has an additional permission - only for you that lets you post to your own page.....
$dangoFanPageTokenNoExpiry = "EAAIJSWcKOUABAJ97A9mpwIdeDEPzz39Mtl38DLn87WFQely69bra94jQhZBlAz0AhJL8BRtGpWPAVXMtJZCgR2NnD1bUZA91ZBl3HyD3CZBAP4FxXwmmZBa4qDiuH6mngewlZBcKGVWFJZAnAeWBoDLfuuiAeHqFWdEZD";

/*PayPal */
$PayPalApiUsername    = $PayPalSandBoxMode ? 'jaffnae-facilitator_api1.hotmail.com' : 'jaffnae_api1.hotmail.com'; //PayPal API Username
$PayPalApiPassword    = $PayPalSandBoxMode ? '1366410803' : 'JD2GRETG3HUPD779'; //Paypal API password
$PayPalApiSignature   = $PayPalSandBoxMode ? 'AFcWxV21C7fd0v3bYYYRCpSSRl31AGQXoKXC3MsMZ1BfbYa96us5D9Fc' : 'AFcWxV21C7fd0v3bYYYRCpSSRl31ACTQr.84YWgaZ8ylQUsApG7OVex9'; //Paypal API Signature
$PayPalApiEndpoint    = $PayPalSandBoxMode ? 'https://api-3t.sandbox.paypal.com/nvp' : 'https://api-3t.paypal.com/nvp';
$PayPalReturnURL      =  $DangoServerPath.'index.php?page=paypal/thanksPremium'; //Point to process.php page
$PayPalCancelURL      =  $DangoServerPath.'index.php?page=paypal/premiumCancel'; //Cancel URL if user clicks cancel
$PayPalLandingPage    =  'Billing';

/*END PayPal*/

/*Braintree */

$BraintreeEnvironment = $BraintreeSandboxMode   ? 'sandbox' : 'production';
$BraintreeMerchantId = $BraintreeSandboxMode    ? '7wh7d68dspgmvxhm' : 'fghn9k7zj2r9hcps';
$BraintreePublicKey = $BraintreeSandboxMode     ? 'dx35nx3fg67hvbn6' : 'hdbxt23q7hrh769c';
$BraintreePrivateKey = $BraintreeSandboxMode    ? 'fb2234ff70fb439aaf5572386a894cdc' : '3d5040c078306a2ed5ff31072ea9ed06';

/*END Braintree */


$UploadedImagePath = $ServerPath."uploads/";

$private_key = "t%3(3-()tvBIG";

function fprand($intMin,$intMax,$intDecimals) {

  if($intDecimals) {

    $intPowerTen=pow(10,$intDecimals);

    return rand($intMin,$intMax*$intPowerTen)/$intPowerTen;

  }

  else

    return rand($intMin,$intMax);

}

$merchantPremiumCost = 16.99;


$num_Allowed_Shares = 3000;
// Shares are near-infinite for now. In the future we access friend lists and check if they have fewer than
// X amount of friends then if they have less - we say it appears your profile is too new or something
// like that.

$GoogleAdsenseKey = "";

$ForLikeButtonFBpage = "http://www.facebook.com/pages/Power-Gift/263488410367378";

$paypal_link = "https://www.paypal.com/cgi-bin/webscr"; // For testing we change it to https://www.sandbox.paypal.com/cgi-bin/webscr
//and For live we have to make it https://www.paypal.com/cgi-bin/webscr
//
$paypal_busnes_email = "jaffnae@hotmail.com";  // Uncomment this for Live
//$paypal_busnes_email = "maizazbarki-facilitator@gmail.com"; // Using for sandbox paypal testing

$paypal_admin_notif_email = "admin@powergift.co.nz"; //For Live we can use admin@powergift.co.nz


$admin_auctions_per_page = 50;  // Give it in numeric, not string okay

$numberOfQuestionsPerGiveaway = 4; //Total Number of questions allowed in a giveaway

$serveTime = date("d-m-Y H:00"); //Heroku returns server time in GMT.

// Email configuration variables
$sendGridCredentials = array();
//The original SendGrid Account credentials
$sendGridCredentials["sendGridStandardAccount"] = array("sendGridUsername" => "dango", "sendGridPassword" => "Shegaypower1");
//Business-focused Sendgrid Account credentials
$sendGridCredentials["sendGridBusinessAccount"] = array("sendGridUsername" => "dango_business_focused", "sendGridPassword" => "Shegaypower1");

$sendgridHost = "smtp.sendgrid.net";
$sendgridPort = 587;


$smtp_host = "smtpout.asia.secureserver.net"; // SMTP server

$smpt_port = 80;                    // set the SMTP port for your server

$smpt_email_username = "dango@dango.co.nz"; // SMTP account username

$smpt_email_pass     = "Nr29jm@11";        // SMTP account password


$sendNewslettersLive = "yes";


$set_from_email      = 'dangogiveaways@gmail.com';

$set_from_name       = 'Dango Giveaways';

$set_from_name_allen       = 'Allen from Dango';


$replyto_email      = 'dangogiveaways@gmail.com';

$replyto_name       = 'Dango';

$mail_altbody       = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
// Email configuration variables //
$random_offer = 0; // just put because FBJS.php was giving warning and whole javascript code wasn't running

$ActiveNZPopup = false; // false for inactive and true for active

$dealLockTime = 3 ; // in minutes

$CalculatedTimeForcookie =  time() + 60 * $dealLockTime; // You don't need to change it okay

$keyForMerchantReferrals = "5%G@G0E";


include_once('globaldatabase.php'); // A database related functions in this file

$availableSubdomains = array("dine", "dango");
$subdomain = getSubdomain($_SERVER['HTTP_HOST']);

//=====GLOBAL FUNCTIONS=====\\

//Decode signed request
function parse_signed_request($signed_request, $secret) {
    list($encoded_sig, $payload) = explode('.', $signed_request, 2);

    // decode the data
    $sig = base64_url_decode($encoded_sig);
    $data = json_decode(base64_url_decode($payload), true);

    // confirm the signature
    $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
    if ($sig !== $expected_sig) {
        error_log('Bad Signed JSON signature!');
        return null;
    }
    return $data;
}

function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
}
//END: Decode signed request

/**
 * Send debug code to the Javascript console
 */
function debug($data) {
    if(is_array($data) || is_object($data))
    {
       echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
    } else {
       echo("<script>console.log('PHP: ".$data."');</script>");
    }
}

function getCountryInfo($ipAddress){
   $ip = isset($ipAddress) ? $ipAddress : $_SERVER["REMOTE_ADDR"]; //NZ country code
   $ip_array = explode(", ", $ip);
   $real_ip = end($ip_array);
   $request_path = get_data("http://www.geoplugin.net/json.gp?ip=".$real_ip);
   $arr = json_decode($request_path);
   return $arr;
}

function getCountryName($ipAddress){
   $clientCountry = isset($ipAddress) ? getCountryInfo($ipAddress) : getCountryInfo();
   return $clientCountry -> geoplugin_countryName;
}


/*Const String*/
include_once('vars.php');



?>

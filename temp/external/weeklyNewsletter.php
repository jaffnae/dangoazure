<?php
include_once("config.php");
db_connect();

	$message = "";
	$messageHeader = "";
	$messageFooter = "";
	$todaysDateCompatible = date("d-m-Y H:i"); // The way we log giveaways - their dates are different.
	$todaysDateCompatible = strtotime($todaysDateCompatible);
	//$lastSevenDays = strtotime($todaysDateCompatible);
	$lastSevenDays = $todaysDateCompatible - 604800;
	//$lastSevenDays = date("d-m-Y H:i", $lastSevenDays);
	$widthOfGiveawayImage = "250";
	$heightOfGiveawayImage = "250";
	$i =0;


	include_once("external/giveawaysInNewsletters.php");

foreach ($giveawaysArray as &$value){

	$getThisWeeksGiveaways = mysql_query("SELECT * FROM giveaways WHERE shortUrl = '$value' LIMIT 1 ");
	while($row = mysql_fetch_assoc($getThisWeeksGiveaways)){
		$fontSize = 19;

		if($i%2 == 0){
			$tdClass = "first_td";
			$message .= '<tr class="'.$i.' tdParentContainer">';
		}else{
			$tdClass = "second_td";
		}

		if(str_word_count($row['title']) > 15){
			$fontSize = 16;
		}

		//Adding individual giveaways
		$message .= '
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr >
							<td >
								<a href="'.$row['shortUrl'].'" class="pickImg" target="_blank" style="color: #336699; '.$additionalStyle.' border-bottom: 1px solid #ccc; width: '.$widthOfGiveawayImage.'px; height: '.$widthOfGiveawayImage.'px; max-height: '. $widthOfGiveawayImage.'px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="'.$row['image0'].'" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline;  height: '. $widthOfGiveawayImage.'px; max-height: '. $widthOfGiveawayImage.';">
								</a>
							</td>
						</tr>
						<tr >
							<td class="'.$tdClass.' commonTdClass" style="text-align: center; vertical-align: middle;" id=txt_'.$i.'>
								<a href="'.$row['shortUrl'].'" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: '.$widthOfGiveawayImage.'px;font-style: italic;color: #58BBC5;font-size: '.$fontSize.'px;">'.$row['title'].'
									<br /><br />
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br /><br />
			</td>';
	   if($i%2 != 0){
			$message .='</tr>'; //Starting new row
		}

		$i++;
	}

}// END: For loop


//Beginning of the newsletter
$messageHeader = '
	<html>
	<head></head>
	<body style="background: #eaeaea;">
	<style>
		.buttonGenerateImages{border:1px solid black; padding: 5px; background: #ddd; display: inline-block; margin: 25px;}
	</style>
	<div class="buttonGenerateImages">
		Generate Images
	</div>
	<textarea class="refinedHtml">


	</textarea>


		<div class="newsletterContainer">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;height: 100% !important;width: 100% !important;">
			<tbody><tr>
			<td align="center" valign="top">

			   <table border="0" cellpadding="0" cellspacing="0" width="566" id="templateContainer" style="border: 0px solid #DDDDDD;background-color: #fff; -webkit-box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.6); -moz-box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.6); box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.6);">
			       <tbody>
			       <tr>
			           <td align="center" valign="top">
			               <table border="0" cellpadding="10" cellspacing="0" width="566">
			                   <tbody><tr>
			                       <td valign="top" class="bodyContent" style="background-color: #fff;">

			                           <table border="0" cellpadding="10" cellspacing="0" width="100%">
			                               <tbody><tr>
			                                   <td valign="top">
			                                       <div style="color: #505050;font-family: Tahoma, Geneva, sans-serif;font-size: 12px;line-height: 150%;text-align: left;">';


//Dango logo onwards...
$messageHeader .= '
																<table background="" class="hTitle" style="width: 100%;text-align: center;margin-bottom: 10px;font-family: Tahoma, Geneva, sans-serif;">
																	<tbody>
																	  <tr>
																		  <td width="20%">
																				<img src="http://i.imgur.com/QAJQroZ.png" alt="Dango Giveaways Logo"/>
																		  </td>
																		  <td width="80%" style="font-family: Tahoma, Geneva, sans-serif; text-align: right;">
																			  <span style="font-size:11px;color: #58BBC5;font-style: italic;">Dango - giveaways by Kiwi businesses</span><br>
																		  </td>
																	  </tr>
																	</tbody>
																</table>
																												<br>';


//Featured Giveaway Image
$messageHeader .= '
	<table style="width: 100%;text-align: center;margin-bottom: 5px;font-family: Tahoma, Geneva, sans-serif;">
		<tbody>
			<tr>
				<td style="font-family: Tahoma, text-alignt: center; background: #58BBC5; Geneva, sans-serif;">
		      	<span class="h1" style="color: #fff;font-family: Tahoma, Geneva, sans-serif;background: #58BBC5; font-size: 42px;font-weight: normal;line-height: normal;text-align: center;padding: 0 10px;">Hot Giveaway of the Week</span>
				</td>
		  </tr>
	  </tbody>
	</table>
	 <a href="'.$featuredGiveaway.'" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">
    	<img align="none" alt="" class="ofertaDestacada" height="400" src="'.$featuredGiveawayImg.'" style="border: 1px solid #c7c2c2;width: 525px;height: 400px;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;" width="525">
	 </a>

	 <br>
	 <br><br>
	 <br>
	 <hr style="border-top: dotted 1px;" />
	 <br>
	 <br>
	 <br>

	 <table class="hTitle" style="width: 100%;text-align: center;margin-bottom: 10px; font-family: Tahoma, Geneva, sans-serif;">
	    <tbody>
	       <tr>
	          <td style="font-family: Tahoma, Geneva, sans-serif; background: #58BBC5;">
	             <span class="h1" style="color: #fff;font-family: Tahoma, Geneva, sans-serif;font-size: 49px;font-weight: normal;line-height: normal;text-align: center;padding: 0 10px;">Other great giveaways</span>
	          </td>
	       </tr>
	    </tbody>
	 </table>

	 <br>';



//The rest of the giveaways begin...
$messageHeader .= '
	<table cellpadding="5" style="min-width: 100%">';


//Footer
$messageFooter ='

																		</table>

																		</div></td>

			                                       </tr>
			                                       </tbody></table>
			                                   </td>
			                               </tr>
			                           </tbody></table>

			                       </td>
			            </tr>';

$messageFooter .= '<tr>
							<td>';

$messageFooter .=		 '<br /><br /><br />
								<table class="hTitle" style="width: 100%;text-align: center;font-family: Tahoma, Geneva, sans-serif;margin-bottom: 10px;">
								   <tbody>
								      <tr>
								         <td style="font-family: Tahoma, Geneva, sans-serif; background: #58BBC5;">

											   <span class="h1" style="color: #fff;font-family: Tahoma, Geneva, sans-serif; line-height: normal; font-size: 36px;text-align: center;">Last week\'s winners!</span>
								         </td>
								      </tr>
								   </tbody>
								</table>                                                                                                <br />
								<table style="width: 100%;text-align: left; font-size:11px;margin-bottom: 5px;font-family: Tahoma, Geneva, sans-serif; text-align: center;">
								   <tbody>
								      <tr>';
											//winners start here
											$winnerCount = 0;
											foreach ($winnersPuidxArray as $winnerPuidx) {
												$q = mysql_query("SELECT winnerName FROM winners WHERE puidx = '$winnerPuidx' LIMIT 1");
												while($rowWinner = mysql_fetch_assoc($q)){
												$winnerName = $rowWinner['winnerName'];
												}

												$q = mysql_query("SELECT title, shortUrl FROM giveaways WHERE puidx = '$winnerPuidx' LIMIT 1");
												while($rowGiveaway = mysql_fetch_assoc($q)){
												$winnerGiveawayTitle = $rowGiveaway['title'];
												$winnerUrl = $rowGiveaway['shortUrl'];
												}

$messageFooter .='
												<td style="text-align: center; width: 50%; font-size: 11px;">
													<b><a href="'.$winnerUrl.'" target="_blank">'.$winnerName.'</a></b>
													<br><i style="font-size: 11px;"><a href="'.$winnerUrl.'" target="_blank">'.$winnerGiveawayTitle.'</a></i>.
													<br>
													<br>
													<br>
												</td>';

												$winnerCount++;

												if($winnerCount > 0 && $winnerCount%2 ==0){
$messageFooter .= '
													</tr>
													<tr>
													';
												}
											}

$messageFooter .='
							</td>
							</tr>';
$messageFooter .='
							<tr>
								<td align="center" valign="top" colspan="2">
									<table border="0" cellpadding="10" cellspacing="0" width="566" id="templateFooter" style="border-top: 0;">
										<tbody>
											<tr>
												<td valign="top" class="footerContent">

													<div style="padding-left: 8px; padding-right: 8px;">
													<hr style="border-top: dotted 1px;" />
													</div>

													<p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 15px 0;">

														<a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">
															<img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
															<strong>See More Giveaways >></strong>
														</a>&nbsp;&nbsp;


													</p>

													<div style="padding-left: 8px; padding-right: 8px;">
													<hr style="border-top: dotted 1px;" >
													</div>

													<p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
														If you received this message in error please disregard and delete this.<br />
														You can also unsubscribe from these emails by following
			                                               <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&uFrm=-userId-" style="color: #aaa;">this link</p>
			                                           </p>


												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>

			               </tbody></table>
			           </td>
			       </tr>




			</tbody>
		</table>
		</div>
	</body>
	</html>';



echo $messageHeader.$message.$messageFooter;

?>

<script src="<?php echo $jqueryLink; ?>jquery-1.11.0.min.js"></script>

<script>
	$(document).ready(function(){
		var imgClass=".pickImg",
			buttonClass=".buttonGenerateImages",
			htmlContainerClass=".refinedHtml",
			newsletterContainerClass=".newsletterContainer";
		console.log("ready");
		$(buttonClass).click(function(){
			console.log("clicked");
			var numberOfImages = $(imgClass).length;
			$(imgClass).each(function(i){
				var currentImage = $(this).find("img"),
					url = currentImage.attr("src");
					console.log("ajax...");
				$.ajax({
					method: "POST",
					url: "save/rescaleNewsletterImages.php",
					data: {url: url, width: "<?php echo $widthOfGiveawayImage; ?>", height: "<?php echo $heightOfGiveawayImage; ?>", urlConverterRequested: true},
					success: function(response){
						console.log(response);

						currentImage.attr("src", response.imgurResponse.data.link);
						if(i == numberOfImages - 1){
							$(htmlContainerClass).val('<html><head></head><body style="background: #eaeaea;">' + $(newsletterContainerClass).html() + '</body></html>');
							alert("done");
						}
					}
				});
			});
		});

		$(".tdParentContainer").each(function(){
			var greatestHeight = 0,
				 firstTd = $(this).find(".first_td").height(),
				 secondTd = $(this).find(".second_td").height();

			if(firstTd > secondTd){
					greatestHeight = firstTd;
			}else{
				greatestHeight = secondTd;
			}
			$(this).find(".commonTdClass").height(greatestHeight);
		});
	});
</script>

<?php

$fromNameAndEmailObject = array();
$fromNameAndEmailObject["name"] = "Kiwi Contests";
$fromNameAndEmailObject["email"] = "dangogiveaways@gmail.com";

//URL to visit if NZ and Unmarked Countries:
//http://www.dango.co.nz/queries/generateEmailJsonFilesAndSend.php?country=new%20zealand&includeUnmarkedCountries=true

$sendGridApiHtml = '
<html>

  <head></head>

  <body style="background: #f3f3f3; margin: 0; padding: 0; text-align: center; ">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 640px !important; background: white;">
      <tbody>
        <tr>
          <td valign="top">
            <img src="http://i.imgur.com/QAJQroZ.png" alt="Dango Giveaways Logo">
          </td>
        </tr>
        <tr>
          <td style="color: #666; padding: 20px;">
            The best giveaways on Dango
          </td>
        </tr>
      </tbody>
    </table>




    <table border="0" cellpadding="0" cellspacing="15px" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 640px !important; background: white;">
      <tbody>


        <!-- =========================================================
                                    Giveaways begin here
              ==========================================================-->
        <!-- First Giveaway row -->
        <tr>
          <td width="285px" style="background-color: #58BBC5">
            <div>
              <table style="width: 285px; border-spacing: 0; min-height: 300px;">
                <tr>
                  <td valign="center">

                    <span style="text-align: center; font-size: 40px; font-weight: bold; color: #fff;">This week\'s giveaways!</span>
                  </td>
                </tr>
              </table>
            </div>
          </td>
          <td width="285px" style="padding: 0;" valign="center">
            <div style="width: 280px; overflow: hidden; min-height: 300px; position: relative; border: 1px solid #ccc;">
              <table style="min-height: 300px; width: 280px; border-spacing: 0;">
                <tr>
                  <td valign="center" style="text-align: center; padding: 10px 0;">
                    <a href="http://dgo.nz/zR42bNZe4087">
                      <img src="http://i.imgur.com/pontfnk.jpg" style="height: auto; max-width: 100%; position: relative; left: 0; top: 0;" />
                    </a>
                    <div style="background: white; padding: 10px 0; color: #cacaca;">
                      <span>Attention Mums! Mumpreneur Prizes Up For Grabs! ! ! ! Pick anything from this image!</span>

                    </div>
                  </td>
                </tr>
              </table>

            </div>
          </td>
        </tr>
        <!-- END: First Giveaway row  -->



        <!-- Second Giveaway row -->
        <tr>
          <td width="280px">
            <div>
              <table style="width: 280px; border-spacing: 0; height: 200px;">
                <tr>
                  <td valign="center">

                    <div style="width: 280px;overflow: hidden; height: 200px; position: relative; border: 1px solid #ccc;">
                      <table style="height: 200px; width: 280px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center;">
                            <a href="http://dgo.nz/zID782bE4097">
                              <img src="http://i.imgur.com/6WNmWMI.jpg" style="width: 100%; position: relative; left: 0; top: -60px;" />
                            </a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; width: 280px; border: 1px solid #ccc; padding: 10px 0; color: #cacaca;">
                    <span>WIN this AMAZING Body Care Pack f...</span>

                  </div>
                  </td>

                </tr>
              </table>
            </div>
          </td>
          <td width="280px">
            <div>
              <table style="width: 280px; border-spacing: 0; height: 200px;">
                <tr>
                  <td valign="center">

                    <div style="width: 280px; overflow: hidden; height: 200px; position: relative; border: 1px solid #ccc;">
                      <table style="height: 200px; width: 280px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center;">
                            <a href="http://dgo.nz/cNZiRmArnpE4069">
                              <img src="http://i.imgur.com/zQ1HrSj.jpg" style="width: 100%; position: relative; left: 0; top: -26px;" />
                            </a>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; width: 280px; border: 1px solid #ccc; padding: 10px 0; color: #cacaca;">
                    <span> * * Which prize would you pick fro...</span>

                  </div>
                  </td>

                </tr>
              </table>
            </div>
          </td>
        </tr>
        <!-- END: Second Giveaway row -->


        <!-- Third Giveaway row -->
        <tr>

          <td width="280px" style="padding: 0;" valign="center">
            <div style="width: 280px; overflow: hidden; min-height: 300px; position: relative; border: 1px solid #ccc;">
              <table style="min-height: 400px; width: 280px; border-spacing: 0;">
                <tr>
                  <td valign="center" style="text-align: center; padding: 10px 0;">
                    <a href="http://dgo.nz/uFWkC7vI4081">
                      <img src="http://i.imgur.com/QF8mYMy.jpg" style="width: 100%; max-width: none; position: relative; left: 0;  top: 0;" />
                    </a>
                  </td>
                </tr>
              </table>

            </div>
            <div style="background: white; width: 280px; border: 1px solid #ccc; padding: 10px 0; color: #cacaca;">
              <span>WIN this INCREDIBLE A2 Print --- Dart Art --- worth $200</span>
            </div>
          </td>
          <td width="280px" style="background-color: #58BBC5">
            <div>
              <table style="width: 280px; border-spacing: 0; min-height: 300px;">
                <tr>
                  <td valign="center">

                    <span style="text-align: center; font-size: 40px; font-weight: bold; color: #fff;">Winners last week:</span><br />

                    <span style="text-align: center; font-size: 12px; font-weight: bold; color: #fff;">
                    <br />
                    <br />
                      Ilana Gutnik<br />
                      Denise Martin<br />
                      Emma Jackson<br />
                      Lois Windsor<br />
                      Marthapearl Clarke-wicks<br />
                      Callie Lucas<br />
                    </span>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <!-- END: Third Giveaway row  -->





        <!-- Fourth Giveaway row -->
        <tr>

          <td style="padding: 0;" colspan="2" valign="center">
            <div style="width: 100%; overflow: hidden; height: 300px; position: relative; border: 1px solid #ccc;">
              <table style="min-height: 300px; border-spacing: 0;">
                <tr>
                  <td valign="center" style="text-align: center; padding: 10px 0;">
                    <a href="http://dgo.nz/RIMZTb4z4098">
                      <img src="http://i.imgur.com/QSPbitn.jpg" style="width: 100%; position: relative; left: 0;  top: 0;" />
                    </a>
                  </td>
                </tr>
              </table>

            </div>
            <div style="width: 100%; background: white; border: 1px solid #ccc; padding: 10px 0;  color: #cacaca;">
              <span>60Min Hot Stone Massage</span>

            </div>
          </td>
        </tr>
        <!-- END: Fourth Giveaway row  -->


        <!-- Fifth Giveaway row -->
        <tr>
          <td colspan="2">


            <table>
              <tr>

              <td width="195px">
                    <div style="width: 195px; overflow: hidden; height: 150px; position: relative; border: 1px solid #ccc;">
                      <table style="min-height: 100px; width: 195px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center; padding: 10px 0;">
                            <a href="http://dgo.nz/cKpmrLzH4108">
                              <img src="http://i.imgur.com/g2Ukp9N.jpg" style="width: 100%; max-width: none; position: relative; left: 0;  top: -30px;" />
                            </a>

                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; border: 1px solid #ccc; padding: 10px 0; width: 100%; color: #cacaca;">
                      <span>Gift Bag of 8 Asst Melts</span>

                    </div>
                  </td>


<td width="195px">
                    <div style="width: 195px; overflow: hidden; height: 150px; position: relative; border: 1px solid #ccc;">
                      <table style="min-height: 100px; width: 195px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center; padding: 10px 0;">
                            <a href="http://dgo.nz/sQKWPk7o4104">
                              <img src="http://i.imgur.com/1Jpp1B5.jpg" style="width: 100%; max-width: none; position: relative; left: 0;  top: -20px;" />
                            </a>

                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; border: 1px solid #ccc; padding: 10px 0; width: 100%; color: #cacaca;">
                      <span>++WIN++ 4 tickets to t...</span>

                    </div>
                  </td>

<td width="195px">
                    <div style="width: 195px; overflow: hidden; height: 150px; position: relative; border: 1px solid #ccc;">
                      <table style="min-height: 100px; width: 195px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center; padding: 10px 0;">
                            <a href="http://dgo.nz/7bwok1XD4103">
                              <img src="http://i.imgur.com/c0HeUvp.jpg" style="width: 100%; max-width: none; position: relative; left: 0;  top: -40px;" />
                            </a>

                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; border: 1px solid #ccc; padding: 10px 0; width: 100%; color: #cacaca;">
                      <span>Dragonfly Pendant...</span>

                    </div>
                  </td>



              </tr>
            </table>


            </td>
        </tr>
        <!-- END: Fifth Giveaway row  -->


        <!-- Sixth Giveaway row -->
        <tr>
          <td colspan="2">
            <table>
              <tr>

              <td width="195px">
                    <div style="width: 195px; overflow: hidden; height: 150px; position: relative; border: 1px solid #ccc;">
                      <table style="min-height: 100px; width: 195px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center; padding: 10px 0;">
                            <a href="http://dgo.nz/7qA4xrma4106">
                              <img src="http://i.imgur.com/HclKKH8.jpg" style="width: 100%; max-width: none; position: relative; left: 0;  top: 0px;" />
                            </a>

                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; border: 1px solid #ccc; padding: 10px 0; width: 100%; color: #cacaca;">
                      <span>A GREAT Prize Bundle: F...</span>

                    </div>
                  </td>


<td width="195px">
                    <div style="width: 195px; overflow: hidden; height: 150px; position: relative; border: 1px solid #ccc;">
                      <table style="min-height: 100px; width: 195px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center; padding: 10px 0;">
                            <a href="http://dgo.nz/aul6hqRW4110">
                              <img src="http://i.imgur.com/1O6Gm5Z.png" style="width: 100%; max-width: none; position: relative; left: 0;  top: -9px;" />
                            </a>

                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; border: 1px solid #ccc; padding: 10px 0; width: 100%; color: #cacaca;">
                      <span> Awesome kids\' books prize</span>

                    </div>
                  </td>

<td width="195px">
                    <div style="width: 195px; overflow: hidden; height: 150px; position: relative; border: 1px solid #ccc;">
                      <table style="min-height: 100px; width: 195px; border-spacing: 0;">
                        <tr>
                          <td valign="center" style="text-align: center; padding: 10px 0;">
                            <a href="http://dgo.nz/kJDgypYt4095">
                              <img src="http://i.imgur.com/kntE9M0.jpg" style="width: 100%; max-width: none; position: relative; left: 0;  top: 0px;" />
                            </a>

                          </td>
                        </tr>
                      </table>
                    </div>
                    <div style="background: white; border: 1px solid #ccc; padding: 10px 0; width: 100%; color: #cacaca;">
                      <span> Toddler Parenting Wor...</span>

                    </div>
                  </td>



              </tr>
            </table>


            </td>
        </tr>
        <!-- END: Sixth Giveaway row  -->


        <!--
            =========================================================
                                    Giveaways END here
            ==========================================================
        -->



        <!-- Footer -->
        <tr>
          <td align="center" valign="top" colspan="2">
            <div style="padding-left: 8px; padding-right: 8px;">

              <br />
              <br />

              <hr style="border-top: dotted 1px;">
            </div>
            <p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 15px 0;">

              <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">
                <img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
                <strong>See More Giveaways &gt;&gt;</strong>
              </a>&nbsp;&nbsp;
            </p>

            <div style="padding-left: 8px; padding-right: 8px;">
              <hr style="border-top: dotted 1px;">
            </div>

            <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
              If you received this message in error please disregard and delete this.
              <br> You can also unsubscribe from these emails by following
              <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-" style="color: #aaa;">this link</a></p>
            <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-" style="color: #aaa;">
              <p></p>
            </a>
          </td>
        </tr>
      </tbody>
    </table>

  </body>

</html>




';
?>

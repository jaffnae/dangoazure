<?php 

//How will we prevent people from accessing this file directly?

include_once("../config.php");
require_once('../api/class.phpmailer.php');

$db_con = db_connect();
$data = array();
       
$fname = "";
$lname = "";
$uid = "";
$email = "";
$textBodyFileName = $_POST["textBodyFileName"];

include_once($textBodyFileName);

//Commented out so we don't send it again by accident
$emailList = $_POST['contacts'];
$emailList = json_decode($emailList, true);

$subject = $_POST['subject'];

foreach ($emailList as &$value) {

   $fname = $value['fname'];
   $lname = $value['lname'];
   $uid = $value['uid'];
   $email = $value['email'];

  
   $bodyText = makeBodyText($fname, $lname, $uid);

   $body = $bodyText;
   $to = $email;

   $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
   $mail->IsSMTP(); // telling the class to use SMTP
   $mail->Host       = $smtp_host;
   $mail->SMTPAuth   = true;                  // enable SMTP authentication
   $mail->Host       = $smtp_host;
   $mail->Port       = $smpt_port;
   $mail->Username   = $smpt_email_username;
   $mail->Password   = $smpt_email_pass;
   $mail->AddAddress($to, '');
   $mail->SetFrom($set_from_email, $set_from_name);
   $mail->AddReplyTo($replyto_email, $replyto_name);
   $mail->Subject = $subject;
   $mail->AltBody = $mail_altbody;
   $mail->MsgHTML($body);

   if($mail->Send()){
      $data['error'] = "0";
   }else{
      $data['error'] = "1";
   }
   
header('Content-Type: application/json');
echo json_encode($data); //Sending AJAX response.


}

//mysql_close($db_con);

?>
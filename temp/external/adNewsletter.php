<?php 
    function makeBodyText($fname, $lname, $uid)
    {
       return '
        <html><head></head><body><div id="tc_central" c-style="bgcolor" onclick="closeAll();">
        <!--[[display]]
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Escape</title>[[display]]-->

<style type="text/css">

div, p, a, li, td { -webkit-text-size-adjust:none; }

*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}

.ReadMsgBody
{width: 100%; background-color: #ffffff;}
.ExternalClass
{width: 100%; background-color: #ffffff;}
/*body*/ #tc_central{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
/*html*/ #tc_central{width: 100%; background-color: #ffffff;}

@font-face {
    font-family: "proxima_novalight";src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot");src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot?#iefix") format("embedded-opentype"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.woff") format("woff"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.ttf") format("truetype");font-weight: normal;font-style: normal;}

@font-face {
    font-family: "proxima_nova_rgregular"; src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot");src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot?#iefix") format("embedded-opentype"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.woff") format("woff"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.ttf") format("truetype");font-weight: normal;font-style: normal;}

@font-face {
    font-family: "proxima_novasemibold";src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot");src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot?#iefix") format("embedded-opentype"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.woff") format("woff"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.ttf") format("truetype");font-weight: normal;font-style: normal;}
    
@font-face {
    font-family: "proxima_nova_rgbold";src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot");src: url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot?#iefix") format("embedded-opentype"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.woff") format("woff"),url("http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.ttf") format("truetype");font-weight: normal;font-style: normal;}



p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.image280 img {width: 280px; height: auto;}
.logo img {width: 112px; height: auto;}
.image143 img {width: 143px; height: auto;}
.image215 img {width: 215px; height: auto;}
.image195 img {width: 195px; height: auto;}
.image600 img {width: 600px; height: auto;}
.icon28 img {width: 28px; height: auto;}
#icon14 img {width: 14px; height: auto;}


 

</style>


<!-- @media only screen and (max-width: 640px) 
           {*/
           -->
<!--LARGEONEO
<style type="text/css">
        /*body*/ #tc_central{width:auto!important;}
        table[class=full] {width: 100%!important; clear: both; }
        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
        .erase {display: none;}
        .buttonScale {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
        .fontSmall {font-size: 55px!important; line-height: 62px!important; letter-spacing: 8px!important;}
        .h80 {height: 90px!important;}
        .h50 {height: 50px!important;}
        .h30 {height: 40px!important;}
        table[class=scale25] {width: 23%!important;}
        table[class=scale25Right] {width: 23%!important;}
        table[class=scale33] {width: 30%!important;}
        table[class=scale33Right] {width: 30%!important;}
        .image143 img {width: 100%!important; height: auto;}
        .image195 img {width: 100%!important; height: auto;}
        table[class=w5] {width: 3%!important; }
        table[class=w10] {width: 5%!important; }
        .pad0 {padding: 0px!important;}
        .image600 img {width: 100%!important;}
        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
        .h15 {width: 100%!important; height: 15px!important;}
        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        .h20 {width: 100%!important; height: 40px!important;}
        
</style>
CLARGEONE-->
<!--

@media only screen and (max-width: 479px) 
           {
           -->
<!--SMALLONEO
<style type="text/css">
        /*body*/ #tc_central{width:auto!important;}
        table[class=full] {width: 100%!important; clear: both; }
        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
        .erase {display: none;}
        .buttonScale {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
        .fontSmall {font-size: 42px!important; line-height: 48px!important; letter-spacing: 5px!important;}
        .fontSmall2 {font-size: 34px!important; line-height: 38px!important; letter-spacing: 2px!important;}
        .h80 {height: 80px!important;}
        .h50 {height: 50px!important;}
        .h30 {height: 30px!important;}
        table[class=scale25] {width: 100%!important; clear: both;}
        table[class=scale25Right] {width: 100%!important; clear: both;}
        table[class=scale33] {width: 100%!important; clear: both;}
        table[class=scale33Right] {width: 100%!important; clear: both;}
        .image143 img {width: 100%!important; height: auto;}
        .image195 img {width: 100%!important; height: auto;}
        .h40 {height: 40px!important;}
        .pad0 {padding: 0px!important;}
        .image600 img {width: 100%!important;}
        .textLeft {width: 100%!important; text-align: left!important; float: left!important;}
        .h15 {width: 100%!important; height: 15px!important;}
        td[class=pad2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        .h20 {width: 100%!important; height: 40px!important;}
                
        }
</style>
CSMALLONE-->
<style>    
    @media screen and (-webkit-min-device-pixel-ratio: 0) {
        div[class="overlay"]{position: relative;}
        div[class="dots"]{display: block !important; opacity: 0.4 !important;}
        div[class="dot1"], div[class="dot2"], div[class="dot3"], div[class="dot4"], div[class="dot5"], div[class="dot6"], div[class="dot7"], div[class="dot8"], div[class="dot9"], div[class="dot10"] {
            position: absolute;
            overflow: hidden;
            display: block !important;
            width:4px !important;
            height:4px !important;
            background:#ffffff;
            border-radius:10px; -webkit-border-radius: 10px; moz-border-radius: 10px;
        }
        .ExternalClass div[class="ecxdot1"], .ExternalClass div[class="ecxdot2"], .ExternalClass div[class="ecxdot3"], .ExternalClass div[class="ecxdot4"], .ExternalClass div[class="ecxdot5"], .ExternalClass div[class="ecxdot6"], .ExternalClass div[class="ecxdot7"], .ExternalClass div[class="ecxdot8"], .ExternalClass div[class="ecxdot9"], .ExternalClass div[class="ecxdot10"] {display: none !important;height:0px !important;width: 0px !important;}
    }
</style> 

<!--[[display]]</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff;">[[display]]-->

<div id="sort_them" class="ui-sortable">




<!-- Wrapper 3 -->
<!-- Wrapper 3 -->

<!-- Wrapper 2 -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#f5f5f5" c-style="greyBG" style="background-color: white;">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                    
                        
                        
                                    
                        <!-- Wrapper -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%">
                                    
                                    <!-- Centered Title -->
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tbody>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 26px; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold;" t-style="headlines" class="fullCenter" mc:edit="18">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Congratulations to our recent winners:</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="40"></td>
                                        </tr>
                                        <tr>
                                            <td width="100%" align="center">
                                                
                                                <table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100" align="center" height="1" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(229, 229, 229); color: rgb(186, 186, 186);" t-style="textColor"></td>
                                                    </tr>
                                                </tbody></table>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper 2 -->
                        
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table><!-- Wrapper 3 -->

<!-- Wrapper 2 -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#f5f5f5" c-style="greyBG" style="background-color: white;">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                    
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="25"></td>
                            </tr>
                        </tbody></table><!-- End Space -->
                        
                        <!-- Wrapper -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                            <tbody><tr>
                                <td width="100%" align="center">
                                    
                                    <!-- Icon 1 -->
                                    <table width="170" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter">
                                        <tbody><tr>
                                            <td width="150" class="icon30" align="center">
                                                <table width="80" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td align="center" width="100%" height="80" bgcolor="#ffffff" style="border-radius: 100%; text-align: center;"><a href="http://dgo.nz/Im59SJsO2984" style="text-decoration: none;" object="image-editable"><img src="http://i.imgur.com/pNanKZ0.png" width="98" alt="" editable="true" border="0" mc:edit="19" class="jump"></a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; font-weight: bold; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="20">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Ra Crean</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="21">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Greytown Twilight Market Goody Bags. There are 20 gorgeous goody bags, including one super bag...</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20">                                   
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                    <!-- Space -->
                                    <table width="45" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td width="100%" height="40">                                   
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Icon 2 -->
                                    <table width="170" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter">
                                        <tbody><tr>
                                            <td width="150" class="icon30" align="center">
                                                <table width="80" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100%" align="center" height="80" bgcolor="#ffffff" style="border-radius: 100%; text-align: center;"><a href="http://dgo.nz/36CDBpm83001" style="text-decoration: none;" object="image-editable"><img src="http://i.imgur.com/GQW2ogd.png" width="98" alt="" editable="true" border="0" mc:edit="23" class="jump"></a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; font-weight: bold; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="24">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Katie Law</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="25">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>A little something GIVEAWAY!</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20">                                   
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                    <!-- Space -->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td width="100%" height="40">                                   
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Icon 3 -->
                                    <table width="170" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter">
                                        <tbody><tr>
                                            <td width="150" class="icon30" align="center">
                                                <table width="80" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100%" align="center" height="80" bgcolor="#ffffff" style="border-radius: 100%; text-align: center;"><a href="http://dgo.nz/9nOzp1F43005" style="text-decoration: none;" object="image-editable"><img src="http://i.imgur.com/Qk2g17Y.png" width="98" alt="" editable="true" border="0" mc:edit="27" class="jump"></a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; font-weight: bold; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="28">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Beth Hornsby-Hunt</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="29">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Win a $50 Shop Credit to spend at Adorns next RTS on Monday 16th Feb 2015</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20">                                   
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper -->
                        
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="25"></td>
                            </tr>
                        </tbody></table><!-- End Space -->
                        
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table>




<!--Hero Image-->

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#f5f5f5" c-style="greyBG" style="background-color: white;">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                    
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="25"></td>
                            </tr>
                        </tbody></table><!-- End Space -->
                        
                        <!-- Wrapper -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                            <tbody><tr>
                                <td width="100%" align="center">
                                    
                                    <!-- Icon 1 -->
                                    <table width="170" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter">
                                        <tbody><tr>
                                            <td width="150" class="icon30" align="center">
                                                <table width="80" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td align="center" width="100%" height="80" bgcolor="#ffffff" style="border-radius: 100%; text-align: center;"><a href="http://dgo.nz/E6eDMbcl3008" style="text-decoration: none;" object="image-editable"><img src="http://i.imgur.com/sJh4XuN.png" width="98" alt="" editable="true" border="0" mc:edit="19" class="jump"></a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; font-weight: bold; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="20">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Abbey Summerton</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="21">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Win a Striped Breastfeeding Maxi Dress!</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20">                                   
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                    <!-- Space -->
                                    <table width="45" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td width="100%" height="40">                                   
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Icon 2 -->
                                    <table width="170" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter">
                                        <tbody><tr>
                                            <td width="150" class="icon30" align="center">
                                                <table width="80" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100%" align="center" height="80" bgcolor="#ffffff" style="border-radius: 100%; text-align: center;"><a href="http://dgo.nz/CJRzLqTG3009" style="text-decoration: none;" object="image-editable"><img src="http://i.imgur.com/PLHpk5L.png" width="98" alt="" editable="true" border="0" mc:edit="23" class="jump"></a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; font-weight: bold; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="24">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Alana Hammar</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="25">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Win an entry to the Rotorua Off Road Half Marathon</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20">                                   
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                    <!-- Space -->
                                    <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td width="100%" height="40">                                   
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Icon 3 -->
                                    <table width="170" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="fullCenter">
                                        <tbody><tr>
                                            <td width="150" class="icon30" align="center">
                                                <table width="80" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100%" align="center" height="80" bgcolor="#ffffff" style="border-radius: 100%; text-align: center;"><a href="http://dgo.nz/xfTBPUvK3003" style="text-decoration: none;" object="image-editable"><img src="http://i.imgur.com/UDAcl0v.png" width="98" alt="" editable="true" border="0" mc:edit="27" class="jump"></a>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; font-weight: bold; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="28">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Angie Forrest</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="29">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Beautiful Red Heart from Rustic love</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20">                                   
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper -->
                        
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="25"></td>
                            </tr>
                        </tbody></table><!-- End Space -->
                        
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" c-style="bgcolor" style="background-color:  rgb(245, 245, 245);">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                        
                        <!-- Wrapper -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%">
                                    
                                    <!-- Space -->
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                    </tbody></table><!-- End Space -->

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%">
                                    
                                    <!-- Centered Title -->
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tbody>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 26px; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold;" t-style="headlines" class="fullCenter" mc:edit="18">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Big GIVEAWAY of the month:</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="40"></td>
                                        </tr>
                                        <tr>
                                            <td width="100%" align="center">
                                                
                                                <table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100" align="center" height="1" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(229, 229, 229); color: rgb(186, 186, 186);" t-style="textColor"></td>
                                                    </tr>
                                                </tbody></table>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table>
                                    
                                    <!-- Image 600 -->
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td align="center" width="100%" style="text-align: center;" class="image600">
                                                
<a href="http://tracking.cohortdigital.com.au/aff_c?offer_id=1300&amp;aff_id=1619&amp;aff_sub=5883|5885&amp;aff_sub2=Checkout_500TravelGiftCard" style="text-decoration: none;" object="image-editable"><img editable="true" src="http://i.imgur.com/Uxvjx4d.jpg" width="600" height="auto" alt="" border="0" mc:edit="88" class="hover"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Text Right -->
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                        <tbody><tr>
                                            <td valign="top" width="100%" style="text-align: center;">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                                                    <tbody><tr>
                                                        <td height="35" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 18px; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold;" t-style="headlines" mc:edit="91">
                                                        
                                                            <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgbold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]-->
  
  
<a style="color: inherit;" href=" http://tracking.cohortdigital.com.au/aff_c?offer_id=1300&amp;aff_id=1619&amp;aff_sub=5883|5885&amp;aff_sub2=Checkout_500TravelGiftCard">
<singleline>Enter NOW to win this Travel Gift Card</singleline>
  
</a><!--[if !mso]><!-->


</span><!--<![endif]--></p>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                                
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(186, 186, 186); line-height: 22px;" t-style="textColor" mc:edit="92">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Dango is working with WinAtCheckout to bring you a giveaway for a $500 Travel Gift Card! <a style="color: inherit;" href=" http://tracking.cohortdigital.com.au/aff_c?offer_id=1300&amp;aff_id=1619&amp;aff_sub=5883|5885&amp;aff_sub2=Checkout_500TravelGiftCard">Click here to go in the draw!</a></singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                        
                                        
                                        
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper 2 -->
                        
                        <!-- Space -->
                        <!-- End Space -->
                        
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table>
<!--Hero Image-->


<!--Instant entries writing-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" c-style="bgcolor">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                    
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="60"></td>
                            </tr>
                        </tbody></table><!-- End Space -->
                                    
                        <!-- Wrapper -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%">
                                    
                                    <!-- Centered Title -->
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tbody><tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 26px; color: rgb(48, 48, 48); line-height: 32px; font-weight: bold;" t-style="headlines" class="fullCenter" mc:edit="78">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_novasemibold",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Instant Entries</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: rgb(186, 186, 186); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="79">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><singleline>Some more of our great giveaways! If you are reading this email you will already have registered with Dango - so all you have to do is click "Enter Giveaway".</singleline><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="40"></td>
                                        </tr>
                                        <tr>
                                            <td width="100%" align="center">
                                                
                                                <table width="100" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody><tr>
                                                        <td width="100" align="center" height="1" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(229, 229, 229); color: rgb(186, 186, 186);" t-style="textColor"></td>
                                                    </tr>
                                                </tbody></table>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper 2 -->
                        
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table>
<!--Instant entries writing-->

<!--Instant entries giveaways-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" c-style="bgcolor">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                        
                        <!--  Wrapper 2 -->
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" class="image195">
                    
                                    <!-- Space -->
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                        <tbody><tr>
                                            <td width="100%" height="25"></td>
                                        </tr>
                                    </tbody></table><!-- End Space -->
                                    
                                    <!-- Image 195 - 1 -->
                                    <table width="195" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scale33">
                                        <tbody><tr>
                                            <td width="100%">
                                                <a href="http://dgo.nz/eBx6r2cw3015" style="text-decoration: none;" object="image-editable"><img editable="true" src="http://i.imgur.com/oMn4JW6.jpg" alt="" border="0" width="195" height="auto" class="hover" mc:edit="80"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25">                                   
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(113, 113, 113); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="21">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><a style="color: inherit;" href="http://dgo.nz/eBx6r2cw3015">
<singleline>Win this funky breastfeeding Top!</singleline>
</a><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        
                                    </tbody></table>
                                    
                                    <!-- Space -->
                                    <!--[if mso]><v:shape><table width="2" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"></v:shape><![endif]-->
                                    <div class="erase"><span style="mso-hide: hide;"><!--[if !mso]><!--></span></div>
                                    <table width="8" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="w10">
                                    <!--msoButton-->
                                        <tbody><tr>
                                            <td width="100%" height="40">                                   
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Image 195 - 2 -->
                                    <table width="195" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scale33">
                                        <tbody><tr>
                                            <td width="100%">
                                                <a href="http://dgo.nz/TbE6JXWm3011" style="text-decoration: none;" object="image-editable" color:=""><img editable="true" src="http://i.imgur.com/h7ZxJWw.jpg" alt="" border="0" width="195" height="auto" class="hover" mc:edit="75"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25">                                   
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(113, 113, 113); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="21">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><a style="color: inherit;" href="http://dgo.nz/TbE6JXWm3011">
<singleline>Your Home and Garden Magazine One Years Subscription</singleline>
</a><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td width="100%" height="0" class="h40">                                    
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    
                                    <!-- Image 195 - 3 -->
                                    <table width="195" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scale33Right">
                                        <tbody><tr>
                                            <td width="100%">
                                                <a href="http://dgo.nz/lkCGZmPb3018" style="text-decoration: none;" object="image-editable"><img editable="true" src="http://i.imgur.com/lMOdh0R.jpg" alt="" border="0" width="195" height="auto" class="hover" mc:edit="85"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="25">                                   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 19px; color: rgb(28, 186, 200); line-height: 24px; text-transform: uppercase;" t-style="blueText" class="fullCenter" mc:edit="86">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(113, 113, 113); line-height: 22px;" class="fullCenter" t-style="textColor" mc:edit="21">
                                                <p object="text-editable"><!--[if !mso]><!--><span style="font-family: " proxima_nova_rgregular",="" helvetica;="" font-weight:="" normal;"=""><!--<![endif]--><a style="color: inherit;" href="http://dgo.nz/lkCGZmPb3018">

<singleline>Rose Crush Luxurious gift pack</singleline>
  
</a><!--[if !mso]><!--></span><!--<![endif]--></p>
                                            </td>
                                        </tr>
                                        
                                    </tbody></table>
                                    
                                </td>
                            </tr>
                        </tbody></table><!-- End Wrapper 2 -->
                        
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="90"></td>
                            </tr>
                        </tbody></table><!-- End Space -->
                        
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table>
<!--Instant entries giveaways-->

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" c-style="bgcolor">
    <tbody><tr mc:repeatable="">
        <td width="100%" valign="top">
        <div mc:hideable="">
            
            <!-- Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                <tbody><tr>
                    <td>
                        
                        
                        
                        
                        <!-- Space -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="25">

  <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
                                                        If you received this message in error please disregard and delete this.<br>
                                                        You can also unsubscribe from these emails by following 
                                                        <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm='.$uid.'" style="color: #aaa;">this link</a></p>

</td>
                            </tr>
                        </tbody></table><!-- End Space -->
       <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                            <tbody><tr>
                                <td width="100%" height="25">



</td>
                            </tr>
                        </tbody></table>                 
                    </td>
                </tr>
            </tbody></table><!-- End Wrapper -->
        
        </div>
        </td>
    </tr>
</tbody></table>



</div>
<!--[[display]]</body>[[display]]-->

<!--[[display]]</html>[[display]]-->    </div></body></html>';

} ?>
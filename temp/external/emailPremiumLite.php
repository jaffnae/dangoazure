<?php
    function makeBodyText($fname, $lname, $uid)
    {
       return '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="ecxouter" style="background:#ddd;">
            <tbody>
              <tr>
                <td>
                  <table align="center" cellpadding="0" cellspacing="0" border="0" width="670" style=" background-color: #f2f2f2; border-right: 1px solid #cccccc; border-left: 1px solid #cccccc">
                  <tbody>
                      <tr>
                        <td>
                            <table style="border-bottom: 1px solid #cccccc">
                            <tbody>
                              <tr>
                                <td style="width:19px;height:77px;">&nbsp;</td>

                                <td height="77" width="46" valign="top" rowspan="2" style="line-height:100%;">

                                    <br />
                                    <img style="height: 40px;" src="//dango.co.nz/images/emails/dangoLogo.png" />
                                  </td>

                                <td class="ecxcut" width="9">&nbsp;</td>

                                <td width="10" height="77" >&nbsp;</td>

                                <td width="520" height="77" style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;color:#333;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr>
                                        <td style="font-size:14px;font-weight:bold;color:#000;font-size:14px;line-height:normal;font-weight:bold;color:#333333;">
                                        <span dir="ltr">'.$fname.' '.$lname.',</span></td>
                                      </tr>

                                      <tr>
                                        <td style="font-size:11px;color:#666;">You\'ve ran a successful giveaway with Dango as a Premium Member. We\'d like to know what you thought of Dango.</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>

                                <td width="10" height="77">&nbsp;</td>

                              </tr>

                            </tbody>
                            </table>
                        </td>
                      </tr>

                      <tr>
                        <table  align="center" cellpadding="0" cellspacing="0" border="0" width="670" style=" background-color: #f2f2f2; border-right: 1px solid #cccccc; border-left: 1px solid #cccccc">
                            <!--SECOND PART-->

                            <tr>
                                <td style="background-color: white;" width="100" height="90">
                                    &nbsp;
                                </td>
                                <td style="background-color: white; text-align: center; font-family:Helvetica Neue, Helvetica, Arial, sans-serif; " height="90">
                                    <br />

                                    <p style="color: #0fb2c4; font-size: 22px; ">Question:</p>


                                    <table bgcolor="#fcfcfc" cellpadding="0" cellspacing="0" border="0" width="100%" style="color:#333; text-align: center; border-radius:6px;border:solid 1px #e5e5e5;background:#fcfcfc; text-align: center;">
                                        <tbody>
                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="12" style="font-size:1px;font-size:1px;">
                                                </td>


                                                <td style="color: #0fb2c4; font-size: 18px">

                                                    After using the Dango App is like how do you feel about it?

                                                </td>

                                                <td width="10" style="font-size:1px;font-size:1px;">
                                                    <br />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">
                                                    &nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                 <td style="background-color: white;" width="100" height="90">
                                    &nbsp;
                                </td>
                            </tr>


                            <tr>
                                <td style="background-color: white;" width="100">
                                    &nbsp;
                                </td>
                                <td style="background-color: white;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;color:#333; border-radius:6px; text-align: center;">
                                        <tbody>
                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="12" style="font-size:1px;font-size:1px;">
                                                </td>


                                                <td style="font-size:18px;">
                                                    <br />
                                               </td>

                                                <td width="10" style="font-size:1px;font-size:1px;">
                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:16px; ">


                                                    <p style="color: #666666; font-size: 22px;">Answers:</p>


                                                    <table bgcolor="#EEEEEE" cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;color:#666666; text-align: center;border:solid 1px #e5e5e5;background:#eeeeee; text-align: center;">
                                        <tbody>
                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="12" style="font-size:1px;font-size:1px;">
                                                </td>


                                                <td>

                                                    <a href="http://www.dango.co.nz/index.php?page=external/ty&uid='.$uid.'" style="color: #666666;">I would use Dango for another giveaway...</a>


                                                </td>

                                                <td width="10" style="font-size:1px;font-size:1px;">

                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">
                                                    &nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>







                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:16px;">


                                                    <br />


                                                     <table bgcolor="#EEEEEE" cellpadding="0" cellspacing="0" border="0" width="100%" style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;color:#666666; text-align: center; border:solid 1px #e5e5e5;background:#eeeeee; text-align: center;">
                                        <tbody>
                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="12" style="font-size:1px;font-size:1px;">
                                                </td>


                                                <td>

                                                    <a href="http://www.dango.co.nz/index.php?page=external/tn&uid='.$uid.'" style="color: #666666">I would not use Dango again...</a>


                                                </td>

                                                <td width="10" style="font-size:1px;font-size:1px;">
                                                    <br /><br />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td height="10" colspan="4" style="font-size:1px;font-size:1px;">
                                                    &nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <br /><br />





                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                 <td style="background-color: white;" width="100">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: white;" width="100">
                                    <br />
                                </td>
                                <td style="background-color: white;" width="100">
                                    <br />
                                </td>
                                <td style="background-color: white;" width="100">
                                    <br />
                                </td>
                            </tr>
                            <!--SECOND PART-->

                            <!--THIRD PART -->
                            <tr>
                                <td>


                                </td>
                            </tr>
                            <!--THIRD PART-->

                        </table>
                    </tr>
                      <tr>
                          <td>
                              <table align="center" cellpadding="0" cellspacing="0" border="0" width="670" style=" background-color: #f2f2f2; border-right: 1px solid #cccccc; border-left: 1px solid #cccccc">
                  <tbody>
                      <tr>
                        <td>
                            <table style="border-bottom: 1px solid #cccccc">
                            <tbody>
                              <tr>
                                <td style="width:19px;height:77px;">&nbsp;</td>

                                <td class="ecxcut" width="9">&nbsp;</td>

                                <td width="10" height="77" >&nbsp;</td>

                                <td width="520" height="77" style="font-family:Helvetica Neue, Helvetica, Arial, sans-serif;color:#333;">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                      <tr>
                                        <td style="font-size:11px;color:#666;">
                                            Want to upgrade to Dango Premium? See our <a href="http://www.dango.co.nz/index.php?page=goPremium">list of Premium Memberships</a>. You can also unusbscribe by <a href="http://www.dango.co.nz/index.php?page=unsubscribeMerch&uId='.$uid.'&uFrm=prmliterem">clicking here</a>.
                                          </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>

                                <td width="10" height="77">&nbsp;</td>


                              </tr>

                            </tbody>
                            </table>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>';
} ?>

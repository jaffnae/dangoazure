#!/usr/bin/php -q

<?php
//DOCUMENTATION:
/*


todo.....
//1)TEST THIS
		WIth jsonStringTest first.
		Then set some test upper/lower dates and ping the cron directly
		with fake jsonString0.php, jsonString1.php and jsonString2.php set up
		and $confirmSend set to true just to see if the emails are coming through correctly.

//2) Set the time lower/upper
//3) Set subject Line
//4) Set the newsletter template name
//5) Set sendGridAccountToUse
//6) Change 'confirmSend' to true
*/


date_default_timezone_set('NZ');
$nzTimeCurrent = date("Y-m-d H:i");

// $setTime = strtotime("2017-Jan-24 20:20");
// $setTime2 = strtotime("2017-Jan-24 20:40");
// echo $setTime;
// echo "
// ";
// echo $setTime2;
//


//TEST strtotimes
// $setTimeLower =1485280200;
// $setTimeUpper = 1485281400;
//
// $nzTime1 = date("Y-m-d H:i", $setTimeLower);
// $nzTime2 = date("Y-m-d H:i", $setTimeUpper);
// echo $nzTime1;
// echo $nzTime2;


//Setup
$subjectLine = "[Reminder] Expires tonight: 20% off Cookbook Sale-a-bration!";
$newsletterTemplateName = "newsletterTemplates/clients/wickedWellbeing10";
$sendGridAccountToUse = "sendGridBusinessAccount";
$jsonsToSend = array("0", "1", "2");
$confirmSend = true;
$initJsonString = "jsonStringTest";

//27th Dec
// $setTimeLower = 1482823200;
// $setTimeUpper = 1482824400;

//Test Dec 26 8.30 am NZ Time -- COMMENT OUT AFTER
// $setTimeLower = 1482693600;
// $setTimeUpper = 1482694800;

//31st DECEMBER - 7.30am
// $setTimeLower =1483122000;
// $setTimeUpper = 1483123200;

//11th January - 8.30pm
// $setTimeLower =1484119200;
// $setTimeUpper = 1484120400;

//18th January - 8.30pm
// $setTimeLower = 1484724000;
// $setTimeUpper = 1484725200;

//24th January - 8.30pm
//$setTimeLower = 1485242400;
//$setTimeUpper = 1485243600;

//25th January - 7.00am
 $setTimeLower = 1485280200;
 $setTimeUpper = 1485281400;


//TEST
// $setTimeLower = strtotime($nzTimeCurrent) - 600;
// $setTimeUpper = strtotime($nzTimeCurrent) + 600;


// $setTime = strtotime("2016-Dec-31 07:20");
// $setTime2 = strtotime("2016-Dec-31 07:40");
//$nzTimeCurrent = date("Y-m-d H:i", 1482823800);
// echo $setTime;
// echo "
// ";
// echo $setTime2;

if(strtotime($nzTimeCurrent) > $setTimeLower && strtotime($nzTimeCurrent) < $setTimeUpper){

	//Should be when our newsletter needs to be sent out....
	$_REQUEST['password'] = "dbx49";
	$_REQUEST['subject'] = $_REQUEST['defaultSubject'] = $subjectLine;
	$_REQUEST['newsletterTemplate'] = $newsletterTemplateName;
	$_REQUEST['sendGridAccountToUse'] = $sendGridAccountToUse;
	$_REQUEST['v'] = $initJsonString;

	include_once("sendgridapi.php");

	if($confirmSend){
		foreach ($jsonsToSend as $jsonFileToSend) {
			$includeFileName = "jsonString".$jsonFileToSend.".php";
			include_once($includeFileName);

		   if(isset($jsonString) && $jsonString != ""){ //Will not be set if file is empty...
		      $decodeJson = $jsonString;
		      $numberOfEmails = count($decodeJson->to);


		      if(!isset($decodeJson->unique_args->subject)
		         || $decodeJson->unique_args->subject == ""
		         || $numberOfEmails < 2500){
		               $subjectToSend = $subject;
		               $subjCat = "d";
		               $decodeJson->unique_args->subject = $subjectToSend;
		               $decodeJson->unique_args->subjCat = $subjCat;
		      }else{
		         $subjectToSend = $decodeJson->unique_args->subject;
		      }
				if(!isset($fromNameAndEmailObject)){
		         $fromNameAndEmailObject = null;
		      }
		      $jsonString = json_encode($decodeJson);
				error_log("about to send");

				sendEmailUsingSendgrid($jsonString, $sendGridApiHtml, $subjectToSend, "", "", false, true, $fromNameAndEmailObject, $sendGridAccountToUse);
			}else{
				echo "Problem with getting \$jsonString var";
			}
		}
	}
}

echo "ran";

?>

<?php
//Config must be included before using this function

//Example of a JSON string to be passed to this function


function sendEmailUsingSendgrid(
                                 $jsonString,
                                 $htmlContent,
                                 $subjectToSend,
                                 $name,
                                 $email,
                                 $useDefaultJsonString,
                                 $forceSendFromLocal,
                                 $fromNameAndEmailObject,
                                 $sendGridAccountToUse){
   global $sendGridCredentials;
   global $accessingFromLocalhost;

   if($useDefaultJsonString && isset($name) && isset($email) && $name != "" && $email != "" && $email != "singleEmailToHotmail"){
      $jsonString = '{
                        "to": [
                           "'.$email.'"

                        ],
                        "sub": {
                           "-name-": [
                              "'.$name.'"
                           ]
                        }
                     }';

   }

   if($useDefaultJsonString && $email == "singleEmailToHotmail"){
      $jsonString = '{
                        "to": [
                           "jaffnae@hotmail.com"

                        ],
                        "sub": {
                           "-name-": [
                              "Allen"
                           ]
                        }
                     }';

   }

   if(!isset($forceSendFromLocal)){
      $forceSendFromLocal = false;
   }

   if(!isset($fromNameAndEmailObject)
      || !isset($fromNameAndEmailObject["name"])
      || !isset($fromNameAndEmailObject["email"])
      || $fromNameAndEmailObject["name"] == ""
      || $fromNameAndEmailObject["email"] == "" ){
            $fromNameAndEmailObject["name"] = "Dango Giveaways";
            $fromNameAndEmailObject["email"] = "dangogiveaways@gmail.com";
   }

   $url = 'https://api.sendgrid.com/';


   if(!isset($sendGridAccountToUse) || $sendGridAccountToUse == ""){
      //If we don't specify which Sendgrid account to use,
      //just default to the original standard one.
      $sendGridAccountToUse = "sendGridStandardAccount";
   }

   //Previously labeled as $sendGridUsername
   $user = $sendGridCredentials[$sendGridAccountToUse]["sendGridUsername"];

   //Previously labeled as $sendgridPassword
   $pass = $sendGridCredentials[$sendGridAccountToUse]["sendGridPassword"];

   $json_string = $jsonString;
   $htmlString = $htmlContent;
   if($json_string == ""){
      return '{"message": "Json String File is empty!"}';
   }
   $params = array(
       'api_user'  => $user,
       'api_key'   => $pass,
       'x-smtpapi' => $json_string,
       'to'        => '_', //Not sent to anyone - just need to have an email address there
       'subject'   => $subjectToSend,
       'html'      => $htmlString,
       'text'      => '', //Need to add some kind of a text alternative here.
       'from'      => $fromNameAndEmailObject["email"],
       'fromname' => $fromNameAndEmailObject["name"]
     );


   $request =  $url.'api/mail.send.json';
   if(!$accessingFromLocalhost || $forceSendFromLocal){
      //Generate curl request
      $session = curl_init($request);
      // Tell curl to use HTTP POST
      curl_setopt ($session, CURLOPT_POST, true);
      // Tell curl that this is the body of the POST
      curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
      // Tell curl not to return headers, but do return the response
      curl_setopt($session, CURLOPT_HEADER, false);
      // Tell PHP not to use SSLv3 (instead opting for TLS)
      curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
      curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

      // obtain response
      $response = curl_exec($session);
      curl_close($session);
      return $response;
   }else{
      if($accessingFromLocalhost){
         echo "<br /><br />
         ============================================================================<br />
         EMAIL NOT SENT. Can't send from local - need \$forceSendFromLocal flag enabled.
         <br />============================================================================<br />";
      }
   }
   return $response;
}


function getTotalGiveawayExposure($puidx){

   $stats = "";
   $g = mysql_query("SELECT stats FROM giveawayStats WHERE puidx = '$puidx' LIMIT 1");
   while($rowStats = mysql_fetch_assoc($g)){
      $stats = json_decode($rowStats['stats']);
      $total = 0;
      foreach($stats as $stat){
         $total += (int)$stat;
      }
      $stats->total = $total;
   }

   return $stats;
}
?>

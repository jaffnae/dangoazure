<?php

$fromNameAndEmailObject = array();
$fromNameAndEmailObject["name"] = "Dango Contests";
$fromNameAndEmailObject["email"] = "dangogiveaways@gmail.com";

//URL to visit if NZ and Unmarked Countries:
//http://www.dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=dango&country=new%20zealand&includeUnmarkedCountries=true

$sendGridApiHtml = '

<html><head></head>
	<body style="background: rgb(234, 234, 234);">
		<div class="newsletterContainer">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;height: 100% !important;width: 100% !important;">
			<tbody><tr>
			<td align="center" valign="top">

			   <table border="0" cellpadding="0" cellspacing="0" width="566" id="templateContainer" style="border: 0px solid #DDDDDD;background-color: #fff; -webkit-box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.6); -moz-box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.6); box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.6);">
			       <tbody>
			       <tr>
			           <td align="center" valign="top">
			               <table border="0" cellpadding="10" cellspacing="0" width="566">
			                   <tbody><tr>
			                       <td valign="top" class="bodyContent" style="background-color: #fff;">

			                           <table border="0" cellpadding="10" cellspacing="0" width="100%">
			                               <tbody><tr>
			                                   <td valign="top">
			                                       <div style="color: #505050;font-family: Tahoma, Geneva, sans-serif;font-size: 12px;line-height: 150%;text-align: left;">
																<table background="" class="hTitle" style="width: 100%;text-align: center;margin-bottom: 10px;font-family: Tahoma, Geneva, sans-serif;">
																	<tbody>
																	  <tr>
																		  <td width="20%">
																				<img src="http://i.imgur.com/QAJQroZ.png" alt="Dango Giveaways Logo">
																		  </td>
																		  <td width="80%" style="font-family: Tahoma, Geneva, sans-serif; text-align: right;">
																			  <span style="font-size:11px;color: #58BBC5;font-style: italic;">Dango - giveaways by Kiwi businesses</span><br>
																		  </td>
																	  </tr>
																	</tbody>
																</table>
																												<br>
	<table style="width: 100%;text-align: center;margin-bottom: 5px;font-family: Tahoma, Geneva, sans-serif;">
		<tbody>
			<tr>
				<td style="font-family: Tahoma, text-alignt: center; background: #58BBC5; Geneva, sans-serif;">
		      	<span class="h1" style="color: #fff;font-family: Tahoma, Geneva, sans-serif;background: #58BBC5; font-size: 42px;font-weight: normal;line-height: normal;text-align: center;padding: 0 10px;">Hot Giveaway of the Week</span>
				</td>
		  </tr>
	  </tbody>
	</table>
	 <a href="http://dgo.nz/jZGUncST4345" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">
    	<img align="none" alt="" class="ofertaDestacada" height="400" src="http://i.imgur.com/pCfly2E.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: 400px;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;" width="525">
	 </a>

	 <br>
	 <br><br>
	 <br>
	 <hr style="border-top: dotted 1px;">
	 <br>
	 <br>
	 <br>

	 <table class="hTitle" style="width: 100%;text-align: center;margin-bottom: 10px; font-family: Tahoma, Geneva, sans-serif;">
	    <tbody>
	       <tr>
	          <td style="font-family: Tahoma, Geneva, sans-serif; background: #58BBC5;">
	             <span class="h1" style="color: #fff;font-family: Tahoma, Geneva, sans-serif;font-size: 49px;font-weight: normal;line-height: normal;text-align: center;padding: 0 10px;">Other great giveaways</span>
	          </td>
	       </tr>
	    </tbody>
	 </table>

	 <br>
	<table cellpadding="5" style="min-width: 100%"><tbody><tr class="0 tdParentContainer">
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/LKMwGbyf4369" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/XfMjhx1.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="first_td commonTdClass" style="text-align: center; vertical-align: middle; height: 117px;" id="txt_0">
								<a href="http://dgo.nz/LKMwGbyf4369" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Win an item of clothing from our NEW SEASON range!
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td>
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/Me6wrT8X4357" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/drlB8rl.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="second_td commonTdClass" style="text-align: center; vertical-align: middle; height: 117px;" id="txt_1">
								<a href="http://dgo.nz/Me6wrT8X4357" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Win a Case of West Coast Ginger Beer!
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td></tr><tr class="2 tdParentContainer">
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/25Iu4Osd4364" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/C8hfS4X.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="first_td commonTdClass" style="text-align: center; vertical-align: middle; height: 117px;" id="txt_2">
								<a href="http://dgo.nz/25Iu4Osd4364" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Ceramic Burner, 5 Melts, Tin of Tea and Handmade Soap
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td>
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/svmA73T94362" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/wQXqkbW.png" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="second_td commonTdClass" style="text-align: center; vertical-align: middle; height: 117px;" id="txt_3">
								<a href="http://dgo.nz/svmA73T94362" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Mum Pamper Pack
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td></tr><tr class="4 tdParentContainer">
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/WoPrgu7A4366" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/HmuZ4FJ.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="first_td commonTdClass" style="text-align: center; vertical-align: middle; height: 140px;" id="txt_4">
								<a href="http://dgo.nz/WoPrgu7A4366" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Mini Equus Session ( 90 minutes of 1:1 Equus Coaching )
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td>
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/1hgA3DuZ4365" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/SnBxm4j.png" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="second_td commonTdClass" style="text-align: center; vertical-align: middle; height: 140px;" id="txt_5">
								<a href="http://dgo.nz/1hgA3DuZ4365" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Ultimate Baby Giveaway
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td></tr><tr class="6 tdParentContainer">
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/79NCHItp4344" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/cfXtxF0.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="first_td commonTdClass" style="text-align: center; vertical-align: middle; height: 140px;" id="txt_6">
								<a href="http://dgo.nz/79NCHItp4344" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">* * * WIN this Natural Deodorant Paste from Totally Balmy * * *
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td>
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/cNZKNuUBDvl4360" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/dFaeRL6.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="second_td commonTdClass" style="text-align: center; vertical-align: middle; height: 140px;" id="txt_7">
								<a href="http://dgo.nz/cNZKNuUBDvl4360" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">**Win** Baby Ink Less Printing Kit OR a Bonds Wondersuit
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td></tr><tr class="8 tdParentContainer">
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/gmiPxUhq4368" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/tN5PEAL.png" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="first_td commonTdClass" style="text-align: center; vertical-align: middle; height: 117px;" id="txt_8">
								<a href="http://dgo.nz/gmiPxUhq4368" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Massage for a Mothers Day gift
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td>
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/cNZLAQBO0174356" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/l1IFhCt.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="second_td commonTdClass" style="text-align: center; vertical-align: middle; height: 117px;" id="txt_9">
								<a href="http://dgo.nz/cNZLAQBO0174356" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Signed Owl &amp; Pussycat 8x10 print
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td></tr><tr class="10 tdParentContainer">
			<td>
				<table style="border: 1px solid #ccc;">
					<tbody>
						<tr>
							<td>
								<a href="http://dgo.nz/pQ6AwGL44361" class="pickImg" target="_blank" style="color: #336699;  border-bottom: 1px solid #ccc; width: 250px; height: 250px; max-height: 250px;  overflow-x: hidden; overflow-y: hidden; display:inline-block; overflow:hidden; vertical-align:top; font-weight: normal;text-decoration: underline;">
									<img align="none" src="http://i.imgur.com/SdQuo5Z.jpg" style="vertical-align: top; font-size: 14px; font-weight: bold; line-height: 100%; outline: none; text-decoration: none; text-transform: capitalize; display: inline; width:100%; max-height: 250px;">
								</a>
							</td>
						</tr>
						<tr>
							<td class="first_td commonTdClass" style="text-align: center; vertical-align: middle; height: 94px;" id="txt_10">
								<a href="http://dgo.nz/pQ6AwGL44361" class="gvwayTitle" target="_blank" style="color: #666; padding: 5px 0; display:inline-block; overflow:hidden; font-weight: normal;text-decoration: none;width: 250px;font-style: italic;color: #58BBC5;font-size: 19px;">Black Bone Koru Pendant
									<br><br>
									<span style="display: block; text-align: right;">
										<img src="http://i.imgur.com/xNA7TH2.png" height="28" width="92" style="width: 92px; height: 28px; margin: 5px;">
									</span>
								</a>

							</td>
						</tr>
					</tbody>
				</table>
				<br><br>
			</td>

																		</tr></tbody></table>

																		</div></td>

			                                       </tr>
			                                       </tbody></table>
			                                   </td>
			                               </tr>
			                           </tbody></table>

			                       </td>
			            </tr><tr>
							<td><br><br><br>
								<table class="hTitle" style="width: 100%;text-align: center;font-family: Tahoma, Geneva, sans-serif;margin-bottom: 10px;">
								   <tbody>
								      <tr>
								         <td style="font-family: Tahoma, Geneva, sans-serif; background: #58BBC5;">

											   <span class="h1" style="color: #fff;font-family: Tahoma, Geneva, sans-serif; line-height: normal; font-size: 36px;text-align: center;">Last week\'s winners!</span>
								         </td>
								      </tr>
								   </tbody>
								</table>                                                                                                <br>
								<table style="width: 100%;text-align: left; font-size:11px;margin-bottom: 5px;font-family: Tahoma, Geneva, sans-serif; text-align: center;">
								   <tbody>
								      <tr>
												<td style="text-align: center; width: 50%; font-size: 11px;">
													<b><a href="http://dgo.nz/KUI4rpyk4346" target="_blank">Louise Maginness</a></b>
													<br><i style="font-size: 11px;"><a href="http://dgo.nz/KUI4rpyk4346" target="_blank">Fanola No Yellow Shampoo</a></i>.
													<br>
													<br>
													<br>
												</td>
												<td style="text-align: center; width: 50%; font-size: 11px;">
													<b><a href="http://dgo.nz/jXefEp5h4355" target="_blank">Kylee Kennedy</a></b>
													<br><i style="font-size: 11px;"><a href="http://dgo.nz/jXefEp5h4355" target="_blank">A set of wool to make a woven wall hanging</a></i>.
													<br>
													<br>
													<br>
												</td>
													</tr>
													<tr>

												<td style="text-align: center; width: 50%; font-size: 11px;">
													<b><a href="http://dgo.nz/jXefEp5h4355" target="_blank">Louise Maginness</a></b>
													<br><i style="font-size: 11px;"><a href="http://dgo.nz/jXefEp5h4355" target="_blank">A set of wool to make a woven wall hanging</a></i>.
													<br>
													<br>
													<br>
												</td>
												<td style="text-align: center; width: 50%; font-size: 11px;">
													<b><a href="http://dgo.nz/NrzB1IZ74340" target="_blank">Michelle Gaskell</a></b>
													<br><i style="font-size: 11px;"><a href="http://dgo.nz/NrzB1IZ74340" target="_blank">WIN A MOTHER\'S DAY PAMPER PACKAGE</a></i>.
													<br>
													<br>
													<br>
												</td>
													</tr>
													<tr>

												<td style="text-align: center; width: 50%; font-size: 11px;">
													<b><a href="http://dgo.nz/haqzOsyl4349" target="_blank">Stacey Weaver</a></b>
													<br><i style="font-size: 11px;"><a href="http://dgo.nz/haqzOsyl4349" target="_blank">Mothers Day Giveaway - VALUE over $460!</a></i>.
													<br>
													<br>
													<br>
												</td>

							</tr>
							<tr>
								<td align="center" valign="top" colspan="2">
									<table border="0" cellpadding="10" cellspacing="0" width="566" id="templateFooter" style="border-top: 0;">
										<tbody>
											<tr>
												<td valign="top" class="footerContent">

													<div style="padding-left: 8px; padding-right: 8px;">
													<hr style="border-top: dotted 1px;">
													</div>

													<p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 15px 0;">

														<a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">
															<img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
															<strong>See More Giveaways &gt;&gt;</strong>
														</a>&nbsp;&nbsp;


													</p>

													<div style="padding-left: 8px; padding-right: 8px;">
													<hr style="border-top: dotted 1px;">
													</div>

													<p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
														If you received this message in error please disregard and delete this.<br>
														You can also unsubscribe from these emails by following
			                                               <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-" style="color: #aaa;">this link</a></p><a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-" style="color: #aaa;">
			                                           <p></p>


												</a></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>

			               </tbody></table>
			           </td>
			       </tr>




			</tbody>
		</table>

</td></tr></tbody></table></div></body></html>

';
?>

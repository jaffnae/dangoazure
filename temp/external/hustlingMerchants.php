<?php 
    function makeBodyText($fname, $lname, $uid)
    {
       return '
       <html>
        Hi '.$fname.',

            <p>My name is Allen - I run Dango. You used it to run a few giveaways in the past (<i>'.$lname.'</i> and others). I\'m writing with a proposal. After analysing all the giveaways over the last year (which are in the thousands) I noticed that some were hugely successful while others weren\'t. Learning what made the successful ones work I\'ll be testing a new idea where I will work very closely with individual businesses and use the learned data to fine-tune each giveaway. The idea is to work with the business to set the giveaway up from the start and monitor its run several times a week for maximum growth. </p>

            <p>This is where you come in. Seeing as there are 100s of giveaways running weekly on Dango but only one of me, I shortlisted 20 businesses that I think would benefit most from this.</p>

            <p>If this is of interest I would like to ring you up and ask couple of questions about your business to determine that the offer really is the best fit.</p>

            <p>Please select from below:</p>

            <p><a href="http://www.dango.co.nz/index.php?page=temp/confirm&u='.$uid.'&t=ya">Ok, I\'d like to see how Dango can help</a></p>

            <p><a href="http://www.dango.co.nz/index.php?page=temp/confirm&u='.$uid.'&t=na">No thanks, I\'ve got enough customers as it is</a></p>

            <p>Cheers,<br />
            Allen</p>
            </html>';

} ?>
<?php
//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize

//Subject: 'Get your giveaway prize paid for by Dango'
//Link: http://www.dango.co.nz/queries/generateEmailJsonFilesAndSend.php?newsletterTemplateName=merchantCoveringPrizes2&customSubject=true
//NameOfJsonFile: jsonStringMERCHANTS
    $sendGridApiHtml = '
    <html>
    <body  style="font-family: sans-serif; color: #444;">
      <span style="font-size: 13px;">Hi -name-,</span><br />
       <br />
       <span style="font-size: 16px;">Dango businesses are now getting 300-400 email subscribers from a single giveaway AND we <b>pay them for their prizes.</b></span>

       <br /><br />

       <span  style="font-size: 13px;">
       We have figured out that collaborative giveaways usually perform very highly - so we have created a membership where we organize the entire giveaway up and group you up with businesses similar to you (or businesses who you <b>prefer</b> to collaborate with). The giveaway is built around the prize you wish to give away. Oh and we will also <b>cover the cost of that prize!</b> Want to join?
       </span>
       <br /> <br />
       <span>
          <a href="http://www.dango.co.nz/index.php?page=temp/giveawayPilot" style="font-size: 16px;">Click to learn more!</a>
       </span>

      <br /> <br /> <br />
       <p style="text-align:left; color: #777; font-size: 11px; font-family: arial, sans-serif;">
         If you received this message in error please disregard and delete this.<br>
         You can also unsubscribe from these emails by following
         <a href="http://www.dango.co.nz/index.php?page=unsubscribeMerch&amp;uId=-userId-&amp;uFrm=merchantCoveringPrizes2" style="color: #aaa;">this link</a></p>


    </body></html>

      ';




      echo $sendGridApiHtml;




 ?>

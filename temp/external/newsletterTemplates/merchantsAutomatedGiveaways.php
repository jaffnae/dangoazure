<?php
    $sendGridApiHtml = '
    <html><head></head><body><table style="border-collapse: collapse; margin: 0; padding: 0; background-color: #f2f2f2; min-height: 100% !important; width: 100% !important;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
   <tbody>
      <tr>
         <td style="margin: 0; padding: 20px; border-top: 0; min-height: 100% !important; width: 100% !important;" align="center" valign="top">
            <table style="border-collapse: collapse; border: 0;" border="0" width="600" cellspacing="0" cellpadding="0">
               <tbody>

                  <tr>
                     <td align="center" valign="top">
                        <table style="border-collapse: collapse; background-color: #ffffff; border-top: 0; border-bottom: 0;" border="0" width="600" cellspacing="0" cellpadding="0">
                           <tbody>
                              <tr>
                                 <td valign="top">
                                    <table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                       <tbody>
                                          <tr>
                                             <td style="padding: 9px;" valign="top">
                                                <table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                   <tbody>

                                                      <tr>
                                                         <td style="padding: 0px 9px; line-height: 100%; color: #606060; font-family: Helvetica; font-size: 15px; text-align: left;" valign="top" width="564">
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                       <tbody>
                                       <tr>
                                         <td>
                                         <div style="color: #ccc; font-family: Helvetica; font-size: 12px; text-align: center;">

                                           Hi -name-, <br />
                                                   Allen from Dango here! I had one question I wanted to ask: <br /><br/>
                                            </div>
                                         </td>
                                       </tr>
                                          <tr>
                                             <td style="padding: 9px;" valign="top">
                                                 <div style="color: #606060; font-family: Helvetica; font-size: 32px; line-height: 150%; text-align: center;">
                                                  Would you run a giveaway for your business if <b style="color: #444;">we paid for your prize?</b> <br /><br/>
                                                 </div>
                                                <table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                   <tbody>
                                                      <tr>
                                                         <td style="padding: 0 9px 9px 9px; text-align: center;" align="left" valign="top"><a style="word-wrap: break-word; " title="" href="http://www.dango.co.nz/index.php?page=automatedGiveaways" target="_blank" rel="nofollow"> <img style="max-width: 1500px; border: 0; outline: none; text-decoration: none; vertical-align: bottom;" src="http://i.imgur.com/lsSOTPt.jpg" alt="" width="407"> </a></td>
                                                      </tr>
                                                      <tr>
                                                         <td style="padding: 0 9px 0 9px; color: #606060; font-family: Helvetica; font-size: 15px; line-height: 150%; text-align: left;" valign="top" width="564">
                                                   <div style="text-align: center;">
<br />

We`re trialing a feature to reduce your risk of running giveaways. If you are a business who is focused on Instagram growth or Mailing List growth through giveaways then Dango is willing to <b>cover your prize and your membership costs</b>!
Initially we will only pilot this offer with <span style="font-weight: bold; font-size: 17px;">10 businesses</span>.

<br /><br />

<h2>How it Works:</h2>
We get in touch to discuss your goals, decide on a prize that makes sense and schedule an <a href="http://www.dango.co.nz/index.php?page=automatedGiveaways">automated giveaway</a> for you.
We then set some <b>giveaway targets</b> for your business. If those targets are not met, then your membership costs and your giveaway prize cost is on us!
<br /><br />
<h2>Giveaway Targets</h2>
The targets we set for your giveaway will vary depending on your business. For instance if you`re looking to build your mailing list then you could guaranteed to get at least 150 new subscribers! Depending on your business this number gets as high as 250 or more!

<br /><br />

If you`re interested to give this a go or learn more about this offer <br />

<a style="color: #0fb2c4; font-size: 20px; line-height: normal;" href="http://www.dango.co.nz/index.php?page=temp/giveawayPilot">click here to register</a>

<br />and we`ll get in touch within 1 day.

 </div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>

                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>

               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>

<p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
			If you received this message in error please disregard and delete this.<br>
			You can also unsubscribe from these emails by following
         <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uId=-userId-&amp;uFrm=autoGiveawayNewsletter" style="color: #aaa;">this link</a></p>

</body></html>
      ';




      echo $sendGridApiHtml;




 ?>

<?php
//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize
//http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[DB_NAME]&newsletterTemplateName=clients/GiftHutt1&customSubject=true


//1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
//2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
    //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
    //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
//3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
//4. Use the following link to send emails:
   //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
   //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_z_cl_giftHutt&newsletterTemplateName=clients/GiftHutt3&customSubject=true




//Make sure the code for Unsubscribe is on the bottom:
/*


<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>


*/



    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Gift Hutt";
    $fromNameAndEmailObject["email"] = "info@gifthutt.co.nz";


    $sendGridApiHtml = '
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="x_bodyTable" style="border-collapse:collapse; height:100%; margin:0; padding:0; width:100%; background-color:#cac8ca">
  <tbody>
    <tr>
      <td align="center" valign="top" id="x_bodyCell" style="height:100%; margin:0; padding:0; width:100%; border-top:0">
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateContainer" style="border-collapse:collapse; border:0">
          <tbody>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templatePreheader" style="border-collapse:collapse; background-color:#9e7da2; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_preheaderContainer" style="padding-top:9px; padding-bottom:9px">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="color:#202020; font-weight:normal; text-decoration:underline">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateHeader" style="border-collapse:collapse; background-color:#ffffff; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_headerContainer" style="">
                        <table class="x_mcnImageBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnImageBlockOuter">
                            <tr>
                              <td class="x_mcnImageBlockInner" valign="top" style="padding:9px">
                                <table class="x_mcnImageContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td class="x_mcnImageContent" valign="top" style="padding-right:9px; padding-left:9px; padding-top:0; padding-bottom:0; text-align:center">
                                        <a href="http://gifthutt.nz/" target="_blank" title="" class="" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/a086a34d-be33-47f8-a46f-459114e254b9.gif" alt="" class="x_mcnImage" align="middle" width="359" style="max-width:359px; padding-bottom:0; display:inline!important; vertical-align:bottom; border:0; height:auto; outline:none; text-decoration:none">                                          </a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="x_mcnBoxedTextBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnBoxedTextBlockOuter">
                            <tr>
                              <td class="x_mcnBoxedTextBlockInner" valign="top" style="">
                                <table class="x_mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px">
                                        <table class="x_mcnTextContentContainer" border="0" cellpadding="18" cellspacing="0" width="100%" style="min-width:100%!important; background-color:#565555; border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnTextContent" valign="top" style="color:#F2F2F2; font-family:Helvetica; font-size:14px; font-weight:normal; text-align:center; word-break:break-word; line-height:150%">
                                                <span style="font-size:15px">Gift Hutt is the art and craft hub in the Hutt.&nbsp;<br>
Our great store is in the heart of Lower Hutt at 136 High Street.<br>
We have a wide range of&nbsp;genuine and unique gifts, and&nbsp; craft classes&nbsp;and craft supplies for your creative needs.</span> </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnImageGroupBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnImageGroupBlockOuter">
                            <tr>
                              <td valign="top" class="x_mcnImageGroupBlockInner" style="padding:9px">
                                <table align="left" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="x_mcnImageGroupContentContainer" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td class="x_mcnImageGroupContent" valign="top" style="padding-left:9px; padding-top:0; padding-bottom:0">
                                        <a href="http://gifthutt.nz/shop/" target="_blank" title="" class="" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/f6290a5a-7254-42e0-af76-ae09c7ba3fbc.gif" alt="" width="263" class="x_mcnImage" style="max-width:323px; padding-bottom:0; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                          </a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <table align="right" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="x_mcnImageGroupContentContainer" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td class="x_mcnImageGroupContent" valign="top" style="padding-right:9px; padding-top:0; padding-bottom:0">
                                        <a href="http://gifthutt.nz/about-us/" target="_blank" title="" class="" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/9bbbc740-e9f9-4985-81a1-60867c3ee160.gif" alt="" width="263" class="x_mcnImage" style="max-width:323px; padding-bottom:0; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                          </a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateUpperBody" style="border-collapse:collapse; background-color:#FFFFFF; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_upperBodyContainer" style="">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnTextBlockOuter">
                            <tr>
                              <td valign="top" class="x_mcnTextBlockInner" style="padding-top:9px">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextContentContainer" style="max-width:100%; min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnTextContent" style="padding:0px 18px 9px; text-align:center; word-break:break-word; color:#404040; font-family:Helvetica; font-size:16px; line-height:150%">
                                        <h2 class="x_null" style="text-align:center; display:block; margin:0; padding:0; font-family:Helvetica; font-size:26px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; color:#202020!important">
Gift Hutt craft workshops and News</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnTextBlockOuter">
                            <tr>
                              <td valign="top" class="x_mcnTextBlockInner" style="padding-top:9px">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextContentContainer" style="max-width:100%; min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#404040; font-family:Helvetica; font-size:16px; line-height:150%; text-align:left">
                                        Welcome to all the new subscribers to this newsletter. &nbsp;Many of you would have signed up as part of a giveaway that I have been running and I would love for you to continue to receive the newsletters keeping you up to date with the news at Gift Hutt.
                                        &nbsp;But if you do not wish to continue to receive the newsletters there is an unsubscribe button at the bottom of this newsletter.
                                        <br>
                                        <br>
                                        <em><strong>What\'s New in Gift Hutt</strong></em>
                                        <br> We have some lovely pressed flower art just arrived from Christina Nica from Angelic Flowers. &nbsp;These make lovely gifts or buy one to put on your wall.
                                        <br>
                                        <br> Also on their way to Lower Hutt are Spellbinders products, the latest Heartfelt Creations Christmas collection. &nbsp;
                                        <br>
                                        <br>
                                        <em><strong>Workshops and Classes:</strong></em>
                                        <br>
                                        <strong>Cardmaking</strong>
                                        <br> I will be holding a class making masculine cards on the 24th July. &nbsp;Masculine cards can be hard to make and I will have a range of ideas for you to try out.
                                        <br>
                                        <br>
                                        <strong>Painting with Voodoo Molly Vintage Paints</strong>
                                        <br> Beth will be back later in the month with a painting class on Thursday 28th for those that find the weekends hard to come to. &nbsp;Depending on demand we will look at alternating weekdays and weekends.
                                        <br>
                                        <br>
                                        <strong>Making a Footstool Upholstery Class</strong>
                                        <br> Finally there is another Footstool making class on Saturday 30th July. &nbsp;This is a great class that gives you the basis for more ambitious upholstery projects.
                                        <br>
                                        <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                        <br>
                                        <br> Over the next few months I will be gradually adding a wider range of art and craft supplies for the artists and crafters to be able to purchase. &nbsp;As a paper crafter myself, I know how frustrating it is not
                                        being able to purchase products easily in New Zealand and I aim to fill some of those gaps. &nbsp;I am trying to source some very basic upholstery supplies to hold in Gift Hutt at the moment. &nbsp;If there is any
                                        particular product that you would like to be able to purchase through Gift Hutt please feel free to contact me and I can see if I can source this for you. &nbsp;I am happy to look for any crafts not just papercrafting.
                                        <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                        <br>
                                        <br> Have a great month and Happy Crafting
                                        <br> Patricia and Beth </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateColumns" style="border-collapse:collapse; background-color:#FFFFFF; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td align="left" valign="top" class="x_columnsContainer" width="50%" style="">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_templateColumn" style="border-collapse:collapse">
                          <tbody>
                            <tr>
                              <td valign="top" class="x_leftColumnContainer" style=""></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td align="left" valign="top" class="x_columnsContainer" width="50%" style="">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_templateColumn" style="border-collapse:collapse">
                          <tbody>
                            <tr>
                              <td valign="top" class="x_rightColumnContainer" style=""></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateLowerBody" style="border-collapse:collapse; background-color:#FFFFFF; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_lowerBodyContainer" style="">
                        <table class="x_mcnBoxedTextBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnBoxedTextBlockOuter">
                            <tr>
                              <td class="x_mcnBoxedTextBlockInner" valign="top" style="">
                                <table class="x_mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px">
                                        <table class="x_mcnTextContentContainer" border="0" cellpadding="18" cellspacing="0" width="100%" style="min-width:100%!important; background-color:#662A58; border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnTextContent" valign="top" style="color:#FFFFFF; font-family:Helvetica; font-size:16px; font-weight:bold; line-height:100%; text-align:left; word-break:break-word">
                                                Upcoming workshops at Gift Hutt, 136 High Street, Lower Hutt </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/36a77e93-8267-4308-8c68-47e8c43671f0.jpg" alt="" width="264" class="x_mcnImage" style="max-width:788px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Bring Your Own Project</h3>
                                                <div style="text-align:left">Use our space and get advice on your painting and upholstery projects.
                                                  <br>
                                                  <br>
                                                  <strong>When</strong>:Thursday 14th July
                                                  <br>
                                                  <strong>Time</strong>: 6.30 pm to 9 pm
                                                  <br>
                                                  <strong>Bring</strong>: Your project to work on.
                                                  <br>
                                                  <strong>Cost</strong>: $25</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://gifthutt.nz/product/bring-project-thursday-night-14-july-6-30-9-00pm/" target="_blank" class="x_mcnButton" title="Purchase ticket" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Purchase ticket</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="x_mcnDividerBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse; table-layout:fixed!important">
                          <tbody class="x_mcnDividerBlockOuter">
                            <tr>
                              <td class="x_mcnDividerBlockInner" style="min-width:100%; padding:14px 18px">
                                <table class="x_mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-top:2px solid #702E61; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style=""><span></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/58125861-1edc-4996-8ac6-1a7e0e33fab4.jpg" alt="" width="264" class="x_mcnImage" style="max-width:900px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Creating with Paverpol</h3>
                                                <div style="text-align:left">
                                                  <p style="margin:10px 0; padding:0; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                    Make a beautiful Sitting Lady with Paverpol and Qualfified Paverpol Instructor – Wendy Beyer.</p>
                                                  <p style="margin:10px 0; padding:0; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                    You will be amazed at what you can make with wire,tinfoil and material, add Paverpol. drape and create.</p>
                                                  <br>
                                                  <br>
                                                  <strong>When</strong>:Saturday 16th
                                                  <br>
                                                  <strong>Time</strong>: 9 am to 4 pm
                                                  <br>
                                                  <strong>Bring</strong>: Old clothes and your lunch
                                                  <br>
                                                  <strong>Cost</strong>: $95</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://gifthutt.nz/product/p/" target="_blank" class="x_mcnButton" title="Purchase ticket" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Purchase ticket</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnDividerBlock" style="min-width:100%; border-collapse:collapse; table-layout:fixed!important">
                          <tbody class="x_mcnDividerBlockOuter">
                            <tr>
                              <td class="x_mcnDividerBlockInner" style="min-width:100%; padding:14px 18px">
                                <table class="x_mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-top-width:2px; border-top-style:solid; border-top-color:#702E61; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style=""><span></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/fa41434d-778f-4a0c-a659-2f92beca0ca3.jpg" alt="" width="264" class="x_mcnImage" style="max-width:500px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Make 4 Masculine Cards</h3>
                                                <div style="text-align:left">Masculine cards are so difficult to make at times so this is a good opportunity to get some cards made. &nbsp;Pick your own designs from a range of Heartfelt Creations Under the Sea and Italiana Riveria
                                                  paper collections. &nbsp;
                                                  <br>
                                                  <br>
                                                  <strong>When</strong>:Sunda&nbsp;24th July
                                                  <br>
                                                  <strong>Time</strong>: 2 pm to 4 pm
                                                  <br>
                                                  <strong>Bring</strong>: Your favourite adhesives and tools.
                                                  <br>
                                                  <strong>Cost</strong>: $20</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://gifthutt.nz/product/masculine-card-class/" target="_blank" class="x_mcnButton" title="Purchase ticket" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Purchase ticket</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="x_mcnDividerBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse; table-layout:fixed!important">
                          <tbody class="x_mcnDividerBlockOuter">
                            <tr>
                              <td class="x_mcnDividerBlockInner" style="min-width:100%; padding:14px 18px">
                                <table class="x_mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-top:2px solid #702E61; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style=""><span></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/f103eb82-3b02-499a-9069-e4e8d9346ffd.jpg" alt="" width="264" class="x_mcnImage" style="max-width:678px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Furniture Painting Workshop</h3>
                                                <div style="text-align:left">Come along and have some fun while learning how to use the fabulous Voodoo Molly Vintage Paints.
                                                  <br>
                                                  <br>
                                                  <strong>When</strong>: Thursday 28th July
                                                  <br>
                                                  <strong>Time</strong>: 9.30 am to 3.30&nbsp;pm
                                                  <br>
                                                  <strong>Bring</strong>: A piece of furniture or two to paint. Bring your lunch - tea, coffee and biscuits will be provided.
                                                  <br>
                                                  <strong>Cost</strong>: $85
                                                  <br> &nbsp;&nbsp;
                                                </div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://gifthutt.nz/product/furniture-painting-workshop-voodoo-molly-paints-sunday-29th-may/" target="_blank" class="x_mcnButton" title="Purchase ticket" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Purchase ticket</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnDividerBlock" style="min-width:100%; border-collapse:collapse; table-layout:fixed!important">
                          <tbody class="x_mcnDividerBlockOuter">
                            <tr>
                              <td class="x_mcnDividerBlockInner" style="min-width:100%; padding:14px 18px">
                                <table class="x_mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-top-width:2px; border-top-style:solid; border-top-color:#702E61; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style=""><span></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/558f375f-2c8f-46b0-9250-d994ca01ff21.jpg" alt="" width="264" class="x_mcnImage" style="max-width:333px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Make a Footstool</h3>
                                                <div style="text-align:left">Come along and learn the basics of upholstery and go home with this lovely footstool.
                                                  <br>
                                                  <br>
                                                  <strong>When</strong>: Saturday 30th July
                                                  <br>
                                                  <strong>Time</strong>: 10.30 am to 3 pm
                                                  <br>
                                                  <strong>Bring</strong>: 1 metre of material - upholstery material is best but a soft furnishing fabric will work as well.
                                                  <br> Tea, coffee and biscuits will be provided.
                                                  <br>
                                                  <strong>Cost</strong>: $100</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://gifthutt.nz/product/furniture-painting-workshop-voodoo-molly-paints-sunday-29th-may/" target="_blank" class="x_mcnButton" title="Purchase ticket" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Purchase ticket</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/8b6c9066951558780aff2cb12/images/29fa515c-bab9-4ea3-9e0f-78a6ea1c62b3.jpg" alt="" width="264" class="x_mcnImage" style="max-width:650px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Giveaway!</h3>
                                                <div style="text-align:left">Enter this draw for your chance to WIN!
                                                  <br>
                                                  <br>
                                                  <strong>Prize</strong>: A GIFT VOUCHER worth $25 from the GIFT HUTT store
                                                  <br>
                                                  <strong>Ends</strong>: 12th August 2016</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://dgo.nz/jqzQSlka4018" target="_blank" class="x_mcnButton" title="Enter now" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Enter now</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>





          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>
';
      echo $sendGridApiHtml;




 ?>

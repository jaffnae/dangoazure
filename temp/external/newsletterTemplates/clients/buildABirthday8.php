<?php
    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Build a Birthday";
    $fromNameAndEmailObject["email"] = "fay@buildabirthday.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_buildABirthday&newsletterTemplateName=clients/buildABirthday8&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_buildABirthday&newsletterTemplateName=clients/buildABirthday8&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <html>
  <head>
    <style type="text/css"></style>
  </head>

  <body style="background: #f3f3f3; margin: 0; padding: 0; text-align: center; ">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td valign="top" style="background: #fff;">
            <img src="http://i.imgur.com/boSoCyh.png" alt="Logo" width="50%">
          </td>
        </tr>
      </tbody>
    </table>




    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left; margin: 0 auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <hr />

            <div style="text-align: left; padding: 10px; font-size: 12px; color: #666">
              <span style=" ">Welcome to your Build a Birthday newsletter, bringing you fun, helpful tips and useful information. In the news is:</span>

              <ul>
                <li>
                   101 uses for cookie cutters - including Christmas ideas!
                 </li>
                <li>
                   New in-store - Christmas Crackers and Advent Calendars!
                 </li>
                <li>
                    Dollar Deals and discounts - Ideal Christmas gifts!
                 </li>
                <li>
                   Win an advent calendar!!!!
                 </li>
              </ul>
            </div>

            <hr />
          </td>
        </tr>
      </tbody>
    </table>






    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">101 uses for cookie cutters:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/Pcqxqgi.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              I can\'t believe there are so many uses for a cookie cutter. Here\'s loads of ideas for parties, Christmas and other special occasions. Not to forget the fun craft ideas! Take a look at some of these gift ideas, my favourite is the crayon shapes. They are perfect for party bag fillers or as a gift. It\'s both cost effective and reduces waste.
 <br /><br />See our Cookie cutter ideas at <br /> <a href="http://www.buildabirthday.co.nz/blogs/for-the-blog/101-uses-for-cookie-cutters">http://www.buildabirthday.co.nz/blogs/for-the-blog/101-uses-for-cookie-cutters</a>
            </div>
          </td>
        </tr>
      </tbody>
    </table>










    <br />





    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">New in-store:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/LyYWW2g.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />Mark the days to Christmas with a designer advent calendar. We\'ve listened to your requests and have increased our range of religious and kids advent calendars. Plus check out our fun Christmas Crackers, Twister, Cluedo, Guess Who and Trivial Pursuits. Ideal for a fun packed Christmas day! These have been rocking off the shelves so grab one now.
              <br /> <br />
See of full range of Christmas supplies at:
<a href="http://www.buildabirthday.co.nz/collections/christmas">http://www.buildabirthday.co.nz/collections/christmas</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>




    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: center; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Dollar deals and discounts - Ideal Christmas gifts:</span>
            <br />

            <br />
            <div style="text-align: center; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <b style="font-size: 15px;">Was $79.99, now $39.99:</b>
            </div>
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/FQht2GQ.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">
<br />

            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />Give the baker in your life the perfect gift with this delicious set from Meri Meri. Containing an array of tools and equipment, they\'ll be able to bake some goodies and store them within the beautiful decorative tin afterwards!
             <br /><br />

 See<a href="http://www.buildabirthday.co.nz/collections/clearance-50-off"> http://www.buildabirthday.co.nz/collections/clearance-50-off</a> and <a href="http://www.buildabirthday.co.nz/pages/sale">http://www.buildabirthday.co.nz/pages/sale</a>

              <br /><br />


            </div>
          </td>
        </tr>
      </tbody>
    </table>






    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Competition:</span>
            <br />
            <br />


            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/kAe2vBO.jpg" style="border: 1px solid #c7c2c2;width: 350px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: center; padding-bottom: 15px; color: #666; font-size: 18px; ">
             <br /> <br />Keep an eye out of facebook for the opportunity to win this cute Santa Advent Calendar from Meri Meri. Starts 8th November, 8pm!
                <br />
                 <br />
                See more on <a href="www.facebook.com/buildabirthday">www.facebook.com/buildabirthday</a>
            </div>
          </td>
        </tr>
      </tbody>
    </table>





    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
        </tr>
        <tr>
          <td align="center" valign="top" colspan="2">
            <div style="padding-left: 8px; padding-right: 8px;">
              <br />
              <span style="font-size: 13px; color: #666;">Don\'t forget to Like us on</span>
            </div>

            <p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 5px 0;">


              <img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Facebook</a></strong> &nbsp; and &nbsp;

              <img src="http://i.imgur.com/cxoH7Pg.png" width="30" alt="Instagram" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Instagram</a></strong>

            </p>

            <div style="padding-left: 8px; padding-right: 8px;">
              <span style="font-size: 13px; color: #666;">to be kept up to date to the lastest information on parties and cake decorating.</span>
            </div>
            <br />
            <br />
            <br />
            <br />
            <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
              If you received this message in error please disregard and delete this.
              <br> You can also unsubscribe from the marketing material and customer loyalty by following
              <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_buildABirthday" style="color: #aaa;">this link</a></p>
          </td>
        </tr>
      </tbody>
    </table>


  </body>

</html>




';
      echo $sendGridApiHtml;




 ?>

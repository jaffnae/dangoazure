<?php
    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Tiny Turtles";
    $fromNameAndEmailObject["email"] = "info@tinyturtles.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_tinyTurtles&newsletterTemplateName=clients/tinyTurtles1&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_tinyTurtles&newsletterTemplateName=clients/tinyTurtles1&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <!doctype html>
    <html>
    <head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
    <style type="text/css">
    body {font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 140%;}
    img {display:block;}
    p, ul, ol, li { font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 140%; margin: 0; padding: 0; padding-bottom: 10px; }
    ul, ol, li { margin-left: 10px; padding-left: 10px; }
    a {
    	font-family: Helvetica, Arial, sans-serif;
    	color: #00A69C;
    	text-decoration: none;
    }
    a.block-link {
    	color: #fff;
    	background-color: #00A69C;
    	text-transform: uppercase;
    	padding: 10px 12px;
        margin: 5px 0px;
        display: inline-block;
    }
    ul.nav {
        width: 100%;
        display: block;
        font-size: 14px;
        text-transform: uppercase;
        color: #00A69C;
        border-top: 1px solid #EEEEEE;
        border-bottom: 1px solid #EEEEEE;
        margin: 35px 0 0 0;
        padding: 15px 0 0 0;
        height: 30px;
        padding-left: 0px !important;
        margin-left: 0px !important;
    }
    ul.nav li {
        font-size: 14px;
        float: left;
        list-style: none;
        margin: 0;
        padding: 0 50px 0 0;
        padding-left: 0px !important;
        margin-left: 0px !important;
    }
    ul.nav li.last {
        padding: 0;
        padding-left: 0px !important;
        margin-left: 0px !important;
    }
    p.disclaimer {
        padding: 30px 0;
    }
    .txt-container {
        padding: 20px 0px;
    }
    .txt-container p {
        font-size: 16px;
    }
    p.subhead {
        text-transform: uppercase;
        font-weight: bold;
        border-top: 2px solid black;
        font-family: Helvetica Neue, Heletica, Arial;
        margin-top: 10px;
    }
    p.intro {
        font-style: italic;
        font-size: 18px;
        line-height: 160%;
    }
    </style>
    </head>

    <body style="padding: 0;margin: 15px auto; background-color: #fff;">

    <table width="645" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
    	<td align="left" width="25%" valign="bottom">
        	<table border="0" cellpadding="0" cellspacing="0"><tr>
            	<td><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/83c21b11-c6d4-4ff3-a21c-7362acdd6728.gif" width="39" height="54" alt="Join us on Facebook" style="margin-right: 10px;" border="0"></a></td>
                <td><a href="https://twitter.com/TinyTurtles_NZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3f7d7a96-77bb-445c-9c60-e61cfd6b0ef2.gif" width="39" height="54" alt="Join us on Twitter" border="0"></a></td>
          </tr></table>
        </td>
        <td align="center" width="50%"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/f9ce15fa-bd61-4b1e-9859-a1a655737f85.gif" width="260" height="126" alt="Shop for BONDS on Tiny Turtles.co.nz" border="0"></a></td>
        <td align="right" width="25%" valign="bottom"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3e5dad31-f112-44db-ab59-7ca98ec53010.gif" width="101" height="105" alt="Free shipping on orders over $70!" border="0"></a></td>
    </tr>
    <tr>
        <td colspan="3">
            <ul class="nav">
                <li><a href="http://www.tinyturtles.co.nz/2016">New</a></li>
                <li><a href="http://www.tinyturtles.co.nz/bonds-baby-boys-clothing">Boys</a></li>
                <li><a href="http://www.tinyturtles.co.nz/bonds-baby-girls-clothing">Girls</a></li>
                <li><a href="http://www.tinyturtles.co.nz/Bonds-Baby-newborn-clothing">Newborns</a></li>
                <li><a href="http://www.tinyturtles.co.nz/wondersuit">Wondersuits</a></li>
                <li class="last"><a href="http://www.tinyturtles.co.nz/Bonds-Outerwear">Outerwear</a></li>
            </ul>
        </td>
    </tr>

    <tr>
        <td colspan="3"><a href="http://www.tinyturtles.co.nz/2016"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/b73fd17d-a970-4ea5-9032-ad3b103f298d.jpg" width="645" height="305" border="0" alt="new summer styles!"></a></td>
    </tr>
    <tr>
        <td colspan="3">
        <div class="txt-container">
            <p>NEW STYLES, for Summer and Christmas </p>
            <p>Have a look online now as we have lots of fantastic New Summer and Christmas Styles, there is lots to see and some super fun designs.  </p>
            <p><em> -Eeeek</em></p>
        </div>

    </td>
    </tr>

    <tr>
    <td colspan="3">
    <a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/7efbb7e1-2b39-4024-b96f-ee82c5e85784.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>

    </tr>
    <tr>
        <td colspan="3"><a href="http://www.tinyturtles.co.nz/Bonds-Maternity"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/288a59de-24b8-4bd1-8d7b-8e1d79115b6d.jpg" width="645" height="297" border="0" alt="Restock of New Maternity Crop"></a></td>
    </tr>
    <tr>
        <td colspan="3">
        <div class="txt-container">
            <p>New Maternity Restock, yes we have managed to get more of the very popular Maternity Feeding Crops.. <br>
        </p>
            <p><em>Comfortable, Stretchy, Soft, Washable Inserts &amp; Super Popular xxx    </em></p>
        </div>
    </td>
    </tr>

    <tr>
    <td colspan="3">
    <a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/7efbb7e1-2b39-4024-b96f-ee82c5e85784.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>
    </tr>
    <tr>
        <td colspan="3"><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/1a4c20fd-eb58-467f-a426-6b412c08db95.jpg" width="645" height="485" border="0" alt="We Love Marys Market!"></a></td>
    </tr>
    <tr>
        <td colspan="3">
        <div class="txt-container">
            <p>Mary\'s Market is Saturday the 8th of October, so if you can make it come and say hi, and of course grab yourself some great BONDS bargains!</p>
        </div>
    </td>
    </tr>

    <tr>
    <td colspan="3">
    <a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/7efbb7e1-2b39-4024-b96f-ee82c5e85784.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>
    </tr>




    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    </table>


    <table width="645" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td width="215" height="215"><a href="http://vimeo.com/102892239"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/27aa85ea-9017-4299-8a5f-9acb15d6abdd.gif" width="215" height="215" border="0" alt="Baby Search Winners Video"></a></td>
        <td width="215" height="215"><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/d361fecc-2a3f-40e3-8c61-2ffb2aedbb8f.gif" width="215" border="0" height="215" alt="Join us on Facebook"></a></td>
        <td width="215" height="215"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3d22163f-7c03-4c79-9b7b-ac98c7c73a5b.gif" width="215" height="215" border="0" alt="Free shipping on orders over $70 because we love you"></a></td>
    </tr>
    </table>

                <center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="canspamBarWrapper" style="background-color:#FFFFFF; border-top:1px solid #E5E5E5;">
                        <tr>
                            <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px;">
                                <table border="0" cellpadding="0" cellspacing="0" id="canspamBar">
                                    <tr>
                                        <td align="center" valign="top" style="color:#606060; font-family:Helvetica, Arial, sans-serif; font-size:11px; line-height:150%; padding-right:20px; padding-bottom:5px; padding-left:20px; text-align:center;">









                                            <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
                  If you received this message in error please disregard and delete this.
                  <br> You can also unsubscribe from the marketing material by following
                  <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_tinyTurtles" style="color: #aaa;">this link</a></p>










                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <style type="text/css">
                        @media only screen and (max-width: 480px){
                            table[id="canspamBar"] td{font-size:14px !important;}
                            table[id="canspamBar"] td a{display:block !important; margin-top:10px !important;}
                        }
                    </style>
                </center></body>
    </html>

';
      echo $sendGridApiHtml;




 ?>

<?php
    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Build a Birthday";
    $fromNameAndEmailObject["email"] = "fay@buildabirthday.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_buildABirthday&newsletterTemplateName=clients/buildABirthday9&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_buildABirthday&newsletterTemplateName=clients/buildABirthday9&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <html>

  <head>
    <style type="text/css"></style>
  </head>

  <body style="background: #f3f3f3; margin: 0; padding: 0; text-align: center; ">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td valign="top" style="background: #fff;">
            <img src="http://i.imgur.com/boSoCyh.png" alt="Logo" width="50%">
          </td>
        </tr>
      </tbody>
    </table>




    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left; margin: 0 auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <hr />

            <div style="text-align: left; padding: 10px; font-size: 12px; color: #666">
              <span style=" ">Welcome to your Build a Birthday newsletter, bringing you fun, helpful tips and useful information. In the news is:</span>

              <ul>
                <li>
                  Tips on running a Secret Santa Pressie Swap!
                </li>
                <li>
                  New in-store - Cookie Spoon Trays
                </li>
                <li>
                  Our Favourite Christmas Cookie Recipe
                </li>
                <li>
                  New SALE DEALS and more DOLLAR DEALS!
                </li>
                <li>
                  Win $500 of prizes from our mystery tree!!!!
                </li>
              </ul>
            </div>

            <hr />
          </td>
        </tr>
      </tbody>
    </table>






    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Secret Santa Time:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/khsuKRd.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
             It\'s that time of the year, with work functions and all manner of end of year parties. Why not add a little Christmas Spirit with a Secret Santa Pressie swap. They work well for both kids and adults, with "Santa" delivering them or the "gift exchange".  Check out our Secret Santa Guide for more details at  <a href="http://www.buildabirthday.co.nz/blogs/for-the-blog/secret-santa-gift-guide">http://www.buildabirthday.co.nz/blogs/for-the-blog/secret-santa-gift-guide</a>
            </div>
          </td>
        </tr>
      </tbody>
    </table>










    <br />





    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">New in-store:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/5vL4G2a.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />Loving these new cookie spoon tins.  Add Christmas sprinkles for festive nibbles with coffee, then use them later for a vintage afternoon tea or party, with pretty pastels and sugar delight. We only have a few left, but can get them out to you in plenty of time for Christmas.

              <br />
              <br /> See of full range of Christmas supplies at:
              <a href="http://www.buildabirthday.co.nz/collections/christmas-food-baking">http://www.buildabirthday.co.nz/collections/christmas-food-baking</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>




    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: center; Geneva, sans-serif;">
            <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Our Favourite Xmas Cookie Recipe:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/JgfkJVN.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">
            <br />

            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />Yes, we are sharing our favourite recipe for Xmas cookies. The perfect gift for teachers, work mates, the milkman or just a fun activity for the kids.  See our range of gift boxes to dress them up in.
              <br />
              <br /> See our recipe at

              <a href="http://www.buildabirthday.co.nz/blogs/for-the-blog/our-favourite-xmas-cookie-recipe"> http://www.buildabirthday.co.nz/blogs/for-the-blog/our-favourite-xmas-cookie-recipe</a>

              <br />
              <br />


            </div>
          </td>
        </tr>
      </tbody>
    </table>







    <br />








    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: center; Geneva, sans-serif;">
            <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">New Sale Deals and more Dollar deals:</span>
            <br />

            <br />
            <div style="text-align: center; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <b style="font-size: 15px;">FREE cupcake papers & toppers with the Meri Meri Fairy Magic cupcake stand:</b>
            </div>
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/5snX7Q9.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">
            <br />

            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />We\'ve created some fantastic deals and discounted more items. A great time to grab a bargain before your next party.
              <br />
              <br /> See

              <a href="http://www.buildabirthday.co.nz/collections/on-sale"> http://www.buildabirthday.co.nz/collections/on-sale </a>

              <br />
              <br />


            </div>
          </td>
        </tr>
      </tbody>
    </table>






    <br />








    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Competition:</span>
            <br />
            <br />


            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/bpqN0sP.jpg" style="border: 1px solid #c7c2c2;width: 350px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: center; padding-bottom: 15px; padding-left: 10px; padding-right: 10px; color: #666; font-size: 18px; ">
              <br />
              <br />Purchase in-store (joining our loyalty scheme) or on-line and go in the draw to win our $500 mystery bag tree!  We have featured our mystery bag tree in the Petone store window. The lucky winner will receive gifts valued at $500, drawn on 20th December 2016.
            </div>
          </td>
        </tr>
      </tbody>
    </table>





    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
        </tr>
        <tr>
          <td align="center" valign="top" colspan="2">
            <div style="padding-left: 8px; padding-right: 8px;">
              <br />
              <span style="font-size: 13px; color: #666;">Don\'t forget to Like us on</span>
            </div>

            <p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 5px 0;">


              <img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Facebook</a></strong> &nbsp; and &nbsp;

              <img src="http://i.imgur.com/cxoH7Pg.png" width="30" alt="Instagram" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Instagram</a></strong>

            </p>

            <div style="padding-left: 8px; padding-right: 8px;">
              <span style="font-size: 13px; color: #666;">to be kept up to date to the lastest information on parties and cake decorating.</span>
            </div>
            <br />
            <br />
            <br />
            <br />
            <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
              If you received this message in error please disregard and delete this.
              <br> You can also unsubscribe from the marketing material and customer loyalty by following
              <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_buildABirthday" style="color: #aaa;">this link</a></p>
          </td>
        </tr>
      </tbody>
    </table>


  </body>

</html>





';
      echo $sendGridApiHtml;




 ?>

<?php


/**************************************************
       ! ! ! ! ! WARNING ! ! ! ! ! !
***************************************************/
//Make sure the code for Unsubscribe is on the bottom:
/*

<!-- BEGIN FOOTER -->
<table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
  <tbody class="mcnTextBlockOuter">
   <tr>
      <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
       <!--[if mso]>
<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
<tr>
<![endif]-->

       <!--[if mso]>
<td valign="top" width="599" style="width:599px;">
<![endif]-->
       <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:100%; min-width:100%;" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>

             <td class="mcnTextContent" valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                <em>Copyright © 2016 Totally Balmy New Zealand, All rights reserved.</em>
                <br> You\'ve received this email because you opted in during a Dango promotion, a competition, or on my website:
                <a style="color:#656565;font-weight:normal;text-decoration:underline" href="http://totallybalmy.us13.list-manage.com/track/click?u=7a555c31cf20566e9ab7c0dd2&amp;id=3f2c8efbfd&amp;e=17138fb4ea"
                target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://totallybalmy.us13.list-manage.com/track/click?u%3D7a555c31cf20566e9ab7c0dd2%26id%3D3f2c8efbfd%26e%3D17138fb4ea&amp;source=gmail&amp;ust=1479780733183000&amp;usg=AFQjCNHJ0NI9xOKqjj2kWLXGMOYiGjUD9A"><font color="#1155cc">www.totallybalmy.co.nz</font></a>
                <br>
                <br>
                <strong>Our mailing address is:</strong>
                <br> 4 Mahi Grove, Feilding, 4702
                <br>
                <br> Want to change how you receive these emails?
                <br> You can <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_totallyBalmy">unsubcribe here</a>
                <br> &nbsp;
             </td>
            </tr>
          </tbody>
       </table>
       <!--[if mso]>
</td>
<![endif]-->

       <!--[if mso]>
</tr>
</table>
<![endif]-->
      </td>
   </tr>
  </tbody>
</table>
<!-- /END FOOTER -->


*/

    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Totally Balmy";
    $fromNameAndEmailObject["email"] = "info@totallybalmy.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there\'s a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_totallyBalmy&newsletterTemplateName=clients/totallyBalmy5&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_totallyBalmy&newsletterTemplateName=clients/totallyBalmy5&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
      <!--[if gte mso 15]>
      <xml>
          <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <meta charset="UTF-8">
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Charcoal Acne Soap back in stock...</title>

     <style type="text/css">
      p{
          margin:10px 0;
          padding:0;
      }
      table{
          border-collapse:collapse;
      }
      h1,h2,h3,h4,h5,h6{
          display:block;
          margin:0;
          padding:0;
      }
      img,a img{
          border:0;
          height:auto;
          outline:none;
          text-decoration:none;
      }
      body,#bodyTable,#bodyCell{
          height:100%;
          margin:0;
          padding:0;
          width:100%;
      }
      #outlook a{
          padding:0;
      }
      img{
          -ms-interpolation-mode:bicubic;
      }
      table{
          mso-table-lspace:0pt;
          mso-table-rspace:0pt;
      }
      .ReadMsgBody{
          width:100%;
      }
      .ExternalClass{
          width:100%;
      }
      p,a,li,td,blockquote{
          mso-line-height-rule:exactly;
      }
      a[href^=tel],a[href^=sms]{
          color:inherit;
          cursor:default;
          text-decoration:none;
      }
      p,a,li,td,body,table,blockquote{
          -ms-text-size-adjust:100%;
          -webkit-text-size-adjust:100%;
      }
      .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
          line-height:100%;
      }
      a[x-apple-data-detectors]{
          color:inherit !important;
          text-decoration:none !important;
          font-size:inherit !important;
          font-family:inherit !important;
          font-weight:inherit !important;
          line-height:inherit !important;
      }
      #bodyCell{
          padding:10px;
      }
      .templateContainer{
          max-width:600px !important;
      }
      a.mcnButton{
          display:block;
      }
      .mcnImage{
          vertical-align:bottom;
      }
      .mcnTextContent{
          word-break:break-word;
      }
      .mcnTextContent img{
          height:auto !important;
      }
      .mcnDividerBlock{
          table-layout:fixed !important;
      }
    /*
    @tab Page
    @section Background Style
    @tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
    */
      body,#bodyTable{
          /*@editable*/background-color:#FAFAFA;
      }
    /*
    @tab Page
    @section Background Style
    @tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
    */
      #bodyCell{
          /*@editable*/border-top:0;
      }
    /*
    @tab Page
    @section Email Border
    @tip Set the border for your email.
    */
      .templateContainer{
          /*@editable*/border:0;
      }
    /*
    @tab Page
    @section Heading 1
    @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
    @style heading 1
    */
      h1{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:26px;
          /*@editable*/font-style:normal;
          /*@editable*/font-weight:bold;
          /*@editable*/line-height:125%;
          /*@editable*/letter-spacing:normal;
          /*@editable*/text-align:left;
      }
    /*
    @tab Page
    @section Heading 2
    @tip Set the styling for all second-level headings in your emails.
    @style heading 2
    */
      h2{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:22px;
          /*@editable*/font-style:normal;
          /*@editable*/font-weight:bold;
          /*@editable*/line-height:125%;
          /*@editable*/letter-spacing:normal;
          /*@editable*/text-align:left;
      }
    /*
    @tab Page
    @section Heading 3
    @tip Set the styling for all third-level headings in your emails.
    @style heading 3
    */
      h3{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:20px;
          /*@editable*/font-style:normal;
          /*@editable*/font-weight:bold;
          /*@editable*/line-height:125%;
          /*@editable*/letter-spacing:normal;
          /*@editable*/text-align:left;
      }
    /*
    @tab Page
    @section Heading 4
    @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
    @style heading 4
    */
      h4{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:18px;
          /*@editable*/font-style:normal;
          /*@editable*/font-weight:bold;
          /*@editable*/line-height:125%;
          /*@editable*/letter-spacing:normal;
          /*@editable*/text-align:left;
      }
    /*
    @tab Preheader
    @section Preheader Style
    @tip Set the background color and borders for your email\'s preheader area.
    */
      #templatePreheader{
          /*@editable*/background-color:#FAFAFA;
          /*@editable*/background-image:none;
          /*@editable*/background-repeat:no-repeat;
          /*@editable*/background-position:center;
          /*@editable*/background-size:cover;
          /*@editable*/border-top:0;
          /*@editable*/border-bottom:0;
          /*@editable*/padding-top:9px;
          /*@editable*/padding-bottom:9px;
      }
    /*
    @tab Preheader
    @section Preheader Text
    @tip Set the styling for your email\'s preheader text. Choose a size and color that is easy to read.
    */
      #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
          /*@editable*/color:#656565;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:12px;
          /*@editable*/line-height:150%;
          /*@editable*/text-align:left;
      }
    /*
    @tab Preheader
    @section Preheader Link
    @tip Set the styling for your email\'s preheader links. Choose a color that helps them stand out from your text.
    */
      #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
          /*@editable*/color:#656565;
          /*@editable*/font-weight:normal;
          /*@editable*/text-decoration:underline;
      }
    /*
    @tab Header
    @section Header Style
    @tip Set the background color and borders for your email\'s header area.
    */
      #templateHeader{
          /*@editable*/background-color:#FFFFFF;
          /*@editable*/background-image:none;
          /*@editable*/background-repeat:no-repeat;
          /*@editable*/background-position:center;
          /*@editable*/background-size:cover;
          /*@editable*/border-top:0;
          /*@editable*/border-bottom:0;
          /*@editable*/padding-top:9px;
          /*@editable*/padding-bottom:0;
      }
    /*
    @tab Header
    @section Header Text
    @tip Set the styling for your email\'s header text. Choose a size and color that is easy to read.
    */
      #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:16px;
          /*@editable*/line-height:150%;
          /*@editable*/text-align:left;
      }
    /*
    @tab Header
    @section Header Link
    @tip Set the styling for your email\'s header links. Choose a color that helps them stand out from your text.
    */
      #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
          /*@editable*/color:#2BAADF;
          /*@editable*/font-weight:normal;
          /*@editable*/text-decoration:underline;
      }
    /*
    @tab Body
    @section Body Style
    @tip Set the background color and borders for your email\'s body area.
    */
      #templateBody{
          /*@editable*/background-color:#FFFFFF;
          /*@editable*/background-image:none;
          /*@editable*/background-repeat:no-repeat;
          /*@editable*/background-position:center;
          /*@editable*/background-size:cover;
          /*@editable*/border-top:0;
          /*@editable*/border-bottom:0;
          /*@editable*/padding-top:9px;
          /*@editable*/padding-bottom:0;
      }
    /*
    @tab Body
    @section Body Text
    @tip Set the styling for your email\'s body text. Choose a size and color that is easy to read.
    */
      #templateBody .mcnTextContent,#templateBody .mcnTextContent p{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:16px;
          /*@editable*/line-height:150%;
          /*@editable*/text-align:left;
      }
    /*
    @tab Body
    @section Body Link
    @tip Set the styling for your email\'s body links. Choose a color that helps them stand out from your text.
    */
      #templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
          /*@editable*/color:#2BAADF;
          /*@editable*/font-weight:normal;
          /*@editable*/text-decoration:underline;
      }
    /*
    @tab Columns
    @section Column Style
    @tip Set the background color and borders for your email\'s columns.
    */
      #templateColumns{
          /*@editable*/background-color:#FFFFFF;
          /*@editable*/background-image:none;
          /*@editable*/background-repeat:no-repeat;
          /*@editable*/background-position:center;
          /*@editable*/background-size:cover;
          /*@editable*/border-top:0;
          /*@editable*/border-bottom:2px solid #EAEAEA;
          /*@editable*/padding-top:0;
          /*@editable*/padding-bottom:9px;
      }
    /*
    @tab Columns
    @section Column Text
    @tip Set the styling for your email\'s column text. Choose a size and color that is easy to read.
    */
      #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
          /*@editable*/color:#202020;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:16px;
          /*@editable*/line-height:150%;
          /*@editable*/text-align:left;
      }
    /*
    @tab Columns
    @section Column Link
    @tip Set the styling for your email\'s column links. Choose a color that helps them stand out from your text.
    */
      #templateColumns .columnContainer .mcnTextContent a,#templateColumns .columnContainer .mcnTextContent p a{
          /*@editable*/color:#2BAADF;
          /*@editable*/font-weight:normal;
          /*@editable*/text-decoration:underline;
      }
    /*
    @tab Footer
    @section Footer Style
    @tip Set the background color and borders for your email\'s footer area.
    */
      #templateFooter{
          /*@editable*/background-color:#FAFAFA;
          /*@editable*/background-image:none;
          /*@editable*/background-repeat:no-repeat;
          /*@editable*/background-position:center;
          /*@editable*/background-size:cover;
          /*@editable*/border-top:0;
          /*@editable*/border-bottom:0;
          /*@editable*/padding-top:9px;
          /*@editable*/padding-bottom:9px;
      }
    /*
    @tab Footer
    @section Footer Text
    @tip Set the styling for your email\'s footer text. Choose a size and color that is easy to read.
    */
      #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
          /*@editable*/color:#656565;
          /*@editable*/font-family:Helvetica;
          /*@editable*/font-size:12px;
          /*@editable*/line-height:150%;
          /*@editable*/text-align:center;
      }
    /*
    @tab Footer
    @section Footer Link
    @tip Set the styling for your email\'s footer links. Choose a color that helps them stand out from your text.
    */
      #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
          /*@editable*/color:#656565;
          /*@editable*/font-weight:normal;
          /*@editable*/text-decoration:underline;
      }
    @media only screen and (min-width:768px){
      .templateContainer{
          width:600px !important;
      }

}	@media only screen and (max-width: 480px){
      body,table,td,p,a,li,blockquote{
          -webkit-text-size-adjust:none !important;
      }

}	@media only screen and (max-width: 480px){
      body{
          width:100% !important;
          min-width:100% !important;
      }

}	@media only screen and (max-width: 480px){
      #bodyCell{
          padding-top:10px !important;
      }

}	@media only screen and (max-width: 480px){
      .columnWrapper{
          max-width:100% !important;
          width:100% !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImage{
          width:100% !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
          max-width:100% !important;
          width:100% !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnBoxedTextContentContainer{
          min-width:100% !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImageGroupContent{
          padding:9px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
          padding-top:9px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
          padding-top:18px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImageCardBottomImageContent{
          padding-bottom:9px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImageGroupBlockInner{
          padding-top:0 !important;
          padding-bottom:0 !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImageGroupBlockOuter{
          padding-top:9px !important;
          padding-bottom:9px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnTextContent,.mcnBoxedTextContentColumn{
          padding-right:18px !important;
          padding-left:18px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
          padding-right:18px !important;
          padding-bottom:0 !important;
          padding-left:18px !important;
      }

}	@media only screen and (max-width: 480px){
      .mcpreview-image-uploader{
          display:none !important;
          width:100% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Heading 1
    @tip Make the first-level headings larger in size for better readability on small screens.
    */
      h1{
          /*@editable*/font-size:22px !important;
          /*@editable*/line-height:125% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Heading 2
    @tip Make the second-level headings larger in size for better readability on small screens.
    */
      h2{
          /*@editable*/font-size:20px !important;
          /*@editable*/line-height:125% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Heading 3
    @tip Make the third-level headings larger in size for better readability on small screens.
    */
      h3{
          /*@editable*/font-size:18px !important;
          /*@editable*/line-height:125% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Heading 4
    @tip Make the fourth-level headings larger in size for better readability on small screens.
    */
      h4{
          /*@editable*/font-size:16px !important;
          /*@editable*/line-height:150% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Boxed Text
    @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    */
      .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
          /*@editable*/font-size:14px !important;
          /*@editable*/line-height:150% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Preheader Visibility
    @tip Set the visibility of the email\'s preheader on small screens. You can hide it to save space.
    */
      #templatePreheader{
          /*@editable*/display:block !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Preheader Text
    @tip Make the preheader text larger in size for better readability on small screens.
    */
      #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
          /*@editable*/font-size:14px !important;
          /*@editable*/line-height:150% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Header Text
    @tip Make the header text larger in size for better readability on small screens.
    */
      #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
          /*@editable*/font-size:16px !important;
          /*@editable*/line-height:150% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Body Text
    @tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    */
      #templateBody .mcnTextContent,#templateBody .mcnTextContent p{
          /*@editable*/font-size:16px !important;
          /*@editable*/line-height:150% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Column Text
    @tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    */
      #templateColumns .columnContainer .mcnTextContent,#templateColumns .columnContainer .mcnTextContent p{
          /*@editable*/font-size:16px !important;
          /*@editable*/line-height:150% !important;
      }

}	@media only screen and (max-width: 480px){
    /*
    @tab Mobile Styles
    @section Footer Text
    @tip Make the footer content text larger in size for better readability on small screens.
    */
      #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
          /*@editable*/font-size:14px !important;
          /*@editable*/line-height:150% !important;
      }

}</style></head>
     <body>
         <center>
             <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                 <tr>
                     <td align="center" valign="top" id="bodyCell">
                   <!-- BEGIN TEMPLATE // -->
                   <!--[if gte mso 9]>
                   <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                   <tr>
                   <td align="center" valign="top" width="600" style="width:600px;">
                   <![endif]-->
                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                      <tr>
                         <td valign="top" id="templatePreheader"><table class="mcnTextBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnTextBlockOuter">
         <tr>
             <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
             <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
             <tr>
             <![endif]-->

             <!--[if mso]>
             <td valign="top" width="390" style="width:390px;">
             <![endif]-->
                 <table style="max-width:390px;" class="mcnTextContentContainer" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                     <tbody><tr>

                         <td class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;" valign="top">

                             New products, winners, and more...
                         </td>
                     </tr>
                 </tbody></table>
             <!--[if mso]>
             </td>
             <![endif]-->

             <!--[if mso]>
             <td valign="top" width="210" style="width:210px;">
             <![endif]-->
                 <table style="max-width:210px;" class="mcnTextContentContainer" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                     <tbody><tr>

                         <td class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;" valign="top">

                         </td>
                     </tr>
                 </tbody></table>
             <!--[if mso]>
             </td>
             <![endif]-->

             <!--[if mso]>
             </tr>
             </table>
             <![endif]-->
             </td>
         </tr>
     </tbody>
</table></td>
                      </tr>
                      <tr>
                         <td valign="top" id="templateHeader"><table class="mcnImageBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnImageBlockOuter">
             <tr>
                 <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                     <table class="mcnImageContentContainer" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                         <tbody><tr>
                             <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">


                                         <img alt="" src="https://gallery.mailchimp.com/7a555c31cf20566e9ab7c0dd2/images/8f9176e3-1ee0-4ffe-a9b3-7b4c181586dc.png" style="max-width:250px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="250" align="middle">


                             </td>
                         </tr>
                     </tbody></table>
                 </td>
             </tr>
     </tbody>
</table></td>
                      </tr>
                      <tr>
                         <td valign="top" id="templateBody"><table class="mcnTextBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnTextBlockOuter">
         <tr>
             <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
             <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
             <tr>
             <![endif]-->

             <!--[if mso]>
             <td valign="top" width="600" style="width:600px;">
             <![endif]-->
                 <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                     <tbody><tr>

                         <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">

                             <strong>Phew!</strong><br>
That\'s a wrap on The Women\'s Lifestyle Expo for this year. What an awesome weekend! I got to meet tonnes of fabulous people, many of whom already use and love my products. Win!<br>
The next market/expo event I\'ll be at will be the Craft Market in Feilding in June.<br>
&nbsp;
                         </td>
                     </tr>
                 </tbody></table>
             <!--[if mso]>
             </td>
             <![endif]-->

             <!--[if mso]>
             </tr>
             </table>
             <![endif]-->
             </td>
         </tr>
     </tbody>
</table></td>
                      </tr>
                      <tr>
                         <td valign="top" id="templateColumns">
                            <!--[if gte mso 9]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                            <tr>
                            <td align="center" valign="top" width="300" style="width:300px;">
                            <![endif]-->
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                               <tr>
                                  <td valign="top" class="columnContainer"><table class="mcnImageBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnImageBlockOuter">
             <tr>
                 <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                     <table class="mcnImageContentContainer" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                         <tbody><tr>
                             <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">


                                         <img alt="" src="https://gallery.mailchimp.com/7a555c31cf20566e9ab7c0dd2/images/8e8f572e-0887-4286-8111-1902ac9a4f96.png" style="max-width:800px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="264" align="middle">


                             </td>
                         </tr>
                     </tbody></table>
                 </td>
             </tr>
     </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnTextBlockOuter">
         <tr>
             <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
             <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
             <tr>
             <![endif]-->

             <!--[if mso]>
             <td valign="top" width="300" style="width:300px;">
             <![endif]-->
                 <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                     <tbody><tr>

                         <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">

                             <span style="font-size:18px"><strong>Top products!</strong><br>
March and April 2017 have seen the biggest sales ever for Totally Balmy, so thank you!<br>
The top sellers of the past 2 months have been the <a href="http://totallybalmy.co.nz/product/natural-deodorant-paste/" target="_blank">Deodorant Paste</a>, <a href="http://totallybalmy.co.nz/product/acne-face-soap/" target="_blank">Acne Face Soap</a>, and <a href="http://totallybalmy.co.nz/product/natural-dog-shampoo-bar/" target="_blank">Dog Shampoo</a>.<br>
I\'ll let you in on a little secret....the dog shampoo has worked wonders on my hair....ssshhhh....<br>
(The dog shampoo sold out at the expo, well almost. I have 1 bar left! The next batch will be ready in a couple of weeks).</span>
                         </td>
                     </tr>
                 </tbody></table>
             <!--[if mso]>
             </td>
             <![endif]-->

             <!--[if mso]>
             </tr>
             </table>
             <![endif]-->
             </td>
         </tr>
     </tbody>
</table></td>
                               </tr>
                            </table>
                            <!--[if gte mso 9]>
                            </td>
                            <td align="center" valign="top" width="300" style="width:300px;">
                            <![endif]-->
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                               <tr>
                                  <td valign="top" class="columnContainer"><table class="mcnBoxedTextBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <!--[if gte mso 9]>
    <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
    <![endif]-->
    <tbody class="mcnBoxedTextBlockOuter">
         <tr>
             <td class="mcnBoxedTextBlockInner" valign="top">

             <!--[if gte mso 9]>
             <td align="center" valign="top" ">
             <![endif]-->
                 <table style="min-width:100%;" class="mcnBoxedTextContentContainer" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                     <tbody><tr>

                         <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                             <table class="mcnTextContentContainer" style="min-width: 100% ! important;background-color: #404040;" border="0" width="100%" cellspacing="0" cellpadding="18">
                                 <tbody><tr>
                                     <td class="mcnTextContent" style="color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;" valign="top">
                                         <span style="font-size:20px"><strong>Winner!</strong></span><br>
Did you enter my competition at The Women\'s Lifestyle Expo in Palmerston North last weekend?<br>
Well, if you did, and your name is <span style="font-size:18px"><strong>Stacey McKenzie-Hofmann</strong></span>, then you\'re in luck! You can tell me what you\'d love to try from my range, and I\'ll get a parcel out to you asap.
                                     </td>
                                 </tr>
                             </tbody></table>
                         </td>
                     </tr>
                 </tbody></table>
             <!--[if gte mso 9]>
             </td>
             <![endif]-->

             <!--[if gte mso 9]>
                 </tr>
                 </table>
             <![endif]-->
             </td>
         </tr>
     </tbody>
</table><table class="mcnImageBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnImageBlockOuter">
             <tr>
                 <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                     <table class="mcnImageContentContainer" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                         <tbody><tr>
                             <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">


                                         <img alt="" src="https://gallery.mailchimp.com/7a555c31cf20566e9ab7c0dd2/images/f00387af-3a50-4f1e-ad08-a65cdc339ff4.jpg" style="max-width:1932px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="264" align="middle">


                             </td>
                         </tr>
                     </tbody></table>
                 </td>
             </tr>
     </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnTextBlockOuter">
         <tr>
             <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
             <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
             <tr>
             <![endif]-->

             <!--[if mso]>
             <td valign="top" width="300" style="width:300px;">
             <![endif]-->
                 <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
                     <tbody><tr>

                         <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">

                             Just a quick newsletter this time. Have you got any questions or suggestions? Email me, info@totallybalmy.co.nz, or find me on Facebook &lt;3
                         </td>
                     </tr>
                 </tbody></table>
             <!--[if mso]>
             </td>
             <![endif]-->

             <!--[if mso]>
             </tr>
             </table>
             <![endif]-->
             </td>
         </tr>
     </tbody>
</table></td>
                               </tr>
                            </table>
                            <!--[if gte mso 9]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                         </td>
                      </tr>
                      <tr>
                         <td valign="top" id="templateFooter"><table class="mcnFollowBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnFollowBlockOuter">
         <tr>
             <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                 <table class="mcnFollowContentContainer" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody><tr>
         <td style="padding-left:9px;padding-right:9px;" align="center">
             <table style="min-width:100%;" class="mcnFollowContent" border="0" width="100%" cellspacing="0" cellpadding="0">
                 <tbody><tr>
                     <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                         <table border="0" cellspacing="0" cellpadding="0" align="center">
                             <tbody><tr>
                                 <td valign="top" align="center">
                                     <!--[if mso]>
                                     <table align="center" border="0" cellspacing="0" cellpadding="0">
                                     <tr>
                                     <![endif]-->

                                         <!--[if mso]>
                                         <td align="center" valign="top">
                                         <![endif]-->


                                             <table style="display:inline;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                 <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                         <table class="mcnFollowContentItem" border="0" width="100%" cellspacing="0" cellpadding="0">
                                                             <tbody><tr>
                                                                 <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                     <table border="0" width="" cellspacing="0" cellpadding="0" align="left">
                                                                         <tbody><tr>

                                                                                 <td class="mcnFollowIconContent" valign="middle" width="24" align="center">
                                                                                     <a href="http://www.twitter.com/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display:block;" class="" height="24" width="24"></a>
                                                                                 </td>


                                                                         </tr>
                                                                     </tbody></table>
                                                                 </td>
                                                             </tr>
                                                         </tbody></table>
                                                    </td>
                                                 </tr>
                                             </tbody></table>

                                         <!--[if mso]>
                                         </td>
                                         <![endif]-->

                                         <!--[if mso]>
                                         <td align="center" valign="top">
                                         <![endif]-->


                                             <table style="display:inline;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                 <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                         <table class="mcnFollowContentItem" border="0" width="100%" cellspacing="0" cellpadding="0">
                                                             <tbody><tr>
                                                                 <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                     <table border="0" width="" cellspacing="0" cellpadding="0" align="left">
                                                                         <tbody><tr>

                                                                                 <td class="mcnFollowIconContent" valign="middle" width="24" align="center">
                                                                                     <a href="http://www.facebook.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" height="24" width="24"></a>
                                                                                 </td>


                                                                         </tr>
                                                                     </tbody></table>
                                                                 </td>
                                                             </tr>
                                                         </tbody></table>
                                                    </td>
                                                 </tr>
                                             </tbody></table>

                                         <!--[if mso]>
                                         </td>
                                         <![endif]-->

                                         <!--[if mso]>
                                         <td align="center" valign="top">
                                         <![endif]-->


                                             <table style="display:inline;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                 <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                         <table class="mcnFollowContentItem" border="0" width="100%" cellspacing="0" cellpadding="0">
                                                             <tbody><tr>
                                                                 <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                     <table border="0" width="" cellspacing="0" cellpadding="0" align="left">
                                                                         <tbody><tr>

                                                                                 <td class="mcnFollowIconContent" valign="middle" width="24" align="center">
                                                                                     <a href="http://mailchimp.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" height="24" width="24"></a>
                                                                                 </td>


                                                                         </tr>
                                                                     </tbody></table>
                                                                 </td>
                                                             </tr>
                                                         </tbody></table>
                                                    </td>
                                                 </tr>
                                             </tbody></table>

                                         <!--[if mso]>
                                         </td>
                                         <![endif]-->

                                     <!--[if mso]>
                                     </tr>
                                     </table>
                                     <![endif]-->
                                 </td>
                             </tr>
                         </tbody></table>
                     </td>
                 </tr>
             </tbody></table>
         </td>
     </tr>
</tbody></table>

             </td>
         </tr>
     </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnDividerBlockOuter">
         <tr>
             <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                 <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EEEEEE;" border="0" width="100%" cellspacing="0" cellpadding="0">
                     <tbody><tr>
                         <td>
                             <span></span>
                         </td>
                     </tr>
                 </tbody></table>
<!--
                 <td class="mcnDividerBlockInner" style="padding: 18px;">
                 <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
             </td>
         </tr>
     </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
     <tbody class="mcnTextBlockOuter">
         <tr>
             <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
                <!--[if mso]>
             <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
             <tr>
             <![endif]-->

             <!--[if mso]>
             <td valign="top" width="600" style="width:600px;">
             <![endif]-->



                 <!-- BEGIN FOOTER -->
<table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
<tbody class="mcnTextBlockOuter">
<tr>
    <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
     <!--[if mso]>
<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
<tr>
<![endif]-->

     <!--[if mso]>
<td valign="top" width="599" style="width:599px;">
<![endif]-->
     <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:100%; min-width:100%;" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>

          <td class="mcnTextContent" valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

             <em>Copyright © 2016 Totally Balmy New Zealand, All rights reserved.</em>
             <br> You\'ve received this email because you opted in during a Dango promotion, a competition, or on my website:
             <a style="color:#656565;font-weight:normal;text-decoration:underline" href="http://totallybalmy.us13.list-manage.com/track/click?u=7a555c31cf20566e9ab7c0dd2&amp;id=3f2c8efbfd&amp;e=17138fb4ea"
             target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://totallybalmy.us13.list-manage.com/track/click?u%3D7a555c31cf20566e9ab7c0dd2%26id%3D3f2c8efbfd%26e%3D17138fb4ea&amp;source=gmail&amp;ust=1479780733183000&amp;usg=AFQjCNHJ0NI9xOKqjj2kWLXGMOYiGjUD9A"><font color="#1155cc">www.totallybalmy.co.nz</font></a>
             <br>
             <br>
             <strong>Our mailing address is:</strong>
             <br> 4 Mahi Grove, Feilding, 4702
             <br>
             <br> Want to change how you receive these emails?
             <br> You can <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_totallyBalmy">unsubcribe here</a>
             <br> &nbsp;
          </td>
          </tr>
        </tbody>
     </table>
     <!--[if mso]>
</td>
<![endif]-->

     <!--[if mso]>
</tr>
</table>
<![endif]-->
    </td>
</tr>
</tbody>
</table>
<!-- /END FOOTER -->


             <!--[if mso]>
             </td>
             <![endif]-->

             <!--[if mso]>
             </tr>
             </table>
             <![endif]-->
             </td>
         </tr>
     </tbody>
</table></td>
                      </tr>
                   </table>
                   <!--[if gte mso 9]>
                   </td>
                   </tr>
                   </table>
                   <![endif]-->
                   <!-- // END TEMPLATE -->
                     </td>
                 </tr>
             </table>
         </center>
     </body>
</html>

      ';

echo $sendGridApiHtml;




 ?>

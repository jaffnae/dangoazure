<?php

/**************************************************
       ! ! ! ! ! WARNING ! ! ! ! ! !
***************************************************/
//Make sure the code for Unsubscribe is on the bottom:
/*

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>


*/


//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize
//http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[DB_NAME]&newsletterTemplateName=clients/GiftHutt1&customSubject=true


//1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
//2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
    //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
    //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
//3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
//4. Use the following link to send emails:
   //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
   //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_giftHutt&newsletterTemplateName=clients/GiftHutt5&customSubject=true


    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Gift Hutt";
    $fromNameAndEmailObject["email"] = "info@gifthutt.co.nz";


    $sendGridApiHtml = '
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

  <head>
    <!-- NAME: REST & RELAXATION -->
    <!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>*|MC:SUBJECT|*</title>

    <style type="text/css">
      p {
        margin: 10px 0;
        padding: 0;
      }

      table {
        border-collapse: collapse;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        display: block;
        margin: 0;
        padding: 0;
      }

      img,
      a img {
        border: 0;
        height: auto;
        outline: none;
        text-decoration: none;
      }

      body,
      #bodyTable,
      #bodyCell {
        height: 100%;
        margin: 0;
        padding: 0;
        width: 100%;
      }

      #outlook a {
        padding: 0;
      }

      img {
        -ms-interpolation-mode: bicubic;
      }

      table {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }

      .ReadMsgBody {
        width: 100%;
      }

      .ExternalClass {
        width: 100%;
      }

      p,
      a,
      li,
      td,
      blockquote {
        mso-line-height-rule: exactly;
      }

      a[href^=tel],
      a[href^=sms] {
        color: inherit;
        cursor: default;
        text-decoration: none;
      }

      p,
      a,
      li,
      td,
      body,
      table,
      blockquote {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }

      .ExternalClass,
      .ExternalClass p,
      .ExternalClass td,
      .ExternalClass div,
      .ExternalClass span,
      .ExternalClass font {
        line-height: 100%;
      }

      a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
      }

      a.mcnButton {
        display: block;
      }

      .mcnImage {
        vertical-align: bottom;
      }

      .mcnTextContent {
        word-break: break-word;
      }

      .mcnTextContent img {
        height: auto !important;
      }

      .mcnDividerBlock {
        table-layout: fixed !important;
      }
      /*
	@tab Page
	@section background style
	@tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
	*/

      body,
      #bodyTable {
        /*@editable*/
        background-color: #cac8ca;
      }
      /*
	@tab Page
	@section background style
	@tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
	*/

      #bodyCell {
        /*@editable*/
        border-top: 0;
      }
      /*
	@tab Page
	@section email border
	@tip Set the border for your email.
	*/

      #templateContainer {
        /*@editable*/
        border: 0;
      }
      /*
	@tab Page
	@section heading 1
	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
	@style heading 1
	*/

      h1 {
        /*@editable*/
        color: #FE6F00 !important;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 40px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: -1px;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Page
	@section heading 2
	@tip Set the styling for all second-level headings in your emails.
	@style heading 2
	*/

      h2 {
        /*@editable*/
        color: #202020 !important;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 26px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Page
	@section heading 3
	@tip Set the styling for all third-level headings in your emails.
	@style heading 3
	*/

      h3 {
        /*@editable*/
        color: #404040 !important;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 18px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Page
	@section heading 4
	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	@style heading 4
	*/

      h4 {
        /*@editable*/
        color: #64931F !important;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Preheader
	@section preheader style
	@tip Set the background color and borders for your email\'s preheader area.
	*/

      #templatePreheader {
        /*@editable*/
        background-color: #9e7da2;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
      }
      /*
	@tab Preheader
	@section preheader text
	@tip Set the styling for your email\'s preheader text. Choose a size and color that is easy to read.
	*/

      .preheaderContainer .mcnTextContent,
      .preheaderContainer .mcnTextContent p {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 11px;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Preheader
	@section preheader link
	@tip Set the styling for your email\'s header links. Choose a color that helps them stand out from your text.
	*/

      .preheaderContainer .mcnTextContent a {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Header
	@section header style
	@tip Set the background color and borders for your email\'s header area.
	*/

      #templateHeader {
        /*@editable*/
        background-color: #ffffff;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
      }
      /*
	@tab Header
	@section header text
	@tip Set the styling for your email\'s header text. Choose a size and color that is easy to read.
	*/

      .headerContainer .mcnTextContent,
      .headerContainer .mcnTextContent p {
        /*@editable*/
        color: #404040;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Header
	@section header link
	@tip Set the styling for your email\'s header links. Choose a color that helps them stand out from your text.
	*/

      .headerContainer .mcnTextContent a {
        /*@editable*/
        color: #FE6F00;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Body
	@section upper body style
	@tip Set the background color and borders for your email\'s upper body area.
	*/

      #templateUpperBody {
        /*@editable*/
        background-color: #FFFFFF;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
      }
      /*
	@tab Body
	@section upper body text
	@tip Set the styling for your email\'s upper body text. Choose a size and color that is easy to read.
	*/

      .upperBodyContainer .mcnTextContent,
      .upperBodyContainer .mcnTextContent p {
        /*@editable*/
        color: #404040;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Body
	@section upper body link
	@tip Set the styling for your email\'s upper body links. Choose a color that helps them stand out from your text.
	*/

      .upperBodyContainer .mcnTextContent a {
        /*@editable*/
        color: #FE6F00;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Columns
	@section column style
	@tip Set the background color and borders for your email\'s columns area.
	*/

      #templateColumns {
        /*@editable*/
        background-color: #FFFFFF;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
      }
      /*
	@tab Columns
	@section left column text
	@tip Set the styling for your email\'s left column text. Choose a size and color that is easy to read.
	*/

      .leftColumnContainer .mcnTextContent,
      .leftColumnContainer .mcnTextContent p {
        /*@editable*/
        color: #404040;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 12px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Columns
	@section left column link
	@tip Set the styling for your email\'s left column links. Choose a color that helps them stand out from your text.
	*/

      .leftColumnContainer .mcnTextContent a {
        /*@editable*/
        color: #FE6F00;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Columns
	@section right column text
	@tip Set the styling for your email\'s right column text. Choose a size and color that is easy to read.
	*/

      .rightColumnContainer .mcnTextContent,
      .rightColumnContainer .mcnTextContent p {
        /*@editable*/
        color: #404040;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 14px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Columns
	@section right column link
	@tip Set the styling for your email\'s right column links. Choose a color that helps them stand out from your text.
	*/

      .rightColumnContainer .mcnTextContent a {
        /*@editable*/
        color: #FE6F00;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Body
	@section lower body style
	@tip Set the background color and borders for your email\'s lower body area.
	*/

      #templateLowerBody {
        /*@editable*/
        background-color: #FFFFFF;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
      }
      /*
	@tab Body
	@section lower body text
	@tip Set the styling for your email\'s lower body text. Choose a size and color that is easy to read.
	*/

      .lowerBodyContainer .mcnTextContent,
      .lowerBodyContainer .mcnTextContent p {
        /*@editable*/
        color: #404040;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 14px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: center;
      }
      /*
	@tab Body
	@section lower body link
	@tip Set the styling for your email\'s lower body links. Choose a color that helps them stand out from your text.
	*/

      .lowerBodyContainer .mcnTextContent a {
        /*@editable*/
        color: #FE6F00;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Footer
	@section footer style
	@tip Set the background color and borders for your email\'s footer area.
	*/

      #templateFooter {
        /*@editable*/
        background-color: #9e7da2;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
      }
      /*
	@tab Footer
	@section footer text
	@tip Set the styling for your email\'s footer text. Choose a size and color that is easy to read.
	*/

      .footerContainer .mcnTextContent,
      .footerContainer .mcnTextContent p {
        /*@editable*/
        color: #f0f0f0;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 11px;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Footer
	@section footer link
	@tip Set the styling for your email\'s footer links. Choose a color that helps them stand out from your text.
	*/

      .footerContainer .mcnTextContent a {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }

      @media only screen and (max-width: 480px) {
        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
          -webkit-text-size-adjust: none !important;
        }
      }

      @media only screen and (max-width: 480px) {
        body {
          width: 100% !important;
          min-width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        #bodyCell {
          padding-top: 10px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        #templateContainer,
        #templatePreheader,
        #templateHeader,
        #templateColumns,
        #templateUpperBody,
        #templateLowerBody,
        #templateFooter {
          max-width: 600px !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .columnsContainer {
          display: block!important;
          max-width: 600px !important;
          width: 100%!important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImage {
          height: auto !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnCartContainer,
        .mcnCaptionTopContent,
        .mcnRecContentContainer,
        .mcnCaptionBottomContent,
        .mcnTextContentContainer,
        .mcnBoxedTextContentContainer,
        .mcnImageGroupContentContainer,
        .mcnCaptionLeftTextContentContainer,
        .mcnCaptionRightTextContentContainer,
        .mcnCaptionLeftImageContentContainer,
        .mcnCaptionRightImageContentContainer,
        .mcnImageCardLeftTextContentContainer,
        .mcnImageCardRightTextContentContainer {
          max-width: 100% !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnBoxedTextContentContainer {
          min-width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageGroupContent {
          padding: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnCaptionLeftContentOuter .mcnTextContent,
        .mcnCaptionRightContentOuter .mcnTextContent {
          padding-top: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageCardTopImageContent,
        .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
          padding-top: 18px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageCardBottomImageContent {
          padding-bottom: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageGroupBlockInner {
          padding-top: 0 !important;
          padding-bottom: 0 !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageGroupBlockOuter {
          padding-top: 9px !important;
          padding-bottom: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnTextContent,
        .mcnBoxedTextContentColumn {
          padding-right: 18px !important;
          padding-left: 18px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageCardLeftImageContent,
        .mcnImageCardRightImageContent {
          padding-right: 18px !important;
          padding-bottom: 0 !important;
          padding-left: 18px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcpreview-image-uploader {
          display: none !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
        h1 {
          /*@editable*/
          font-size: 24px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
        h2 {
          /*@editable*/
          font-size: 20px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
        h3 {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
        h4 {
          /*@editable*/
          font-size: 16px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        .mcnBoxedTextContentContainer .mcnTextContent,
        .mcnBoxedTextContentContainer .mcnTextContent p {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Preheader Visibility
	@tip Set the visibility of the email\'s preheader on small screens. You can hide it to save space.
	*/
        #templatePreheader {
          /*@editable*/
          display: block !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Preheader Text
	@tip Make the preheader text larger in size for better readability on small screens.
	*/
        .preheaderContainer .mcnTextContent,
        .preheaderContainer .mcnTextContent p {
          /*@editable*/
          font-size: 14px !important;
          /*@editable*/
          line-height: 115% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
        .headerContainer .mcnTextContent,
        .headerContainer .mcnTextContent p {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Upper Body Text
	@tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        .upperBodyContainer .mcnTextContent,
        .upperBodyContainer .mcnTextContent p {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Lower Body Text
	@tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        .lowerBodyContainer .mcnTextContent,
        .lowerBodyContainer .mcnTextContent p {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Left Column Text
	@tip Make the left column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        .leftColumnContainer .mcnTextContent,
        .leftColumnContainer .mcnTextContent p {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Right Column Text
	@tip Make the right column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        .rightColumnContainer .mcnTextContent,
        .rightColumnContainer .mcnTextContent p {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section footer text
	@tip Make the body content text larger in size for better readability on small screens.
	*/
        .footerContainer .mcnTextContent,
        .footerContainer .mcnTextContent p {
          /*@editable*/
          font-size: 14px !important;
          /*@editable*/
          line-height: 115% !important;
        }
      }

    </style>
  </head>

  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    <center>
      <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tbody>
          <tr>
            <td align="center" valign="top" id="bodyCell">
              <!-- BEGIN TEMPLATE // -->
              <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                <tbody>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN PREHEADER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                        <tbody>
                          <tr>
                            <td valign="top" class="preheaderContainer" style="padding-top:9px; padding-bottom:9px;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                <tbody class="mcnTextBlockOuter">
                                  <tr>
                                    <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                      <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                                      <!--[if mso]>
				<td valign="top" width="390" style="width:390px;">
				<![endif]-->
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:390px;" width="100%" class="mcnTextContentContainer">
                                        <tbody>
                                          <tr>

                                            <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                              The latest news from Gift Hutt
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <!--[if mso]>
				</td>
				<![endif]-->

                                      <!--[if mso]>
				<td valign="top" width="210" style="width:210px;">
				<![endif]-->
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:210px;" width="100%" class="mcnTextContentContainer">
                                        <tbody>
                                          <tr>

                                            <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                              <a href="*|ARCHIVE|*" target="_blank">View this email in your browser</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <!--[if mso]>
				</td>
				<![endif]-->

                                      <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- // END PREHEADER -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN HEADER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                        <tbody>
                          <tr>
                            <td valign="top" class="headerContainer">
                              <table class="mcnImageBlock" style="min-width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody class="mcnImageBlockOuter">
                                  <tr>
                                    <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                                      <table class="mcnImageContentContainer" style="min-width:100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">

                                              <a href="http://gifthutt.nz/" title="" class="" target="_blank">
                                                <img alt="" src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/a086a34d-be33-47f8-a46f-459114e254b9.gif" style="max-width:359px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" align="middle" width="359">
                                              </a>

                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table class="mcnBoxedTextBlock" style="min-width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                <tbody class="mcnBoxedTextBlockOuter">
                                  <tr>
                                    <td class="mcnBoxedTextBlockInner" valign="top">

                                      <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                      <table style="min-width:100%;" class="mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>

                                            <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                              <table class="mcnTextContentContainer" style="min-width: 100% ! important;background-color: #565555;" border="0" cellpadding="18" cellspacing="0" width="100%">
                                                <tbody>
                                                  <tr>
                                                    <td style="color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;" class="mcnTextContent" valign="top">
                                                      <span style="font-size:15px">Gift Hutt is the art and craft hub in the Hutt.&nbsp;<br>
Our great store is in the heart of Lower Hutt at 136 High Street.<br>
We have a wide range of&nbsp;genuine and unique gifts, and&nbsp; craft classes&nbsp;and craft supplies for your creative needs.</span>
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                      <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageGroupBlock">
                                <tbody class="mcnImageGroupBlockOuter">

                                  <tr>
                                    <td valign="top" style="padding:9px" class="mcnImageGroupBlockInner">

                                      <table align="left" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="mcnImageGroupContentContainer">
                                        <tbody>
                                          <tr>
                                            <td class="mcnImageGroupContent" valign="top" style="padding-left: 9px; padding-top: 0; padding-bottom: 0;">

                                              <a href="http://gifthutt.nz/shop/" title="" class="" target="_blank">
                                                <img alt="" src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/f6290a5a-7254-42e0-af76-ae09c7ba3fbc.gif" width="263" style="max-width:323px; padding-bottom: 0;" class="mcnImage">
                                              </a>

                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>

                                      <table align="right" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="mcnImageGroupContentContainer">
                                        <tbody>
                                          <tr>
                                            <td class="mcnImageGroupContent" valign="top" style="padding-right: 9px; padding-top: 0; padding-bottom: 0;">

                                              <a href="http://gifthutt.nz/about-us/" title="" class="" target="_blank">
                                                <img alt="" src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/9bbbc740-e9f9-4985-81a1-60867c3ee160.gif" width="263" style="max-width:323px; padding-bottom: 0;" class="mcnImage">
                                              </a>

                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>

                                    </td>
                                  </tr>

                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- // END HEADER -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN UPPER BODY // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateUpperBody">
                        <tbody>
                          <tr>
                            <td valign="top" class="upperBodyContainer">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                <tbody class="mcnTextBlockOuter">
                                  <tr>
                                    <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                      <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                                      <!--[if mso]>
				<td valign="top" width="599" style="width:599px;">
				<![endif]-->
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                        <tbody>
                                          <tr>

                                            <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; text-align: center;">

                                              <h2 class="null" style="text-align: center;">Gift Hutt craft workshops and News</h2>

                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <!--[if mso]>
				</td>
				<![endif]-->

                                      <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                                <tbody class="mcnTextBlockOuter">
                                  <tr>
                                    <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                      <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                                      <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                        <tbody>
                                          <tr>

                                            <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                              Welcome to all the new subscribers to this newsletter. &nbsp;Many of you would have signed up as part of a giveaway that I have been running and I would love for you to continue to receive the newsletters keeping you up to date with the news at Gift Hutt.
                                              &nbsp;But if you do not wish to continue to receive the newsletters there is an unsubscribe button at the bottom of this newsletter.
                                              <br>
                                              <br>
                                              <em><strong>What\'s New in Gift Hutt</strong></em>. &nbsp;
                                              <br>
                                              <br> We now have some stunning floral arrangements using flowers in shoes. &nbsp;These are really cool and great for gifts for the shoe aficionado. &nbsp;Coming soon in time for Christmas are some gift baskets
                                              featuring a wide range of goodies.
                                              <br>
                                              <br> For the paper crafter I now have the Marsupial layering tool. &nbsp;It is so easy to use and will give you are professional look to your papercrafting.&nbsp;
                                              <br> I now have the gorgeous new Classic Rose collection from Heartfelt Creations in store. &nbsp;This is my new favourite flower set and I know that I will be using it a lot on my cards and other projects. &nbsp;Check
                                              out these beautiful cards that I have made <a href="http://gifthutt.nz/product/heartfelt-creations-classic-rose-collection-cards/" target="_blank">here</a>. &nbsp;The Classic Rose stamp and die set sold out
                                              very quickly and I have now restocked but you will need to be quick to get them.
                                              <br>
                                              <br> Upcycling furniture is very popular now and we have just the paints for you from Voodoo Molly Vintage. &nbsp;These paints have been developed in New Zealand specifically for upcycling furniture and are so
                                              easy to use. &nbsp;No more stripping varnish and paint back to the wood! &nbsp;For some inspiration check out this Stuff article <a href="http://www.stuff.co.nz/life-style/home-property/85347191/looking-for-almostinstant-gratification-try-these-weekend-painting-projects"
                                              target="_blank">here.</a>&nbsp; We run classes here every month to help you get started with these paints and get you a professional finish every time. &nbsp;
                                              <br>
                                              <br>
                                              <em><strong>Wainuiomata Pop Up Shop</strong></em>
                                              <br>
                                              <br> I have had the opportunity to open up a pop up shop in Wainuiomata from 1st November to 24 December and I am looking for people who would like to sell their products in the shop for the Christmas shopping
                                              period. &nbsp;If you are interested please contact me at info@gifthutt.nz.
                                              <br>
                                              <br>
                                              <em><strong>Workshops and Classes:</strong></em>
                                              <br>
                                              <br> We have some exciting workshops coming up in Gift Hutt. &nbsp;The flower making class was very popular and I will be doing another class.
                                              <br> Flo Hill-Rennie will be coming down from Tauranga to take a class on the Cricut Explore on the 29th October. &nbsp;Flo is the Cricut \'Guru\' of New Zealand and it is a great opportunity to learn how to get
                                              the most out of your Cricut Explore. &nbsp;I am looking forward to seeing what she shows us.
                                              <br>
                                              <br>
                                              <strong>Bring Your Own Project</strong>
                                              <br> Saturday 22 October 1.30 pm to 4 pm
                                              <br> Cost $25.
                                              <br>
                                              <br>
                                              <strong>Getting to Know Your Cricut Explore</strong>
                                              <br> Saturday 29th October 9.30 am to 4 pm
                                              <br> Cost $60
                                              <br>
                                              <br>
                                              <strong>Cardmaking Class</strong>
                                              <br> Sunday 23rd October 1.30 - 4.30 pm
                                              <br> Cost $5 per card
                                              <br>
                                              <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                              <br>
                                              <br> Over the next few months I will be gradually adding a wider range of art and craft supplies for the artists and crafters to be able to purchase. &nbsp;As a paper crafter myself, I know how frustrating it
                                              is not being able to purchase products easily in New Zealand and I aim to fill some of those gaps. &nbsp;If there is any particular product that you would like to be able to purchase through Gift Hutt please
                                              feel free to contact me and I can see if I can source this for you. &nbsp;I am happy to look for any crafts not just papercrafting.
                                              <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                              <br>
                                              <br> Have a great month and Happy Crafting
                                              <br> Patricia and Beth
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <!--[if mso]>
				</td>
				<![endif]-->

                                      <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- // END UPPER BODY -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN COLUMNS // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateColumns">
                        <tbody>
                          <tr>
                            <td align="left" valign="top" class="columnsContainer" width="50%">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                                <tbody>
                                  <tr>
                                    <td valign="top" class="leftColumnContainer"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                            <td align="left" valign="top" class="columnsContainer" width="50%">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                                <tbody>
                                  <tr>
                                    <td valign="top" class="rightColumnContainer"></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- // END COLUMNS -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN LOWER BODY // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateLowerBody">
                        <tbody>
                          <tr>
                            <td valign="top" class="lowerBodyContainer">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                                <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                                <tbody class="mcnBoxedTextBlockOuter">
                                  <tr>
                                    <td valign="top" class="mcnBoxedTextBlockInner">

                                      <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                        <tbody>
                                          <tr>

                                            <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                              <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #662A58;">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top" class="mcnTextContent" style="color: #FFFFFF;font-family: Helvetica;font-size: 16px;font-weight: bold;line-height: 100%;text-align: left;">
                                                      Upcoming workshops at Gift Hutt, 136 High Street, Lower Hutt
                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <!--[if gte mso 9]>
				</td>
				<![endif]-->

                                      <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                <tbody class="mcnCaptionBlockOuter">
                                  <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                        <tbody>
                                          <tr>
                                            <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                <tbody>
                                                  <tr>
                                                    <td class="mcnCaptionRightImageContent" valign="top">



                                                      <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/36a77e93-8267-4308-8c68-47e8c43671f0.jpg" width="264" style="max-width:788px;" class="mcnImage">



                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top" class="mcnTextContent">
                                                      <h3 class="null">Introduction to Voodoo Molly Vintage</h3>

                                                      <div style="text-align: left;">
                                                        <article id="post-12420">
                                                        </article>
                                                        <p>Learn about surface preparation, creating a smooth paint finish, de-stressing,&nbsp;finishing and all about the Voodoo Molly Products.</p>

                                                        <br>
                                                        <strong>When</strong>: Saturday 8 October
                                                        <br>
                                                        <strong>Time</strong>: 10 am to 4 pm
                                                        <br> Bring: &nbsp;A piece of furniture or two
                                                        <br>
                                                        <strong>Cost</strong>: $85 - includes a pottle of paint and use of other Voodoo Molly Vintage products</div>

                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>




                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                                <tbody class="mcnButtonBlockOuter">
                                  <tr>
                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                              <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/furniture-painting-workshop-voodoo-molly-paints-sunday-29th-may/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                <tbody class="mcnCaptionBlockOuter">
                                  <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                        <tbody>
                                          <tr>
                                            <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                <tbody>
                                                  <tr>
                                                    <td class="mcnCaptionRightImageContent" valign="top">



                                                      <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/9e896bec-0deb-4232-8de2-093083ffc366.jpg" width="264" style="max-width:500px;" class="mcnImage">



                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top" class="mcnTextContent">
                                                      <h3 class="null">Flower Making Workshop for Cardmaking</h3>

                                                      <div style="text-align: left;">Learn how to make flowers using the fabulous Heartfelt Creations stamps and coordinating dies. &nbsp;
                                                        <br>
                                                        <br>
                                                        <strong>When</strong>:Sunday 9 October
                                                        <br>
                                                        <strong>Time</strong>: 1.30 pm to 4.30 pm
                                                        <br>
                                                        <strong>Bring</strong>: Your favourite shaping tools
                                                        <br>
                                                        <strong>Cost</strong>: $20</div>

                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>




                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                                <tbody class="mcnButtonBlockOuter">
                                  <tr>
                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                              <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/heartfelt-creations-flower-making-workshop/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                <tbody class="mcnCaptionBlockOuter">
                                  <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                        <tbody>
                                          <tr>
                                            <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                <tbody>
                                                  <tr>
                                                    <td class="mcnCaptionRightImageContent" valign="top">



                                                      <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/b77e0ab6-afb7-4e12-8ac2-9fb55e33d397.jpg" width="264" style="max-width:500px;" class="mcnImage">



                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top" class="mcnTextContent">
                                                      <h3 class="null">Bring Your Own Project</h3>

                                                      <div style="text-align: left;">Use our space and get advice on your painting and upholstery projects.
                                                        <br>
                                                        <br>
                                                        <strong>When</strong>:Saturday 22 October&nbsp;
                                                        <br>
                                                        <strong>Time</strong>: 1.30 pm to 3.30 pm
                                                        <br>
                                                        <strong>Bring</strong>: Your project to work on.
                                                        <br>
                                                        <strong>Cost</strong>: $25</div>

                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>




                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                                <tbody class="mcnButtonBlockOuter">
                                  <tr>
                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                              <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/bring-project-saturday-24-september-2016/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                <tbody class="mcnCaptionBlockOuter">
                                  <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                        <tbody>
                                          <tr>
                                            <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                <tbody>
                                                  <tr>
                                                    <td class="mcnCaptionRightImageContent" valign="top">



                                                      <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/fabf214c-fe74-4fcb-8779-b0c40b7b5260.jpg" width="264" style="max-width:375px;" class="mcnImage">



                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top" class="mcnTextContent">
                                                      <h3 class="null">Christmas Cards Class</h3>

                                                      <div style="text-align: left;">Get a head start with your Christmas cards using a variety of techniques and finishes to impress your family and friends.
                                                        <br>
                                                        <br>
                                                        <strong>When</strong>:Sunday 23 October&nbsp;
                                                        <br>
                                                        <strong>Time</strong>: 1.30 pm to 4 30 pm
                                                        <br>
                                                        <strong>Bring</strong>: Flowers that you have made - there will be extras to purchase. &nbsp;Your favourite tools and adhesives.
                                                        <br>
                                                        <strong>Cost</strong>: $5 per card.</div>

                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>




                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                                <tbody class="mcnButtonBlockOuter">
                                  <tr>
                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                              <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/cardmaking-class-using-flowers-25-september-1-30-to-4-30-copy/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                                <tbody class="mcnCaptionBlockOuter">
                                  <tr>
                                    <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                        <tbody>
                                          <tr>
                                            <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                              <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                                <tbody>
                                                  <tr>
                                                    <td class="mcnCaptionRightImageContent" valign="top">



                                                      <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/d7f9c388-769d-4a60-b009-73000ae4495c.jpg" width="264" style="max-width:1024px;" class="mcnImage">



                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                              <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                                <tbody>
                                                  <tr>
                                                    <td valign="top" class="mcnTextContent">
                                                      <h3 class="null">Getting to Know Your Cricut Explore</h3>

                                                      <div style="text-align: left;">Learn how to get the most out of your Cricut Explore with the Cricut \'Guru\' Flo Hill-Rennie.
                                                        <br> Use Design Space and let your imagination run wild.
                                                        <br> Numbers are limited so be in quick.
                                                        <br>
                                                        <br>
                                                        <strong>When</strong>:Saturday 29th October&nbsp;
                                                        <br>
                                                        <strong>Time</strong>: 9.30 - 4 pm
                                                        <br>
                                                        <strong>Bring</strong>: Your Cricut Explore and laptop, phone with internet connection, Cricut tools, lunch and snacks.
                                                        <br>
                                                        <strong>Cost</strong>: $60</div>

                                                    </td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>




                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                                <tbody class="mcnButtonBlockOuter">
                                  <tr>
                                    <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                      <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                        <tbody>
                                          <tr>
                                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                              <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/getting-know-cricut-explore/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- // END LOWER BODY -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN FOOTER // -->



                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
                        <tbody>
                          <tr>
                          </tr>
                          <tr>
                            <td align="center" valign="top" colspan="2">

                              <br />
                              <br />
                              <br />
                              <br />
                              <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
                                If you received this message in error please disregard and delete this.
                                <br> You can also unsubscribe from these emails by following
                                <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>


                      <!-- // END FOOTER -->
                    </td>
                  </tr>
                </tbody>
              </table>
              <!-- // END TEMPLATE -->
            </td>
          </tr>
        </tbody>
      </table>
    </center>

  </body>

</html>


';
      echo $sendGridApiHtml;




 ?>

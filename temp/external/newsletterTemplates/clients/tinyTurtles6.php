<?php


/**************************************************
       ! ! ! ! ! WARNING ! ! ! ! ! !
***************************************************/
//Make sure the code for Unsubscribe is on the bottom:
/*

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_tinyTurtles" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>
*/

    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Tiny Turtles";
    $fromNameAndEmailObject["email"] = "info@tinyturtles.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_tinyTurtles&newsletterTemplateName=clients/tinyTurtles6&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_tinyTurtles&newsletterTemplateName=clients/tinyTurtles6&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraph.org/schema/"><head>

  <meta property="og:title" content="NEW SUMMER STOCK - Pre order now&nbsp;💗💗💗">
  <meta property="fb:page_id" content="43929265776">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta charset="UTF-8">
  <title>Untitled Document</title>
  <style type="text/css">
  body {font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 140%;}
  img {display:block;}
  p, ul, ol, li { font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 140%; margin: 0; padding: 0; padding-bottom: 10px; }
  ul, ol, li { margin-left: 10px; padding-left: 10px; }
  a {
  	font-family: Helvetica, Arial, sans-serif;
  	color: #00A69C;
  	text-decoration: none;
  }
  a.block-link {
  	color: #fff;
  	background-color: #00A69C;
  	text-transform: uppercase;
  	padding: 10px 12px;
      margin: 5px 0px;
      display: inline-block;
  }
  ul.nav {
      width: 100%;
      display: block;
      font-size: 14px;
      text-transform: uppercase;
      color: #00A69C;
      border-top: 1px solid #EEEEEE;
      border-bottom: 1px solid #EEEEEE;
      margin: 35px 0 0 0;
      padding: 15px 0 0 0;
      height: 30px;
      padding-left: 0px !important;
      margin-left: 0px !important;
  }
  ul.nav li {
      font-size: 14px;
      float: left;
      list-style: none;
      margin: 0;
      padding: 0 50px 0 0;
      padding-left: 0px !important;
      margin-left: 0px !important;
  }
  ul.nav li.last {
      padding: 0;
      padding-left: 0px !important;
      margin-left: 0px !important;
  }
  p.disclaimer {
      padding: 30px 0;
  }
  .txt-container {
      padding: 20px 0px;
  }
  .txt-container p {
      font-size: 16px;
  }
  p.subhead {
      text-transform: uppercase;
      font-weight: bold;
      border-top: 2px solid black;
      font-family: Helvetica Neue, Heletica, Arial;
      margin-top: 10px;
  }
  p.intro {
      font-style: italic;
      font-size: 18px;
      line-height: 160%;
  }
  </style>
   </head> <body id="archivebody" style="padding: 0px;margin: auto;background-color: rgb(255, 255, 255);">


  <table width="645" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody><tr>
  	<td align="left" width="25%" valign="bottom">
      	<table border="0" cellpadding="0" cellspacing="0"><tbody><tr>
          	<td><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/83c21b11-c6d4-4ff3-a21c-7362acdd6728.gif" width="39" height="54" alt="Join us on Facebook" style="margin-right: 10px;" border="0"></a></td>
              <td><a href="https://twitter.com/TinyTurtles_NZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3f7d7a96-77bb-445c-9c60-e61cfd6b0ef2.gif" width="39" height="54" alt="Join us on Twitter" border="0"></a></td>
        </tr></tbody></table>
      </td>
      <td align="center" width="50%"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/f9ce15fa-bd61-4b1e-9859-a1a655737f85.gif" width="260" height="126" alt="Shop for BONDS on Tiny Turtles.co.nz" border="0"></a></td>
      <td align="right" width="25%" valign="bottom"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3e5dad31-f112-44db-ab59-7ca98ec53010.gif" width="101" height="105" alt="Sleep Sale - HURRY 1 Week only" border="0"></a></td>
  </tr>
  <tr>
      <td colspan="3">
          <ul class="nav">
              <li><a href="http://www.tinyturtles.co.nz/2016">New</a></li>
              <li><a href="http://www.tinyturtles.co.nz/bonds-baby-boys-clothing">Boys</a></li>
              <li><a href="http://www.tinyturtles.co.nz/bonds-baby-girls-clothing">Girls</a></li>
              <li><a href="http://www.tinyturtles.co.nz/Bonds-Baby-newborn-clothing">Newborns</a></li>
              <li><a href="http://www.tinyturtles.co.nz/wondersuit">Wondersuits</a></li>
              <li class="last"><a href="http://www.tinyturtles.co.nz/Bonds-Outerwear">Outerwear</a></li>
          </ul>
      </td>
  </tr>

  <tr>
      <td colspan="3"><a href="http://tinyturtles.co.nz/product-category/sale/"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/61419b2e-d069-4abc-b353-d3471df6aec5.jpg" width="645" height="505" border="0" alt="OUR SHOP IS OPEN!"></a></td>
  </tr>
  <tr>
      <td colspan="3">
      <div class="txt-container">
          <p>NEW SHOP - OPEN - 22 King Street, Pukekohe. </p>
          <p>OUR NEW STORE is OPEN, Monday-Friday 9-4.30pm.   We have tons of opening specials in store and ONLINE TOO as we know lots of our amazing customers can\'t make it in store.  Click on the image to go to our sale tab. </p>
          <p><em> -Eeeek</em></p>
      </div>
     </td></tr><tr>
      <td colspan="3"><a href="http://tinyturtles.co.nz/product-category/kidswear/"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/_compresseds/0bca7b9a-a148-4f61-828f-0ba15d8e6f92.jpg" width="645" height="355" border="0" alt="OUR SHOP IS OPEN!"></a></td>
  </tr>
  <tr>
      <td colspan="3">
      <div class="txt-container">
          <p>NEW STYLES - 3-10 YEARS. </p>
          <p>CHECK OUT OUR NEW KIDS STYLES, Milky and Hootkids are now a part of our range with more styles joining us soon. STAY TUNED AS WE HAVE NEW BONDS STYLES DROPPING VERY SOON.  </p>
          <p><em>PRE ORDERS WILL BE GOING UP SOON TOO... BUT WE PROMISE TO KEEP YOU UPDATED </em></p>
   </div></td> </tr>


  <tr>
  <td colspan="3">
  <a href="http://www.tinyturtles.co.nz">

  </a></td></tr>
  <tr>
      <td colspan="3"><a href="http://www.tinyturtles.co.nz/Bonds-Maternity"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/1f8f9c55-0b1e-4d24-9cf1-2a3a47b1722a.jpg" width="645" height="397" border="0" alt="Layby\'s, Yes we do."></a></td>
  </tr>
  <tr>
      <td colspan="3">
      <div class="txt-container">
          <p>Layby\'s are available, simply select internet banking &amp; pop a note in the comments section saying you would like a layby &amp; we\'ll get back to you. <br>
      </p>
          <p><em>2, 3 &amp; 4 week options available xxx    </em></p>
      </div>
  </td>
  </tr>

  <tr>
  <td colspan="3">
  <a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/7efbb7e1-2b39-4024-b96f-ee82c5e85784.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>
  </tr>
  <tr>
      <td colspan="3"><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/31c5fd8c-ad3a-4cad-a8f0-df39d3aad955.jpg" width="645" height="385" border="0" alt="We Love Marys Market!"></a></td>
  </tr>
  <tr>
      <td colspan="3">
      <div class="txt-container">
          <p>Massive Mary\'s Market **SALE** is Saturday the 11th of March, so if you can make it come grab yourself some great BONDS bargains! FIND US IN THE FOYER</p>
      </div>
  </td>
  </tr>

  <tr>
  <td colspan="3">
  <a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/73c7f1ba-f758-47d9-baf8-05ce6403bee5.png" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>
  </tr>




  <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
  </tbody></table>


  <table width="645" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody><tr>
      <td width="215" height="215"><a href="http://vimeo.com/102892239"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/27aa85ea-9017-4299-8a5f-9acb15d6abdd.gif" width="215" height="215" border="0" alt="Baby Search Winners Video"></a></td>
      <td width="215" height="215"><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/d361fecc-2a3f-40e3-8c61-2ffb2aedbb8f.gif" width="215" border="0" height="215" alt="Join us on Facebook"></a></td>
      <td width="215" height="215"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3d22163f-7c03-4c79-9b7b-ac98c7c73a5b.gif" width="215" height="215" border="0" alt="Free shipping on orders over $70 because we love you"></a></td>
  </tr>
  <tr>
  </tr>


  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
    <tbody>
      <tr>
      </tr>
      <tr>
        <td align="center" valign="top" colspan="2">

          <br />
          <br />
          <br />
          <br />
          <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
            If you received this message in error please disregard and delete this.
            <br> You can also unsubscribe from these emails by following
            <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_tinyTurtles" style="color: #aaa;">this link</a></p>
        </td>
      </tr>
    </tbody>
  </table>


  </tbody></table>
  </body></html>';

echo $sendGridApiHtml;
 ?>

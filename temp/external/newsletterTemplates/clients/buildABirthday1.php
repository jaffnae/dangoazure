<?php
//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize



//1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
//2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
    //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
    //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
//3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
//4. Use the following link to send emails:
   //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
   //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_buildABirthday&newsletterTemplateName=clients/buildABirthday1&customSubject=true


    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Build a Birthday";
    $fromNameAndEmailObject["email"] = "fay@buildabirthday.co.nz";


    $sendGridApiHtml = '
    <html>

  <head>
    <style type="text/css"></style>
  </head>

  <body style="background: #f3f3f3; margin: 0; padding: 0; text-align: center; ">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td valign="top" style="background: #fff;">
            <img src="http://i.imgur.com/qZePJGk.png" alt="Logo" width="50%">
          </td>
        </tr>

      </tbody>
    </table>




    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left; margin: 0 auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <div style="width: 100%; height: 10px; background-color: #0fb2c4;"> </div>
            <hr />

            <div style="text-align: left; padding: 10px; font-size: 12px; color: #666">
              <span style=" ">Welcome to your Build a Birthday newsletter, bringing you fun, helpful tips and useful information. In the news is:</span>

              <ul>
                <li>FREE school holiday programme - Petone Store</li>
                <li>Chocolate Seashell cupcakes, plus our QUICK TIP for busy mums!</li>
                <li>New in-store</li>
                <li>Special Deal - save $60</li>
                <li>Competition</li>
              </ul>
            </div>

            <hr />
          </td>
        </tr>
      </tbody>
    </table>






    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <div style="width: 100%; text-align: left; height: 10px; background-color: #0fb2c4;"> </div>
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">FREE School Holiday Programme - Petone Store</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/xguaUkn.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br /> Come join us for some free school holiday fun:

              <br />
              <br />

              <b>Tuesday 12th July, 11am-1pm </b>
              <br /> Get your face painted by Galia Face painting

              <br />
              <br />

              <b>Wednesday 13th July, 12.30pm – 1.30pm</b>
              <br /> Finding Dory Fun

              <br />
              <br />

              <b>Wednesday 20th July, 12.30pm – 1.30pm</b>
              <br /> Finding Dory Fun

              <br />
              <br />

              <b>Thursday 21st July, 10am - 10.45am</b>
              <br /> Ariel the Little Mermaid from Enchanted Entertainment

              <br />
              <br /> There are limited spaces so please book now. For more information see www.buildabirthday.co.nz/pages/wellington-school-holiday-programme There are also other Free School Hoilday Programmes organised by the Jackson Street Programme, see
              <a href="http://www.jacksonstreet.co.nz/events/">http://www.jacksonstreet.co.nz/events/</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>





    <br />





    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <div style="width: 100%; text-align: left; height: 10px; background-color: #0fb2c4;"> </div>
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Chocolate Seashell cupcakes, plus our Quick tip for busy mums!</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/k7zmNDZ.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br /> These are simple, effective and oh so Yum! Perfect for the Finding Dory or Mermaid party or any party! As a working mum, I struggle to find the time to make party food the looks and tastes great. What to know my quick tip with this recipe?
              See <a href="http://www.buildabirthday.co.nz/blogs/party-ideas/chocolate-sea-shell-cupcakes">www.buildabirthday.co.nz/blogs/party-ideas/chocolate-sea-shell-cupcakes</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>


    <br />



    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <div style="width: 100%; text-align: left; height: 10px; background-color: #0fb2c4;"> </div>
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">New in-store:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/gbxfV55.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br /> Finding Dory has hit the big screens and is looking to be a box office leader. We have a comprehensive range of Finding Dory products plus lots of creative items to add the WOW factor to your party. We\'re excited to help customers with
              a fantastic theme to be creative with. Pop in-store or give us a call. See <a href="http://www.buildabirthday.co.nz/collections/finding-dory">http://www.buildabirthday.co.nz/collections/finding-dory</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>





    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <div style="width: 100%; text-align: left; height: 10px; background-color: #0fb2c4;"> </div>
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Special Deal:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/sTrfg2q.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br /> We have limited stock of these Meri Meri Pirate Baking Tins. Were $79.99, now $19.99, save $60. The perfect gift for any little pirate. The kit contains 3 cookie cutters, cupcake cases in two designs, recipe cards, an apron and designer
              spatula, all in a beautiful tin. Please order now to avoid disappointment at <a href="http://www.buildabirthday.co.nz/collections/pirate">http://www.buildabirthday.co.nz/collections/pirate</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>






    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
            <div style="width: 100%; text-align: left; height: 10px; background-color: #0fb2c4;"> </div>
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Competition:</span>
            <br />
            <br />


            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/n0rVRhN.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br /> Be into win the ever popular Star Wars Party Pack. Enter on <a href="http://www.facebook.com/buildabirthday">www.facebook.com/buildabirthday</a> or <a href="http://dgo.nz/uhWD6MY83964">click here</a>. Entries close 12th of July!

            </div>
          </td>
        </tr>
      </tbody>
    </table>





    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
        </tr>
        <tr>
          <td align="center" valign="top" colspan="2">
            <div style="padding-left: 8px; padding-right: 8px;">
              <br />
              <span style="font-size: 13px; color: #666;">Don\'t forget to Like use on</span>
            </div>

            <p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 5px 0;">


              <img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Facebook</a></strong> &nbsp; and &nbsp;

              <img src="http://i.imgur.com/cxoH7Pg.png" width="30" alt="Instagram" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Instagram</a></strong>

            </p>

            <div style="padding-left: 8px; padding-right: 8px;">
              <span style="font-size: 13px; color: #666;">to be kept up to date to the lastest information on parties and cake decorating.</span>
            </div>
            <br />
            <br />
            <br />
            <br />
            <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
              If you received this message in error please disregard and delete this.
              <br> You can also unsubscribe from these emails by following
              <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_buildABirthday" style="color: #aaa;">this link</a></p>
          </td>
        </tr>
      </tbody>
    </table>


  </body>

</html>';
      echo $sendGridApiHtml;




 ?>

<?php


/**************************************************
       ! ! ! ! ! WARNING ! ! ! ! ! !
***************************************************/
//Make sure the code for Unsubscribe is on the bottom:
/*

<!-- BEGIN FOOTER -->
<table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
  <tbody class="mcnTextBlockOuter">
   <tr>
      <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
       <!--[if mso]>
<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
<tr>
<![endif]-->

       <!--[if mso]>
<td valign="top" width="599" style="width:599px;">
<![endif]-->
       <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:100%; min-width:100%;" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>

             <td class="mcnTextContent" valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                <em>Copyright © 2016 Totally Balmy New Zealand, All rights reserved.</em>
                <br> You've received this email because you opted in during a Dango promotion, a competition, or on my website:
                <a style="color:#656565;font-weight:normal;text-decoration:underline" href="http://totallybalmy.us13.list-manage.com/track/click?u=7a555c31cf20566e9ab7c0dd2&amp;id=3f2c8efbfd&amp;e=17138fb4ea"
                target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://totallybalmy.us13.list-manage.com/track/click?u%3D7a555c31cf20566e9ab7c0dd2%26id%3D3f2c8efbfd%26e%3D17138fb4ea&amp;source=gmail&amp;ust=1479780733183000&amp;usg=AFQjCNHJ0NI9xOKqjj2kWLXGMOYiGjUD9A"><font color="#1155cc">www.totallybalmy.co.nz</font></a>
                <br>
                <br>
                <strong>Our mailing address is:</strong>
                <br> 4 Mahi Grove, Feilding, 4702
                <br>
                <br> Want to change how you receive these emails?
                <br> You can <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_totallyBalmy">unsubcribe here</a>
                <br> &nbsp;
             </td>
            </tr>
          </tbody>
       </table>
       <!--[if mso]>
</td>
<![endif]-->

       <!--[if mso]>
</tr>
</table>
<![endif]-->
      </td>
   </tr>
  </tbody>
</table>
<!-- /END FOOTER -->


*/

    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Totally Balmy";
    $fromNameAndEmailObject["email"] = "info@totallybalmy.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_totallyBalmy&newsletterTemplateName=clients/totallyBalmy1&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_totallyBalmy&newsletterTemplateName=clients/totallyBalmy1&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

  <head>
    <!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>It\'s beginning to look a lot like Christmas...</title>

    <style type="text/css">
      p {
        margin: 10px 0;
        padding: 0;
      }

      table {
        border-collapse: collapse;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        display: block;
        margin: 0;
        padding: 0;
      }

      img,
      a img {
        border: 0;
        height: auto;
        outline: none;
        text-decoration: none;
      }

      body,
      #bodyTable,
      #bodyCell {
        height: 100%;
        margin: 0;
        padding: 0;
        width: 100%;
      }

      #outlook a {
        padding: 0;
      }

      img {
        -ms-interpolation-mode: bicubic;
      }

      table {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }

      .ReadMsgBody {
        width: 100%;
      }

      .ExternalClass {
        width: 100%;
      }

      p,
      a,
      li,
      td,
      blockquote {
        mso-line-height-rule: exactly;
      }

      a[href^=tel],
      a[href^=sms] {
        color: inherit;
        cursor: default;
        text-decoration: none;
      }

      p,
      a,
      li,
      td,
      body,
      table,
      blockquote {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }

      .ExternalClass,
      .ExternalClass p,
      .ExternalClass td,
      .ExternalClass div,
      .ExternalClass span,
      .ExternalClass font {
        line-height: 100%;
      }

      a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
      }

      #bodyCell {
        padding: 10px;
      }

      .templateContainer {
        max-width: 600px !important;
      }

      a.mcnButton {
        display: block;
      }

      .mcnImage {
        vertical-align: bottom;
      }

      .mcnTextContent {
        word-break: break-word;
      }

      .mcnTextContent img {
        height: auto !important;
      }

      .mcnDividerBlock {
        table-layout: fixed !important;
      }
      /*
	@tab Page
	@section Background Style
	@tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
	*/

      body,
      #bodyTable {
        /*@editable*/
        background-color: #FAFAFA;
      }
      /*
	@tab Page
	@section Background Style
	@tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
	*/

      #bodyCell {
        /*@editable*/
        border-top: 0;
      }
      /*
	@tab Page
	@section Email Border
	@tip Set the border for your email.
	*/

      .templateContainer {
        /*@editable*/
        border: 0;
      }
      /*
	@tab Page
	@section Heading 1
	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
	@style heading 1
	*/

      h1 {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 26px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Page
	@section Heading 2
	@tip Set the styling for all second-level headings in your emails.
	@style heading 2
	*/

      h2 {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 22px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Page
	@section Heading 3
	@tip Set the styling for all third-level headings in your emails.
	@style heading 3
	*/

      h3 {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 20px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Page
	@section Heading 4
	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	@style heading 4
	*/

      h4 {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 18px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Preheader
	@section Preheader Style
	@tip Set the background color and borders for your email\'s preheader area.
	*/

      #templatePreheader {
        /*@editable*/
        background-color: #FAFAFA;
        /*@editable*/
        background-image: none;
        /*@editable*/
        background-repeat: no-repeat;
        /*@editable*/
        background-position: center;
        /*@editable*/
        background-size: cover;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
        /*@editable*/
        padding-top: 9px;
        /*@editable*/
        padding-bottom: 9px;
      }
      /*
	@tab Preheader
	@section Preheader Text
	@tip Set the styling for your email\'s preheader text. Choose a size and color that is easy to read.
	*/

      #templatePreheader .mcnTextContent,
      #templatePreheader .mcnTextContent p {
        /*@editable*/
        color: #656565;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 12px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Preheader
	@section Preheader Link
	@tip Set the styling for your email\'s preheader links. Choose a color that helps them stand out from your text.
	*/

      #templatePreheader .mcnTextContent a,
      #templatePreheader .mcnTextContent p a {
        /*@editable*/
        color: #656565;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Header
	@section Header Style
	@tip Set the background color and borders for your email\'s header area.
	*/

      #templateHeader {
        /*@editable*/
        background-color: #FFFFFF;
        /*@editable*/
        background-image: none;
        /*@editable*/
        background-repeat: no-repeat;
        /*@editable*/
        background-position: center;
        /*@editable*/
        background-size: cover;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
        /*@editable*/
        padding-top: 9px;
        /*@editable*/
        padding-bottom: 0;
      }
      /*
	@tab Header
	@section Header Text
	@tip Set the styling for your email\'s header text. Choose a size and color that is easy to read.
	*/

      #templateHeader .mcnTextContent,
      #templateHeader .mcnTextContent p {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Header
	@section Header Link
	@tip Set the styling for your email\'s header links. Choose a color that helps them stand out from your text.
	*/

      #templateHeader .mcnTextContent a,
      #templateHeader .mcnTextContent p a {
        /*@editable*/
        color: #2BAADF;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Body
	@section Body Style
	@tip Set the background color and borders for your email\'s body area.
	*/

      #templateBody {
        /*@editable*/
        background-color: #FFFFFF;
        /*@editable*/
        background-image: none;
        /*@editable*/
        background-repeat: no-repeat;
        /*@editable*/
        background-position: center;
        /*@editable*/
        background-size: cover;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
        /*@editable*/
        padding-top: 9px;
        /*@editable*/
        padding-bottom: 0;
      }
      /*
	@tab Body
	@section Body Text
	@tip Set the styling for your email\'s body text. Choose a size and color that is easy to read.
	*/

      #templateBody .mcnTextContent,
      #templateBody .mcnTextContent p {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Body
	@section Body Link
	@tip Set the styling for your email\'s body links. Choose a color that helps them stand out from your text.
	*/

      #templateBody .mcnTextContent a,
      #templateBody .mcnTextContent p a {
        /*@editable*/
        color: #2BAADF;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Columns
	@section Column Style
	@tip Set the background color and borders for your email\'s columns.
	*/

      #templateColumns {
        /*@editable*/
        background-color: #FFFFFF;
        /*@editable*/
        background-image: none;
        /*@editable*/
        background-repeat: no-repeat;
        /*@editable*/
        background-position: center;
        /*@editable*/
        background-size: cover;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 2px solid #EAEAEA;
        /*@editable*/
        padding-top: 0;
        /*@editable*/
        padding-bottom: 9px;
      }
      /*
	@tab Columns
	@section Column Text
	@tip Set the styling for your email\'s column text. Choose a size and color that is easy to read.
	*/

      #templateColumns .columnContainer .mcnTextContent,
      #templateColumns .columnContainer .mcnTextContent p {
        /*@editable*/
        color: #202020;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
      }
      /*
	@tab Columns
	@section Column Link
	@tip Set the styling for your email\'s column links. Choose a color that helps them stand out from your text.
	*/

      #templateColumns .columnContainer .mcnTextContent a,
      #templateColumns .columnContainer .mcnTextContent p a {
        /*@editable*/
        color: #2BAADF;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }
      /*
	@tab Footer
	@section Footer Style
	@tip Set the background color and borders for your email\'s footer area.
	*/

      #templateFooter {
        /*@editable*/
        background-color: #FAFAFA;
        /*@editable*/
        background-image: none;
        /*@editable*/
        background-repeat: no-repeat;
        /*@editable*/
        background-position: center;
        /*@editable*/
        background-size: cover;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
        /*@editable*/
        padding-top: 9px;
        /*@editable*/
        padding-bottom: 9px;
      }
      /*
	@tab Footer
	@section Footer Text
	@tip Set the styling for your email\'s footer text. Choose a size and color that is easy to read.
	*/

      #templateFooter .mcnTextContent,
      #templateFooter .mcnTextContent p {
        /*@editable*/
        color: #656565;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 12px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: center;
      }
      /*
	@tab Footer
	@section Footer Link
	@tip Set the styling for your email\'s footer links. Choose a color that helps them stand out from your text.
	*/

      #templateFooter .mcnTextContent a,
      #templateFooter .mcnTextContent p a {
        /*@editable*/
        color: #656565;
        /*@editable*/
        font-weight: normal;
        /*@editable*/
        text-decoration: underline;
      }

      @media only screen and (min-width:768px) {
        .templateContainer {
          width: 600px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        body,
        table,
        td,
        p,
        a,
        li,
        blockquote {
          -webkit-text-size-adjust: none !important;
        }
      }

      @media only screen and (max-width: 480px) {
        body {
          width: 100% !important;
          min-width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        #bodyCell {
          padding-top: 10px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .columnWrapper {
          max-width: 100% !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImage {
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnCartContainer,
        .mcnCaptionTopContent,
        .mcnRecContentContainer,
        .mcnCaptionBottomContent,
        .mcnTextContentContainer,
        .mcnBoxedTextContentContainer,
        .mcnImageGroupContentContainer,
        .mcnCaptionLeftTextContentContainer,
        .mcnCaptionRightTextContentContainer,
        .mcnCaptionLeftImageContentContainer,
        .mcnCaptionRightImageContentContainer,
        .mcnImageCardLeftTextContentContainer,
        .mcnImageCardRightTextContentContainer {
          max-width: 100% !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnBoxedTextContentContainer {
          min-width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageGroupContent {
          padding: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnCaptionLeftContentOuter .mcnTextContent,
        .mcnCaptionRightContentOuter .mcnTextContent {
          padding-top: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageCardTopImageContent,
        .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
          padding-top: 18px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageCardBottomImageContent {
          padding-bottom: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageGroupBlockInner {
          padding-top: 0 !important;
          padding-bottom: 0 !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageGroupBlockOuter {
          padding-top: 9px !important;
          padding-bottom: 9px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnTextContent,
        .mcnBoxedTextContentColumn {
          padding-right: 18px !important;
          padding-left: 18px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcnImageCardLeftImageContent,
        .mcnImageCardRightImageContent {
          padding-right: 18px !important;
          padding-bottom: 0 !important;
          padding-left: 18px !important;
        }
      }

      @media only screen and (max-width: 480px) {
        .mcpreview-image-uploader {
          display: none !important;
          width: 100% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
        h1 {
          /*@editable*/
          font-size: 22px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
        h2 {
          /*@editable*/
          font-size: 20px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
        h3 {
          /*@editable*/
          font-size: 18px !important;
          /*@editable*/
          line-height: 125% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
        h4 {
          /*@editable*/
          font-size: 16px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        .mcnBoxedTextContentContainer .mcnTextContent,
        .mcnBoxedTextContentContainer .mcnTextContent p {
          /*@editable*/
          font-size: 14px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Preheader Visibility
	@tip Set the visibility of the email\'s preheader on small screens. You can hide it to save space.
	*/
        #templatePreheader {
          /*@editable*/
          display: block !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Preheader Text
	@tip Make the preheader text larger in size for better readability on small screens.
	*/
        #templatePreheader .mcnTextContent,
        #templatePreheader .mcnTextContent p {
          /*@editable*/
          font-size: 14px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
        #templateHeader .mcnTextContent,
        #templateHeader .mcnTextContent p {
          /*@editable*/
          font-size: 16px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        #templateBody .mcnTextContent,
        #templateBody .mcnTextContent p {
          /*@editable*/
          font-size: 16px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Column Text
	@tip Make the column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
        #templateColumns .columnContainer .mcnTextContent,
        #templateColumns .columnContainer .mcnTextContent p {
          /*@editable*/
          font-size: 16px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

      @media only screen and (max-width: 480px) {
        /*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
        #templateFooter .mcnTextContent,
        #templateFooter .mcnTextContent p {
          /*@editable*/
          font-size: 14px !important;
          /*@editable*/
          line-height: 150% !important;
        }
      }

    </style>
  </head>

  <body>
    <center>
      <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
          <td align="center" valign="top" id="bodyCell">
            <!-- BEGIN TEMPLATE // -->
            <!--[if gte mso 9]>
						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
						<tr>
						<td align="center" valign="top" width="600" style="width:600px;">
						<![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
              <tr>
                <td valign="top" id="templatePreheader">
                  <table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                    <tbody class="mcnTextBlockOuter">
                      <tr>
                        <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
                          <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                          <!--[if mso]>
				<td valign="top" width="389" style="width:389px;">
				<![endif]-->
                          <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:389px;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>

                                <td class="mcnTextContent" valign="top" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                  <span style="font-size:14px"><strong>It\'s beginning to look a lot like Christmas...</strong></span>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--[if mso]>
				</td>
				<![endif]-->

                          <!--[if mso]>
				<td valign="top" width="209" style="width:209px;">
				<![endif]-->
                          <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:209px;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>

                                <td class="mcnTextContent" valign="top" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--[if mso]>
				</td>
				<![endif]-->

                          <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateHeader">
                  <table width="100%" class="mcnImageBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                    <tbody class="mcnImageBlockOuter">
                      <tr>
                        <td class="mcnImageBlockInner" valign="top" style="padding:9px">
                          <table width="100%" align="left" class="mcnImageContentContainer" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>
                                <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                  <img width="564" align="center" class="mcnImage" style="max-width:800px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" alt="" src="https://gallery.mailchimp.com/7a555c31cf20566e9ab7c0dd2/images/20231bfb-67e7-482a-8b4a-a7b656a9c7f5.jpg">


                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateBody">
                  <table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                    <tbody class="mcnTextBlockOuter">
                      <tr>
                        <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
                          <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                          <!--[if mso]>
				<td valign="top" width="599" style="width:599px;">
				<![endif]-->
                          <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:100%; min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>

                                <td class="mcnTextContent" valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                  <h1><strong><span style="font-size:18px">Time to make someone\'s day...</span></strong><br>
<span style="font-size:12px">Ok, two people. I\'ve been running competitions with Under 5\'s Online Directory, so welcome to all my new subscribers from there! And you\'ll all be dying to know who has won the soap right?! Well, Hannah Southgate-McNeil is our lucky winner! Congratulations Hannah :)<br>
I\'ve also just had a 3 day Craft Market in Feilding, where I live. Therefore, I need to announce the winner of the pamper pack. So congratulations goes to Bianca Henderson! I\'ll be in touch with you lucky ladies asap.<br>
Make sure you keep an eye on Facebook for updates on where I\'ll be, and which competitions I\'ll be running. You\'ve got to be in to win xxx</span></h1>

                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--[if mso]>
				</td>
				<![endif]-->

                          <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="100%" class="mcnBoxedTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                    <tbody class="mcnBoxedTextBlockOuter">
                      <tr>
                        <td class="mcnBoxedTextBlockInner" valign="top">

                          <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                          <table width="100%" align="left" class="mcnBoxedTextContentContainer" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>

                                <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                  <table width="100%" class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #404040;" border="0" cellspacing="0" cellpadding="18">
                                    <tbody>
                                      <tr>
                                        <td class="mcnTextContent" valign="top" style="text-align: center;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;">
                                          <span style="font-size:14px"><strong>Follow me, I have no idea where I\'m going, but let\'s enjoy the journey!</strong></span> <span style="font-size:12px">Like many small businesses these days, most of&nbsp;our communication happens through Social Media. The best way to find out about recipes, specials, new products, and everything really, is to follow Totally Balmy on <a style="color:#2baadf;font-weight:normal;text-decoration:underline" href="http://totallybalmy.us13.list-manage.com/track/click?u=7a555c31cf20566e9ab7c0dd2&amp;id=b1e3054f01&amp;e=17138fb4ea" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://totallybalmy.us13.list-manage.com/track/click?u%3D7a555c31cf20566e9ab7c0dd2%26id%3Db1e3054f01%26e%3D17138fb4ea&amp;source=gmail&amp;ust=1479780733183000&amp;usg=AFQjCNFWfnOogDWMBWdZu8p7twFe0me4bw">Facebook</a><br>
Furthermore, if you\'ve got an interest in living more naturally, and you want to connect with like-minded people to share ideas, come and join us in our <a style="color:#2baadf;font-weight:normal;text-decoration:underline" href="http://totallybalmy.us13.list-manage1.com/track/click?u=7a555c31cf20566e9ab7c0dd2&amp;id=46bdcb377c&amp;e=17138fb4ea" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://totallybalmy.us13.list-manage1.com/track/click?u%3D7a555c31cf20566e9ab7c0dd2%26id%3D46bdcb377c%26e%3D17138fb4ea&amp;source=gmail&amp;ust=1479780733183000&amp;usg=AFQjCNFxJf4V6eMsVYFfeoSv2lkmA6GkPQ">group</a>!</span>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--[if gte mso 9]>
				</td>
				<![endif]-->

                          <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateColumns">
                  <!--[if gte mso 9]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="300" style="width:300px;">
									<![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer">
                        <table width="100%" class="mcnImageBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                          <tbody class="mcnImageBlockOuter">
                            <tr>
                              <td class="mcnImageBlockInner" valign="top" style="padding:9px">
                                <table width="100%" align="left" class="mcnImageContentContainer" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                    <tr>
                                      <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                        <img width="264" align="center" class="mcnImage" style="max-width:640px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" alt="" src="https://gallery.mailchimp.com/7a555c31cf20566e9ab7c0dd2/images/2a432f0e-0ff7-4b23-bbbb-70d93e7cedb6.jpg">


                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                          <tbody class="mcnTextBlockOuter">
                            <tr>
                              <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
                                <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                                <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
                                <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:100%; min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                    <tr>

                                      <td class="mcnTextContent" valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                        <strong>Tools of the trade! </strong><span style="font-size:12px">Soap making is a lot of fun as it\'s quite creative. However, it\'s an art form, &amp; can also be incredibly frustrating. You need a lot of safety equipment, &amp; it can go wrong in an instant, so definitely not an activity to do with children! It\'s currently my best-selling product, yet takes weeks to cure, so there\'s sometimes a wait on particular scents. To read about why you should use soap, check out my blog post <a href="http://totallybalmy.co.nz/2016/07/26/bubble/" target="_blank">here</a></span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <!--[if mso]>
				</td>
				<![endif]-->

                                <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
									</td>
									<td align="center" valign="top" width="300" style="width:300px;">
									<![endif]-->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="300" class="columnWrapper">
                    <tr>
                      <td valign="top" class="columnContainer">
                        <table width="100%" class="mcnImageCardBlock" border="0" cellspacing="0" cellpadding="0">
                          <tbody class="mcnImageCardBlockOuter">
                            <tr>
                              <td class="mcnImageCardBlockInner" valign="top" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                <table width="100%" align="left" class="mcnImageCardBottomContent" style="background-color: #404040;" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                    <tr>
                                      <td align="left" class="mcnImageCardBottomImageContent" valign="top" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;">



                                        <img width="263" class="mcnImage" style="max-width:800px;" alt="" src="https://gallery.mailchimp.com/7a555c31cf20566e9ab7c0dd2/images/7148b558-2e84-421e-9120-017a387a2f4e.jpg">


                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="245" class="mcnTextContent" valign="top" style="padding: 9px 18px;text-align: center;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;">
                                        <strong><span style="font-size:18px">Present ideas!</span> </strong>Here\'s a few ideas for your loved ones
                                        <br>
                                        <strong>Child</strong>: Puzzle, book, kite, instrument, pot &amp; seeds, ball, water paint book, water gun
                                        <br>
                                        <strong>Adult</strong>: Colouring book, cookbook stand, movie voucher, soap, lip balm
                                        <br>
                                        <strong>Jars</strong>! Fill a jar with shortbread, chocolate, nuts, beads to make a necklace, mini soaps, DIY biscuits etc.
                                        <br> That\'s a few to get your started! I\'m nearly finished Christmas shopping, phew!
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>




                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <!--[if gte mso 9]>
									</td>
									</tr>
									</table>
									<![endif]-->
                </td>
              </tr>
              <tr>
                <td valign="top" id="templateFooter">
                  <table width="100%" class="mcnBoxedTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                    <tbody class="mcnBoxedTextBlockOuter">
                      <tr>
                        <td class="mcnBoxedTextBlockInner" valign="top">

                          <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                          <table width="100%" align="left" class="mcnBoxedTextContentContainer" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>

                                <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                  <table width="100%" class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #404040;" border="0" cellspacing="0" cellpadding="18">
                                    <tbody>
                                      <tr>
                                        <td class="mcnTextContent" valign="top" style="text-align: center;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;">
                                          <strong>Wash, exfoliate, clean </strong>- In my latest blog I tell you all about my daily cleansing and beauty routine. It may be quite different from yours. <strong>Here\'s a snippet:</strong> Have you ever thought
                                          about your daily cleaning/beauty regime? How do you clean your skin? Do you know what’s in the products you use? Do you really need to use all of those fancy lotions and potions? Let’s examine my routine for a
                                          moment. And this is only what I do; not necessarily what you should do. I’d just like to give you an example of what another fellow human does each day to cleanse themselves…Click on this <a href="http://totallybalmy.co.nz/2016/11/20/daily-cleansing-and-beauty-routine/"
                                          target="_blank">button </a>to read the whole post xx
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--[if gte mso 9]>
				</td>
				<![endif]-->

                          <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>



                  <!-- BEGIN FOOTER -->
                  <table width="100%" class="mcnTextBlock" style="min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                    <tbody class="mcnTextBlockOuter">
                      <tr>
                        <td class="mcnTextBlockInner" valign="top" style="padding-top:9px;">
                          <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->

                          <!--[if mso]>
				<td valign="top" width="599" style="width:599px;">
				<![endif]-->
                          <table width="100%" align="left" class="mcnTextContentContainer" style="max-width:100%; min-width:100%;" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>

                                <td class="mcnTextContent" valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                  <em>Copyright © 2016 Totally Balmy New Zealand, All rights reserved.</em>
                                  <br> You\'ve received this email because you opted in during a Dango promotion, a competition, or on my website:
                                  <a style="color:#656565;font-weight:normal;text-decoration:underline" href="http://totallybalmy.us13.list-manage.com/track/click?u=7a555c31cf20566e9ab7c0dd2&amp;id=3f2c8efbfd&amp;e=17138fb4ea"
                                  target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://totallybalmy.us13.list-manage.com/track/click?u%3D7a555c31cf20566e9ab7c0dd2%26id%3D3f2c8efbfd%26e%3D17138fb4ea&amp;source=gmail&amp;ust=1479780733183000&amp;usg=AFQjCNHJ0NI9xOKqjj2kWLXGMOYiGjUD9A"><font color="#1155cc">www.totallybalmy.co.nz</font></a>
                                  <br>
                                  <br>
                                  <strong>Our mailing address is:</strong>
                                  <br> 4 Mahi Grove, Feilding, 4702
                                  <br>
                                  <br> Want to change how you receive these emails?
                                  <br> You can <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_totallyBalmy">unsubcribe here</a>
                                  <br> &nbsp;
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <!--[if mso]>
				</td>
				<![endif]-->

                          <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- /END FOOTER -->




                </td>
              </tr>
            </table>
            <!--[if gte mso 9]>
						</td>
						</tr>
						</table>
						<![endif]-->
            <!-- // END TEMPLATE -->
          </td>
        </tr>
      </table>
    </center>
  </body>

</html>
      ';

echo $sendGridApiHtml;




 ?>

<?php
//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize
//http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[DB_NAME]&newsletterTemplateName=clients/GiftHutt1&customSubject=true


//1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
//2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
    //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
    //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
//3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
//4. Use the following link to send emails:
   //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
   //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_z_cl_giftHutt&newsletterTemplateName=clients/GiftHutt3&customSubject=true




//Make sure the code for Unsubscribe is on the bottom:
/*


<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>


*/



    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Gift Hutt";
    $fromNameAndEmailObject["email"] = "info@gifthutt.co.nz";


    $sendGridApiHtml = '
    <html>
<head>

</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="x_bodyTable" style="border-collapse:collapse; height:100%; margin:0; padding:0; width:100%; background-color:#cac8ca">
  <tbody>
    <tr>
      <td align="center" valign="top" id="x_bodyCell" style="height:100%; margin:0; padding:0; width:100%; border-top:0">
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateContainer" style="border-collapse:collapse; border:0">
          <tbody>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templatePreheader" style="border-collapse:collapse; background-color:#9e7da2; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_preheaderContainer" style="padding-top:9px; padding-bottom:9px">







                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="color:#202020; font-weight:normal; text-decoration:underline">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateHeader" style="border-collapse:collapse; background-color:#ffffff; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_headerContainer" style="">
                        <table class="x_mcnImageBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnImageBlockOuter">
                            <tr>
                              <td class="x_mcnImageBlockInner" valign="top" style="padding:9px">
                                <table class="x_mcnImageContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td class="x_mcnImageContent" valign="top" style="padding-right:9px; padding-left:9px; padding-top:0; padding-bottom:0; text-align:center">
                                        <a href="http://gifthutt.nz/" target="_blank" title="" class="" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/a086a34d-be33-47f8-a46f-459114e254b9.gif" alt="" class="x_mcnImage" align="middle" width="359" style="max-width:359px; padding-bottom:0; display:inline!important; vertical-align:bottom; border:0; height:auto; outline:none; text-decoration:none">                                          </a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="x_mcnBoxedTextBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnBoxedTextBlockOuter">
                            <tr>
                              <td class="x_mcnBoxedTextBlockInner" valign="top" style="">
                                <table class="x_mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px">
                                        <table class="x_mcnTextContentContainer" border="0" cellpadding="18" cellspacing="0" width="100%" style="min-width:100%!important; background-color:#565555; border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnTextContent" valign="top" style="color:#F2F2F2; font-family:Helvetica; font-size:14px; font-weight:normal; text-align:center; word-break:break-word; line-height:150%">
                                                <span style="font-size:15px">Gift Hutt is the art and craft hub in the Hutt.&nbsp;<br>
Our great store is in the heart of Lower Hutt at 136 High Street.<br>
We have a wide range of&nbsp;genuine and unique gifts, and&nbsp; craft classes&nbsp;and craft supplies for your creative needs.</span> </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnImageGroupBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnImageGroupBlockOuter">
                            <tr>
                              <td valign="top" class="x_mcnImageGroupBlockInner" style="padding:9px">
                                <table align="left" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="x_mcnImageGroupContentContainer" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td class="x_mcnImageGroupContent" valign="top" style="padding-left:9px; padding-top:0; padding-bottom:0">
                                        <a href="http://gifthutt.nz/shop/" target="_blank" title="" class="" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/f6290a5a-7254-42e0-af76-ae09c7ba3fbc.gif" alt="" width="263" class="x_mcnImage" style="max-width:323px; padding-bottom:0; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                          </a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <table align="right" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="x_mcnImageGroupContentContainer" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td class="x_mcnImageGroupContent" valign="top" style="padding-right:9px; padding-top:0; padding-bottom:0">
                                        <a href="http://gifthutt.nz/about-us/" target="_blank" title="" class="" style=""><img src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/9bbbc740-e9f9-4985-81a1-60867c3ee160.gif" alt="" width="263" class="x_mcnImage" style="max-width:323px; padding-bottom:0; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                          </a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateUpperBody" style="border-collapse:collapse; background-color:#FFFFFF; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_upperBodyContainer" style="">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnTextBlockOuter">
                            <tr>
                              <td valign="top" class="x_mcnTextBlockInner" style="padding-top:9px">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextContentContainer" style="max-width:100%; min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnTextContent" style="padding:0px 18px 9px; text-align:center; word-break:break-word; color:#404040; font-family:Helvetica; font-size:16px; line-height:150%">
                                        <h2 class="x_null" style="text-align:center; display:block; margin:0; padding:0; font-family:Helvetica; font-size:26px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; color:#202020!important">
Gift Hutt craft workshops and News</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnTextBlockOuter">
                            <tr>
                              <td valign="top" class="x_mcnTextBlockInner" style="padding-top:9px">
                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnTextContentContainer" style="max-width:100%; min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px; word-break:break-word; color:#404040; font-family:Helvetica; font-size:16px; line-height:150%; text-align:left">
                                        Welcome to all the new subscribers to this newsletter. &nbsp;Many of you would have signed up as part of a giveaway that I have been running and I would love for you to continue to receive the newsletters keeping you up to date with the news at Gift Hutt.
                                        &nbsp;But if you do not wish to continue to receive the newsletters there is an unsubscribe button at the bottom of this newsletter.
                                        <br>
                                        <br>
                                        <em><strong>What\'s New in Gift Hutt</strong></em>
                                        <br>
                                        <br> For the paper crafters the Spellbinders dies and embossing folders are now in store and look amazing. &nbsp;Check them out<a href="http://gifthutt.nz/product-category/craft-supplies/spellbinders/" target="_blank"
                                        style="color:#FE6F00; font-weight:normal; text-decoration:underline"> here</a>. &nbsp;
                                        <br>
                                        <br> I also have the new <a href="http://gifthutt.nz/product-category/craft-supplies/die-cutting-machines/" target="_blank" style="color:#FE6F00; font-weight:normal; text-decoration:underline">Big Shot Plus die cutting machine</a>.
                                        &nbsp;This is in between the Big Shot and the Big Shot Pro with a good sized A4 platform for the larger dies and even for smaller dies for turning around in for even cutting. &nbsp;I have one in store or you could
                                        order one and I could have it in store within a week for you. &nbsp;This is priced at a very competitive $365 and will keep you going for a long time. &nbsp;I am also able to get in any Big Shot accessories including
                                        the magnetic platforms and extra plates etc.
                                        <br>
                                        <br> A new shipment of <a href="http://gifthutt.nz/product-category/craft-supplies/heartfelt-creations/" target="_blank" style="color:#FE6F00; font-weight:normal; text-decoration:underline">Heartfelt Creations</a> products
                                        has arrived in time for the Paper Arts and Crafts Show later in the month. &nbsp;
                                        <br>
                                        <br> Also just arrived are some amazing <a href="http://gifthutt.nz/product/all-things-painted-decorated-bottles/" target="_blank" style="color:#FE6F00; font-weight:normal; text-decoration:underline">hand painted bottles</a>                                        with vintage images from All Things Painted. &nbsp;These are lovely and great to put a single flower in or just add to your shabby chic decor. &nbsp;
                                        <br>
                                        <br>
                                        <em><strong>Paper Arts and Crafts Show August 20th to 21st, Te Rauparaha Arena, Porirua.</strong></em>
                                        <br>
                                        <br> I will be at the Paper Arts and Crafts Show this weekend with all the paper crafting supplies including Art Glitter glue and glitter products, Heartfelt Creations, Spellbinders, Sue Wilson, Elizabeth Crafts and
                                        more. &nbsp;Come on down and have some fun making flowers and checking out the products. &nbsp;
                                        <br>
                                        <br>
                                        <em><strong>Wainuiomata Pop Up Shop</strong></em>
                                        <br>
                                        <br> I have had the opportunity to open up a pop up shop in the Wainuiomata Mall from 1st October to 24 December and I am looking for people who would like to sell their products in the shop for the Christmas shopping
                                        period. &nbsp;If you are interested please contact me at info@gifthutt.nz.
                                        <br>
                                        <br>
                                        <em><strong>Workshops and Classes:</strong></em>
                                        <br>
                                        <br> We are having a quiet month for workshops this month because of the show but Beth will be doing another Bring Your Own Project session where you can bring any upholstery or painting project down and get advice
                                        on technique and colour etc.
                                        <br>
                                        <br>
                                        <strong>Bring Your Own Project</strong>
                                        <br> Saturday 13 August 1.30 pm to 4 pm\'
                                        <br> Cost $25.
                                        <br>
                                        <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                        <br>
                                        <br> Over the next few months I will be gradually adding a wider range of art and craft supplies for the artists and crafters to be able to purchase. &nbsp;As a paper crafter myself, I know how frustrating it is not
                                        being able to purchase products easily in New Zealand and I aim to fill some of those gaps. &nbsp;I am trying to source some very basic upholstery supplies to hold in Gift Hutt at the moment. &nbsp;If there is any
                                        particular product that you would like to be able to purchase through Gift Hutt please feel free to contact me and I can see if I can source this for you. &nbsp;I am happy to look for any crafts not just papercrafting.
                                        <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                        <br>
                                        <br> Have a great month and Happy Crafting
                                        <br> Patricia and Beth </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateColumns" style="border-collapse:collapse; background-color:#FFFFFF; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td align="left" valign="top" class="x_columnsContainer" width="50%" style="">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_templateColumn" style="border-collapse:collapse">
                          <tbody>
                            <tr>
                              <td valign="top" class="x_leftColumnContainer" style=""></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td align="left" valign="top" class="x_columnsContainer" width="50%" style="">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_templateColumn" style="border-collapse:collapse">
                          <tbody>
                            <tr>
                              <td valign="top" class="x_rightColumnContainer" style=""></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateLowerBody" style="border-collapse:collapse; background-color:#FFFFFF; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_lowerBodyContainer" style="">
                        <table class="x_mcnBoxedTextBlock" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnBoxedTextBlockOuter">
                            <tr>
                              <td class="x_mcnBoxedTextBlockInner" valign="top" style="">
                                <table class="x_mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%; border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px">
                                        <table class="x_mcnTextContentContainer" border="0" cellpadding="18" cellspacing="0" width="100%" style="min-width:100%!important; background-color:#662A58; border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnTextContent" valign="top" style="color:#FFFFFF; font-family:Helvetica; font-size:16px; font-weight:bold; line-height:100%; text-align:left; word-break:break-word">
                                                Upcoming workshops at Gift Hutt, 136 High Street, Lower Hutt </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnCaptionBlock" style="border-collapse:collapse">
                          <tbody class="x_mcnCaptionBlockOuter">
                            <tr>
                              <td class="x_mcnCaptionBlockInner" valign="top" style="padding:9px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightContentOuter" width="100%" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td valign="top" class="x_mcnCaptionRightContentInner" style="padding:0 9px">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="x_mcnCaptionRightImageContentContainer" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td class="x_mcnCaptionRightImageContent" valign="top" style=""><img src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/36a77e93-8267-4308-8c68-47e8c43671f0.jpg" alt="" width="263" class="x_mcnImage" style="max-width:788px; border:0; height:auto; outline:none; text-decoration:none; vertical-align:bottom">                                                </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <table class="x_mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="263" style="border-collapse:collapse">
                                          <tbody>
                                            <tr>
                                              <td valign="top" class="x_mcnTextContent" style="word-break:break-word; color:#404040; font-family:Helvetica; font-size:14px; line-height:150%; text-align:center">
                                                <h3 class="x_null" style="display:block; margin:0; padding:0; font-family:Helvetica; font-size:18px; font-style:normal; font-weight:bold; line-height:125%; letter-spacing:normal; text-align:left; color:#404040!important">
Bring Your Own Project</h3>
                                                <div style="text-align:left">Use our space and get advice on your painting and upholstery projects.
                                                  <br>
                                                  <br>
                                                  <strong>When</strong>:Saturday 13 August
                                                  <br>
                                                  <strong>Time</strong>: 1.30 pm to 4 pm
                                                  <br>
                                                  <strong>Bring</strong>: Your project to work on.
                                                  <br>
                                                  <strong>Cost</strong>: $25</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_mcnButtonBlock" style="min-width:100%; border-collapse:collapse">
                          <tbody class="x_mcnButtonBlockOuter">
                            <tr>
                              <td valign="top" align="right" class="x_mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px">
                                <table border="0" cellpadding="0" cellspacing="0" class="x_mcnButtonContentContainer" style="border-collapse:separate!important; border-radius:0px; background-color:#702E61">
                                  <tbody>
                                    <tr>
                                      <td align="center" valign="middle" class="x_mcnButtonContent" style="font-family:Arial; font-size:16px; padding:15px">
                                        <a href="http://gifthutt.nz/product/bring-project-saturday-13th-august-1-30-pm-4-pm/" target="_blank" class="x_mcnButton" title="Purchase ticket" style="font-weight:bold; letter-spacing:normal; line-height:100%; text-align:center; text-decoration:none; color:#FFFFFF; display:block">Purchase ticket</a>                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" valign="top" style="">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="x_templateFooter" style="border-collapse:collapse; background-color:#9e7da2; border-top:0; border-bottom:0">
                  <tbody>
                    <tr>
                      <td valign="top" class="x_footerContainer" style="padding-top:9px; padding-bottom:9px">










                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>








<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>




</body>
</html>

';
      echo $sendGridApiHtml;




 ?>

<?php


/**************************************************
       ! ! ! ! ! WARNING ! ! ! ! ! !
***************************************************/
//Make sure the code for Unsubscribe is on the bottom:
/*

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_tinyTurtles" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>
*/

    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Tiny Turtles";
    $fromNameAndEmailObject["email"] = "info@tinyturtles.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_tinyTurtles&newsletterTemplateName=clients/tinyTurtles2&customSubject=true

       //OR

       //If we wanna use Dango Business Focused Account:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_tinyTurtles&newsletterTemplateName=clients/tinyTurtles2&customSubject=true&sendGridAccountToUse=sendGridBusinessAccount






    $sendGridApiHtml = '
    <html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraph.org/schema/"><head>

<meta property="og:title" content="BLACK FRIDAY 25% off today only&nbsp;💗💗💗">
<meta property="fb:page_id" content="43929265776">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta charset="UTF-8">
<title>Untitled Document</title>
<style type="text/css">
body {font-family: Helvetica, Arial, sans-serif; font-size: 12px; line-height: 140%;}
img {display:block;}
p, ul, ol, li { font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 140%; margin: 0; padding: 0; padding-bottom: 10px; }
ul, ol, li { margin-left: 10px; padding-left: 10px; }
a {
	font-family: Helvetica, Arial, sans-serif;
	color: #00A69C;
	text-decoration: none;
}
a.block-link {
	color: #fff;
	background-color: #00A69C;
	text-transform: uppercase;
	padding: 10px 12px;
    margin: 5px 0px;
    display: inline-block;
}
ul.nav {
    width: 100%;
    display: block;
    font-size: 14px;
    text-transform: uppercase;
    color: #00A69C;
    border-top: 1px solid #EEEEEE;
    border-bottom: 1px solid #EEEEEE;
    margin: 35px 0 0 0;
    padding: 15px 0 0 0;
    height: 30px;
    padding-left: 0px !important;
    margin-left: 0px !important;
}
ul.nav li {
    font-size: 14px;
    float: left;
    list-style: none;
    margin: 0;
    padding: 0 50px 0 0;
    padding-left: 0px !important;
    margin-left: 0px !important;
}
ul.nav li.last {
    padding: 0;
    padding-left: 0px !important;
    margin-left: 0px !important;
}
p.disclaimer {
    padding: 30px 0;
}
.txt-container {
    padding: 20px 0px;
}
.txt-container p {
    font-size: 16px;
}
p.subhead {
    text-transform: uppercase;
    font-weight: bold;
    border-top: 2px solid black;
    font-family: Helvetica Neue, Heletica, Arial;
    margin-top: 10px;
}
p.intro {
    font-style: italic;
    font-size: 18px;
    line-height: 160%;
}
</style>
</head> <body id="archivebody" style="padding: 0;margin: 15px auto; background-color: #fff;">


<table width="645" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody><tr>
	<td align="left" width="25%" valign="bottom">
    	<table border="0" cellpadding="0" cellspacing="0"><tbody><tr>
        	<td><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/83c21b11-c6d4-4ff3-a21c-7362acdd6728.gif" width="39" height="54" alt="Join us on Facebook" style="margin-right: 10px;" border="0"></a></td>
            <td><a href="https://twitter.com/TinyTurtles_NZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3f7d7a96-77bb-445c-9c60-e61cfd6b0ef2.gif" width="39" height="54" alt="Join us on Twitter" border="0"></a></td>
      </tr></tbody></table>
    </td>
    <td align="center" width="50%"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/f9ce15fa-bd61-4b1e-9859-a1a655737f85.gif" width="260" height="126" alt="Shop for BONDS on Tiny Turtles.co.nz" border="0"></a></td>
    <td align="right" width="25%" valign="bottom"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3e5dad31-f112-44db-ab59-7ca98ec53010.gif" width="101" height="105" alt="Free shipping on orders over $70!" border="0"></a></td>
</tr>
<tr>
    <td colspan="3">
        <ul class="nav">
            <li><a href="http://tinyturtles.co.nz/product-category/new/">New</a></li>
            <li><a href="http://tinyturtles.co.nz/product-category/boy/">Boys</a></li>
            <li><a href="http://tinyturtles.co.nz/product-category/girl/">Girls</a></li>
            <li><a href="http://tinyturtles.co.nz/product-category/newborn/">Newborns</a></li>
            <li><a href="http://tinyturtles.co.nz/product-category/wondersuit/">Wondersuits</a></li>
            <li class="last"><a href="http://tinyturtles.co.nz/product-category/maternity/">Maternity</a></li>
        </ul>
    </td>
</tr>

<tr>
    <td colspan="3"><a href="http://tinyturtles.co.nz/shop/"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/dbf530ed-a4c8-4be5-adaf-8db30911a81e.jpg" width="635" height="345" border="0" alt="Black Friday"></a></td>
</tr>
<tr>
    <td colspan="3">
    <div class="txt-container">
        <p>BLACK FRIDAY, Yes we have 25% off everything in store today (including reduced items), just use the code "BLACK" in your cart (not at checkout) for this great discount.  Not available for laybys. </p>
        <p>  </p>
        <p><em> -Eeeek</em></p>
    </div>

</td>
</tr>

<tr>
<td colspan="3">
<a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/7efbb7e1-2b39-4024-b96f-ee82c5e85784.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>

</tr>
<tr>
    <td colspan="3"><a href="http://tinyturtles.co.nz/"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/8865a414-3f5b-444e-866e-ad382dcf7f62.jpg" width="645" height="897" border="0" alt="New Website Layout"></a></td>
</tr>
<tr>
    <td colspan="3">
    <div class="txt-container">
        <p>POP UP SHOP, Yes we have our PUKEKOHE POP UP SHOP OPEN AGAIN. we are 110 King Street Pukekohe, We have lots of great deals in store happening too. Pop in and mention Black Friday and we\'ll give you 25% off in store too.<br>
    </p>
        <p><em>  </em></p>
    </div>
</td>
</tr>

<tr>
<td colspan="3">
<a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/7efbb7e1-2b39-4024-b96f-ee82c5e85784.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>
</tr>
<tr>
    <td colspan="3"><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/_compresseds/c9528150-1ee2-463a-97b3-2d8fef8e6901.jpg" width="645" height="485" border="0" alt="We Love Marys Market!"></a></td>
</tr>
<tr>
    <td colspan="3">
    <div class="txt-container">
        <p>Layby\'s, YES WE DO, simple select Internet Banking when you make your online order and then send us a message to ask for us to set up a layby to suit.  </p>
    </div>
</td>
</tr>

<tr>
<td colspan="3">
<a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/801fa3d3-d91a-4f86-8c3c-79629c859df1.gif" width="645" height="100" alt="Shop BONDS at TinyTurtles.co.nz" border="0"></a></td>
</tr>




<tr>
    <td colspan="3">&nbsp;</td>
</tr>
</tbody></table>


<table width="645" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody><tr>
    <td width="215" height="215"><a href="http://vimeo.com/102892239"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/27aa85ea-9017-4299-8a5f-9acb15d6abdd.gif" width="215" height="215" border="0" alt="Baby Search Winners Video"></a></td>
    <td width="215" height="215"><a href="https://www.facebook.com/TinyTurtlesNZ"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/d361fecc-2a3f-40e3-8c61-2ffb2aedbb8f.gif" width="215" border="0" height="215" alt="Join us on Facebook"></a></td>
    <td width="215" height="215"><a href="http://www.tinyturtles.co.nz"><img src="https://gallery.mailchimp.com/ed8d4869a0ae48d49924dff57/images/3d22163f-7c03-4c79-9b7b-ac98c7c73a5b.gif" width="215" height="215" border="0" alt="Free shipping on orders over $70 because we love you"></a></td>
</tr>
<tr>
    <td colspan="3" align="center">
    <p class="disclaimer">
            If you would like to unsubscribe, <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_tinyTurtles">click here</a><br>
            Tiny Turtles
PO BOX 853
Pukekohe
Pukekohe, Auckland 2120
New Zealand</p>
    </td>
</tr>
</tbody></table>


<div id="global-zeroclipboard-html-bridge" class="global-zeroclipboard-container" data-clipboard-ready="true" style="position: absolute; left: -9999px; top: -9999px; width: 15px; height: 15px; z-index: 9999;">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" width="100%" height="100%">         <param name="movie" value="//downloads.mailchimp.com/ZeroClipboard.07.13.swf?nocache=1478769643634">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedDomain=us1.campaign-archive1.com">         <embed src="//downloads.mailchimp.com/ZeroClipboard.07.13.swf?nocache=1478769643634" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="100%" height="100%" name="global-zeroclipboard-flash-bridge" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedDomain=us1.campaign-archive1.com" scale="exactfit">                </object></div><div id="social-proxy" style="display:none;">                   <div id="social-proxy_content">                   </div>                   <a href="#" title="Close" id="social-proxy_close">Close</a>                 </div></body></html>

';

echo $sendGridApiHtml;




 ?>

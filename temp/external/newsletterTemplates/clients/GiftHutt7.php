<?php

/**************************************************
       ! ! ! ! ! WARNING ! ! ! ! ! !
***************************************************/
//Make sure the code for Unsubscribe is on the bottom:
/*

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
  <tbody>
    <tr>
    </tr>
    <tr>
      <td align="center" valign="top" colspan="2">

        <br />
        <br />
        <br />
        <br />
        <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
          If you received this message in error please disregard and delete this.
          <br> You can also unsubscribe from these emails by following
          <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
      </td>
    </tr>
  </tbody>
</table>


*/


//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize
//http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[DB_NAME]&newsletterTemplateName=clients/GiftHutt7&customSubject=true


//1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
//2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
    //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
    //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
//3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
//4. Use the following link to send emails:
   //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
   //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_giftHutt&newsletterTemplateName=clients/GiftHutt7&customSubject=true


    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Gift Hutt";
    $fromNameAndEmailObject["email"] = "info@gifthutt.co.nz";


    $sendGridApiHtml = '
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

      <head>
        <!-- NAME: REST & RELAXATION -->
        <!--[if gte mso 15]>
    		<xml>
    			<o:OfficeDocumentSettings>
    			<o:AllowPNG/>
    			<o:PixelsPerInch>96</o:PixelsPerInch>
    			</o:OfficeDocumentSettings>
    		</xml>
    		<![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>*|MC:SUBJECT|*</title>

        <style type="text/css">
          p {
            margin: 10px 0;
            padding: 0;
          }

          table {
            border-collapse: collapse;
          }

          h1,
          h2,
          h3,
          h4,
          h5,
          h6 {
            display: block;
            margin: 0;
            padding: 0;
          }

          img,
          a img {
            border: 0;
            height: auto;
            outline: none;
            text-decoration: none;
          }

          body,
          #bodyTable,
          #bodyCell {
            height: 100%;
            margin: 0;
            padding: 0;
            width: 100%;
          }

          #outlook a {
            padding: 0;
          }

          img {
            -ms-interpolation-mode: bicubic;
          }

          table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
          }

          .ReadMsgBody {
            width: 100%;
          }

          .ExternalClass {
            width: 100%;
          }

          p,
          a,
          li,
          td,
          blockquote {
            mso-line-height-rule: exactly;
          }

          a[href^=tel],
          a[href^=sms] {
            color: inherit;
            cursor: default;
            text-decoration: none;
          }

          p,
          a,
          li,
          td,
          body,
          table,
          blockquote {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
          }

          .ExternalClass,
          .ExternalClass p,
          .ExternalClass td,
          .ExternalClass div,
          .ExternalClass span,
          .ExternalClass font {
            line-height: 100%;
          }

          a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
          }

          a.mcnButton {
            display: block;
          }

          .mcnImage {
            vertical-align: bottom;
          }

          .mcnTextContent {
            word-break: break-word;
          }

          .mcnTextContent img {
            height: auto !important;
          }

          .mcnDividerBlock {
            table-layout: fixed !important;
          }
          /*
    	@tab Page
    	@section background style
    	@tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
    	*/

          body,
          #bodyTable {
            /*@editable*/
            background-color: #cac8ca;
          }
          /*
    	@tab Page
    	@section background style
    	@tip Set the background color and top border for your email. You may want to choose colors that match your company\'s branding.
    	*/

          #bodyCell {
            /*@editable*/
            border-top: 0;
          }
          /*
    	@tab Page
    	@section email border
    	@tip Set the border for your email.
    	*/

          #templateContainer {
            /*@editable*/
            border: 0;
          }
          /*
    	@tab Page
    	@section heading 1
    	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
    	@style heading 1
    	*/

          h1 {
            /*@editable*/
            color: #FE6F00 !important;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 40px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: bold;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            letter-spacing: -1px;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Page
    	@section heading 2
    	@tip Set the styling for all second-level headings in your emails.
    	@style heading 2
    	*/

          h2 {
            /*@editable*/
            color: #202020 !important;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 26px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: bold;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Page
    	@section heading 3
    	@tip Set the styling for all third-level headings in your emails.
    	@style heading 3
    	*/

          h3 {
            /*@editable*/
            color: #404040 !important;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 18px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: bold;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Page
    	@section heading 4
    	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
    	@style heading 4
    	*/

          h4 {
            /*@editable*/
            color: #64931F !important;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 16px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Preheader
    	@section preheader style
    	@tip Set the background color and borders for your email\'s preheader area.
    	*/

          #templatePreheader {
            /*@editable*/
            background-color: #9e7da2;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
          }
          /*
    	@tab Preheader
    	@section preheader text
    	@tip Set the styling for your email\'s preheader text. Choose a size and color that is easy to read.
    	*/

          .preheaderContainer .mcnTextContent,
          .preheaderContainer .mcnTextContent p {
            /*@editable*/
            color: #202020;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 11px;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Preheader
    	@section preheader link
    	@tip Set the styling for your email\'s header links. Choose a color that helps them stand out from your text.
    	*/

          .preheaderContainer .mcnTextContent a {
            /*@editable*/
            color: #202020;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }
          /*
    	@tab Header
    	@section header style
    	@tip Set the background color and borders for your email\'s header area.
    	*/

          #templateHeader {
            /*@editable*/
            background-color: #ffffff;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
          }
          /*
    	@tab Header
    	@section header text
    	@tip Set the styling for your email\'s header text. Choose a size and color that is easy to read.
    	*/

          .headerContainer .mcnTextContent,
          .headerContainer .mcnTextContent p {
            /*@editable*/
            color: #404040;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 16px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Header
    	@section header link
    	@tip Set the styling for your email\'s header links. Choose a color that helps them stand out from your text.
    	*/

          .headerContainer .mcnTextContent a {
            /*@editable*/
            color: #FE6F00;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }
          /*
    	@tab Body
    	@section upper body style
    	@tip Set the background color and borders for your email\'s upper body area.
    	*/

          #templateUpperBody {
            /*@editable*/
            background-color: #FFFFFF;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
          }
          /*
    	@tab Body
    	@section upper body text
    	@tip Set the styling for your email\'s upper body text. Choose a size and color that is easy to read.
    	*/

          .upperBodyContainer .mcnTextContent,
          .upperBodyContainer .mcnTextContent p {
            /*@editable*/
            color: #404040;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 16px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Body
    	@section upper body link
    	@tip Set the styling for your email\'s upper body links. Choose a color that helps them stand out from your text.
    	*/

          .upperBodyContainer .mcnTextContent a {
            /*@editable*/
            color: #FE6F00;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }
          /*
    	@tab Columns
    	@section column style
    	@tip Set the background color and borders for your email\'s columns area.
    	*/

          #templateColumns {
            /*@editable*/
            background-color: #FFFFFF;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
          }
          /*
    	@tab Columns
    	@section left column text
    	@tip Set the styling for your email\'s left column text. Choose a size and color that is easy to read.
    	*/

          .leftColumnContainer .mcnTextContent,
          .leftColumnContainer .mcnTextContent p {
            /*@editable*/
            color: #404040;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 12px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Columns
    	@section left column link
    	@tip Set the styling for your email\'s left column links. Choose a color that helps them stand out from your text.
    	*/

          .leftColumnContainer .mcnTextContent a {
            /*@editable*/
            color: #FE6F00;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }
          /*
    	@tab Columns
    	@section right column text
    	@tip Set the styling for your email\'s right column text. Choose a size and color that is easy to read.
    	*/

          .rightColumnContainer .mcnTextContent,
          .rightColumnContainer .mcnTextContent p {
            /*@editable*/
            color: #404040;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 14px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Columns
    	@section right column link
    	@tip Set the styling for your email\'s right column links. Choose a color that helps them stand out from your text.
    	*/

          .rightColumnContainer .mcnTextContent a {
            /*@editable*/
            color: #FE6F00;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }
          /*
    	@tab Body
    	@section lower body style
    	@tip Set the background color and borders for your email\'s lower body area.
    	*/

          #templateLowerBody {
            /*@editable*/
            background-color: #FFFFFF;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
          }
          /*
    	@tab Body
    	@section lower body text
    	@tip Set the styling for your email\'s lower body text. Choose a size and color that is easy to read.
    	*/

          .lowerBodyContainer .mcnTextContent,
          .lowerBodyContainer .mcnTextContent p {
            /*@editable*/
            color: #404040;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 14px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: center;
          }
          /*
    	@tab Body
    	@section lower body link
    	@tip Set the styling for your email\'s lower body links. Choose a color that helps them stand out from your text.
    	*/

          .lowerBodyContainer .mcnTextContent a {
            /*@editable*/
            color: #FE6F00;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }
          /*
    	@tab Footer
    	@section footer style
    	@tip Set the background color and borders for your email\'s footer area.
    	*/

          #templateFooter {
            /*@editable*/
            background-color: #9e7da2;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
          }
          /*
    	@tab Footer
    	@section footer text
    	@tip Set the styling for your email\'s footer text. Choose a size and color that is easy to read.
    	*/

          .footerContainer .mcnTextContent,
          .footerContainer .mcnTextContent p {
            /*@editable*/
            color: #f0f0f0;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 11px;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            text-align: left;
          }
          /*
    	@tab Footer
    	@section footer link
    	@tip Set the styling for your email\'s footer links. Choose a color that helps them stand out from your text.
    	*/

          .footerContainer .mcnTextContent a {
            /*@editable*/
            color: #202020;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
          }

          @media only screen and (max-width: 480px) {
            body,
            table,
            td,
            p,
            a,
            li,
            blockquote {
              -webkit-text-size-adjust: none !important;
            }
          }

          @media only screen and (max-width: 480px) {
            body {
              width: 100% !important;
              min-width: 100% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            #bodyCell {
              padding-top: 10px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            #templateContainer,
            #templatePreheader,
            #templateHeader,
            #templateColumns,
            #templateUpperBody,
            #templateLowerBody,
            #templateFooter {
              max-width: 600px !important;
              width: 100% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .columnsContainer {
              display: block!important;
              max-width: 600px !important;
              width: 100%!important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImage {
              height: auto !important;
              width: 100% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnCartContainer,
            .mcnCaptionTopContent,
            .mcnRecContentContainer,
            .mcnCaptionBottomContent,
            .mcnTextContentContainer,
            .mcnBoxedTextContentContainer,
            .mcnImageGroupContentContainer,
            .mcnCaptionLeftTextContentContainer,
            .mcnCaptionRightTextContentContainer,
            .mcnCaptionLeftImageContentContainer,
            .mcnCaptionRightImageContentContainer,
            .mcnImageCardLeftTextContentContainer,
            .mcnImageCardRightTextContentContainer {
              max-width: 100% !important;
              width: 100% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnBoxedTextContentContainer {
              min-width: 100% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImageGroupContent {
              padding: 9px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnCaptionLeftContentOuter .mcnTextContent,
            .mcnCaptionRightContentOuter .mcnTextContent {
              padding-top: 9px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImageCardTopImageContent,
            .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
              padding-top: 18px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImageCardBottomImageContent {
              padding-bottom: 9px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImageGroupBlockInner {
              padding-top: 0 !important;
              padding-bottom: 0 !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImageGroupBlockOuter {
              padding-top: 9px !important;
              padding-bottom: 9px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnTextContent,
            .mcnBoxedTextContentColumn {
              padding-right: 18px !important;
              padding-left: 18px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcnImageCardLeftImageContent,
            .mcnImageCardRightImageContent {
              padding-right: 18px !important;
              padding-bottom: 0 !important;
              padding-left: 18px !important;
            }
          }

          @media only screen and (max-width: 480px) {
            .mcpreview-image-uploader {
              display: none !important;
              width: 100% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section heading 1
    	@tip Make the first-level headings larger in size for better readability on small screens.
    	*/
            h1 {
              /*@editable*/
              font-size: 24px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section heading 2
    	@tip Make the second-level headings larger in size for better readability on small screens.
    	*/
            h2 {
              /*@editable*/
              font-size: 20px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section heading 3
    	@tip Make the third-level headings larger in size for better readability on small screens.
    	*/
            h3 {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section heading 4
    	@tip Make the fourth-level headings larger in size for better readability on small screens.
    	*/
            h4 {
              /*@editable*/
              font-size: 16px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Boxed Text
    	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    	*/
            .mcnBoxedTextContentContainer .mcnTextContent,
            .mcnBoxedTextContentContainer .mcnTextContent p {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Preheader Visibility
    	@tip Set the visibility of the email\'s preheader on small screens. You can hide it to save space.
    	*/
            #templatePreheader {
              /*@editable*/
              display: block !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Preheader Text
    	@tip Make the preheader text larger in size for better readability on small screens.
    	*/
            .preheaderContainer .mcnTextContent,
            .preheaderContainer .mcnTextContent p {
              /*@editable*/
              font-size: 14px !important;
              /*@editable*/
              line-height: 115% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Header Text
    	@tip Make the header text larger in size for better readability on small screens.
    	*/
            .headerContainer .mcnTextContent,
            .headerContainer .mcnTextContent p {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Upper Body Text
    	@tip Make the upper body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    	*/
            .upperBodyContainer .mcnTextContent,
            .upperBodyContainer .mcnTextContent p {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Lower Body Text
    	@tip Make the lower body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    	*/
            .lowerBodyContainer .mcnTextContent,
            .lowerBodyContainer .mcnTextContent p {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Left Column Text
    	@tip Make the left column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    	*/
            .leftColumnContainer .mcnTextContent,
            .leftColumnContainer .mcnTextContent p {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section Right Column Text
    	@tip Make the right column text larger in size for better readability on small screens. We recommend a font size of at least 16px.
    	*/
            .rightColumnContainer .mcnTextContent,
            .rightColumnContainer .mcnTextContent p {
              /*@editable*/
              font-size: 18px !important;
              /*@editable*/
              line-height: 125% !important;
            }
          }

          @media only screen and (max-width: 480px) {
            /*
    	@tab Mobile Styles
    	@section footer text
    	@tip Make the body content text larger in size for better readability on small screens.
    	*/
            .footerContainer .mcnTextContent,
            .footerContainer .mcnTextContent p {
              /*@editable*/
              font-size: 14px !important;
              /*@editable*/
              line-height: 115% !important;
            }
          }

        </style>
      </head>

      <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <center>
          <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
            <tr>
              <td align="center" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN PREHEADER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templatePreheader">
                        <tr>
                          <td valign="top" class="preheaderContainer" style="padding-top:9px; padding-bottom:9px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                              <tbody class="mcnTextBlockOuter">
                                <tr>
                                  <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                    <!--[if mso]>
    				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
    				<tr>
    				<![endif]-->

                                    <!--[if mso]>
    				<td valign="top" width="390" style="width:390px;">
    				<![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:390px;" width="100%" class="mcnTextContentContainer">
                                      <tbody>
                                        <tr>

                                          <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                            The latest news from Gift Hutt
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if mso]>
    				</td>
    				<![endif]-->

                                    <!--[if mso]>
    				<td valign="top" width="210" style="width:210px;">
    				<![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:210px;" width="100%" class="mcnTextContentContainer">
                                      <tbody>
                                        <tr>

                                          <td valign="top" class="mcnTextContent" style="padding-top:0; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if mso]>
    				</td>
    				<![endif]-->

                                    <!--[if mso]>
    				</tr>
    				</table>
    				<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // END PREHEADER -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN HEADER // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                        <tr>
                          <td valign="top" class="headerContainer">
                            <table class="mcnImageBlock" style="min-width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody class="mcnImageBlockOuter">
                                <tr>
                                  <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                                    <table class="mcnImageContentContainer" style="min-width:100%;" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tbody>
                                        <tr>
                                          <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">

                                            <a href="http://gifthutt.nz/" title="" class="" target="_blank">
                                              <img alt="" src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/a086a34d-be33-47f8-a46f-459114e254b9.gif" style="max-width:359px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" align="middle" width="359">
                                            </a>

                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table class="mcnBoxedTextBlock" style="min-width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%">
                              <!--[if gte mso 9]>
    	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
    	<![endif]-->
                              <tbody class="mcnBoxedTextBlockOuter">
                                <tr>
                                  <td class="mcnBoxedTextBlockInner" valign="top">

                                    <!--[if gte mso 9]>
    				<td align="center" valign="top" ">
    				<![endif]-->
                                    <table style="min-width:100%;" class="mcnBoxedTextContentContainer" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tbody>
                                        <tr>

                                          <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                            <table class="mcnTextContentContainer" style="min-width: 100% ! important;background-color: #565555;" border="0" cellpadding="18" cellspacing="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td style="color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;" class="mcnTextContent" valign="top">
                                                    <span style="font-size:15px">Gift Hutt is the art and craft hub in the Hutt.&nbsp;<br>
    Our great store is in the heart of Lower Hutt at 136 High Street.<br>
    We have a wide range of&nbsp;genuine and unique gifts, and&nbsp; craft classes&nbsp;and craft supplies for your creative needs.</span>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if gte mso 9]>
    				</td>
    				<![endif]-->

                                    <!--[if gte mso 9]>
                    </tr>
                    </table>
    				<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageGroupBlock">
                              <tbody class="mcnImageGroupBlockOuter">

                                <tr>
                                  <td valign="top" style="padding:9px" class="mcnImageGroupBlockInner">

                                    <table align="left" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="mcnImageGroupContentContainer">
                                      <tbody>
                                        <tr>
                                          <td class="mcnImageGroupContent" valign="top" style="padding-left: 9px; padding-top: 0; padding-bottom: 0;">

                                            <a href="http://gifthutt.nz/shop/" title="" class="" target="_blank">
                                              <img alt="" src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/f6290a5a-7254-42e0-af76-ae09c7ba3fbc.gif" width="263" style="max-width:323px; padding-bottom: 0;" class="mcnImage">
                                            </a>

                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>

                                    <table align="right" width="272.99999237060547" border="0" cellpadding="0" cellspacing="0" class="mcnImageGroupContentContainer">
                                      <tbody>
                                        <tr>
                                          <td class="mcnImageGroupContent" valign="top" style="padding-right: 9px; padding-top: 0; padding-bottom: 0;">

                                            <a href="http://gifthutt.nz/about-us/" title="" class="" target="_blank">
                                              <img alt="" src="https://gallery.mailchimp.com/6a47c0ea776e511d8a16157c1/images/9bbbc740-e9f9-4985-81a1-60867c3ee160.gif" width="263" style="max-width:323px; padding-bottom: 0;" class="mcnImage">
                                            </a>

                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>

                                  </td>
                                </tr>

                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // END HEADER -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN UPPER BODY // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateUpperBody">
                        <tr>
                          <td valign="top" class="upperBodyContainer">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                              <tbody class="mcnTextBlockOuter">
                                <tr>
                                  <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                    <!--[if mso]>
    				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
    				<tr>
    				<![endif]-->

                                    <!--[if mso]>
    				<td valign="top" width="599" style="width:599px;">
    				<![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                      <tbody>
                                        <tr>

                                          <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; text-align: center;">

                                            <h2 class="null" style="text-align: center;">Gift Hutt craft workshops and News</h2>

                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if mso]>
    				</td>
    				<![endif]-->

                                    <!--[if mso]>
    				</tr>
    				</table>
    				<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
                              <tbody class="mcnTextBlockOuter">
                                <tr>
                                  <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                    <!--[if mso]>
    				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
    				<tr>
    				<![endif]-->

                                    <!--[if mso]>
    				<td valign="top" width="599" style="width:599px;">
    				<![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                                      <tbody>
                                        <tr>

                                          <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                            Welcome to all the new subscribers to this newsletter. &nbsp;Many of you would have signed up as part of a giveaway that I have been running and I would love for you to continue to receive the newsletters keeping you up to date with the news at Gift Hutt.
                                            &nbsp;But if you do not wish to continue to receive the newsletters there is an unsubscribe button at the bottom of this newsletter.
                                            <br>
                                            <br> Wow what a week it has been with the earthquake Monday morning and then followed by floods on Tuesday. &nbsp;My thoughts go out to the families of the two people who died and all those who have suffered badly in
                                            this major event. &nbsp;We have been incredibly lucky and only suffered minimal damage in the shop with no apparent structural damage and have been signed off my an engineer. &nbsp;
                                            <br>
                                            <br>
                                            <em><strong>What\'s New in Gift Hutt</strong></em>. &nbsp;
                                            <br>
                                            <br> The Christmas decorations are out in the shop and we are looking very festive. &nbsp;Beth has done some very cool driftwood Christmas trees. &nbsp;They sold very fast last year so you will need to be very quick.
                                            &nbsp;There is also a wide range of hand crafted Christmas decorations to add the special touch to your tree.
                                            <br>
                                            <br> Some lovely gift baskets and gift ideas have arrived in time for Christmas. These are perfect for corporate gifts and secret santa gifts. &nbsp;Also in are some new beautiful soft toys just perfect for giving to
                                            a new baby. &nbsp;
                                            <br>
                                            <br> We now have a range of quality quilling supplies in store and we hope to have Meg from Dora\'s Papers come in early next month to teach a class on quilling. &nbsp;
                                            <br>
                                            <br> Another class we are looking at having is decoupage and we have now got some supplies in and waiting on more back orders to come in. &nbsp;Decoupage is great for using with the Voodoo Molly Vintage paints.
                                            <br>
                                            <br> Upcycling furniture is very popular now and we have just the paints for you from Voodoo Molly Vintage. &nbsp;These paints have been developed in New Zealand specifically for upcycling furniture and are so easy
                                            to use. &nbsp;No more stripping varnish and paint back to the wood! &nbsp;We run classes here every month to help you get started with these paints and get you a professional finish every time. &nbsp;
                                            <br>
                                            <br>
                                            <em><strong>Workshops and Classes:</strong></em>
                                            <br>
                                            <br> We have some exciting workshops coming up in Gift Hutt. &nbsp;The flower making class was very popular and I will be doing another class.
                                            <br> Beth will be busy with some upholstery and painting classes.
                                            <br>
                                            <br>
                                            <strong>Furniture Painting Class</strong>
                                            <br> Saturday 19th November 10.30 am to 4 pm
                                            <br> Cost $85 including a pottle of paint
                                            <br>
                                            <br>
                                            <strong>Introduction to Upholstery Class</strong>
                                            <br> Saturday 26th November 10.30 am to 3 pm
                                            <br> Cost $95 bring a metre of fabric
                                            <br>
                                            <br>
                                            <strong>Cardmaking Class</strong>
                                            <br> Sunday 27th November 1.30 - 4.30 pm
                                            <br> Cost $5 per card
                                            <br>
                                            <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                            <br>
                                            <br> Over the next few months I will be gradually adding a wider range of art and craft supplies for the artists and crafters to be able to purchase. &nbsp;As a paper crafter myself, I know how frustrating it is not
                                            being able to purchase products easily in New Zealand and I aim to fill some of those gaps. &nbsp;If there is any particular product that you would like to be able to purchase through Gift Hutt please feel free
                                            to contact me and I can see if I can source this for you. &nbsp;I am happy to look for any crafts not just papercrafting.
                                            <br> If you can think of any craft you would like to learn or are able to teach at Gift Hutt please contact Beth &nbsp;at gifthuttworkshops@gmail.com or phone her on 0272652253.
                                            <br>
                                            <br> Have a great month and Happy Crafting
                                            <br> Patricia and Beth
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if mso]>
    				</td>
    				<![endif]-->

                                    <!--[if mso]>
    				</tr>
    				</table>
    				<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // END UPPER BODY -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN COLUMNS // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateColumns">
                        <tr>
                          <td align="left" valign="top" class="columnsContainer" width="50%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                              <tr>
                                <td valign="top" class="leftColumnContainer"></td>
                              </tr>
                            </table>
                          </td>
                          <td align="left" valign="top" class="columnsContainer" width="50%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateColumn">
                              <tr>
                                <td valign="top" class="rightColumnContainer"></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // END COLUMNS -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">
                      <!-- BEGIN LOWER BODY // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateLowerBody">
                        <tr>
                          <td valign="top" class="lowerBodyContainer">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width:100%;">
                              <!--[if gte mso 9]>
    	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
    	<![endif]-->
                              <tbody class="mcnBoxedTextBlockOuter">
                                <tr>
                                  <td valign="top" class="mcnBoxedTextBlockInner">

                                    <!--[if gte mso 9]>
    				<td align="center" valign="top" ">
    				<![endif]-->
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnBoxedTextContentContainer">
                                      <tbody>
                                        <tr>

                                          <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">

                                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #662A58;">
                                              <tbody>
                                                <tr>
                                                  <td valign="top" class="mcnTextContent" style="color: #FFFFFF;font-family: Helvetica;font-size: 16px;font-weight: bold;line-height: 100%;text-align: left;">
                                                    Upcoming workshops at Gift Hutt, 136 High Street, Lower Hutt
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if gte mso 9]>
    				</td>
    				<![endif]-->

                                    <!--[if gte mso 9]>
                    </tr>
                    </table>
    				<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                              <tbody class="mcnButtonBlockOuter">
                                <tr>
                                  <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                            <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/heartfelt-creations-flower-making-workshop-sunday-13-november-1-30-4-30/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                              <tbody class="mcnCaptionBlockOuter">
                                <tr>
                                  <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                      <tbody>
                                        <tr>
                                          <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                              <tbody>
                                                <tr>
                                                  <td class="mcnCaptionRightImageContent" valign="top">



                                                    <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/36a77e93-8267-4308-8c68-47e8c43671f0.jpg" width="263" style="max-width:788px;" class="mcnImage">



                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="263">
                                              <tbody>
                                                <tr>
                                                  <td valign="top" class="mcnTextContent">
                                                    <h3 class="null">Introduction to Voodoo Molly Vintage Products</h3>

                                                    <div style="text-align: left;">
                                                      <article id="post-12420">
                                                      </article>
                                                      <p>Learn about surface preparation, creating a smooth paint finish, de-stressing,&nbsp;finishing and all about the Voodoo Molly Products.</p>

                                                      <br>
                                                      <strong>When</strong>: 19th November&nbsp;
                                                      <br>
                                                      <strong>Time</strong>: 10 am to 4 pm
                                                      <br> Bring: &nbsp;A piece of furniture or two
                                                      <br>
                                                      <strong>Cost</strong>: $85 - includes a pottle of paint and use of other Voodoo Molly Vintage products</div>

                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>




                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                              <tbody class="mcnButtonBlockOuter">
                                <tr>
                                  <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                            <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/furniture-painting-workshop-voodoo-molly-paints-sunday-29th-may/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                              <tbody class="mcnCaptionBlockOuter">
                                <tr>
                                  <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                      <tbody>
                                        <tr>
                                          <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                              <tbody>
                                                <tr>
                                                  <td class="mcnCaptionRightImageContent" valign="top">



                                                    <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/cc0f3533-58f7-4632-8d95-f9cf467127db.jpg" width="264" style="max-width:1024px;" class="mcnImage">



                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                              <tbody>
                                                <tr>
                                                  <td valign="top" class="mcnTextContent">
                                                    <h3 class="null"><strong>Introduction to Basic Upholstery</strong></h3>

                                                    <div style="text-align: left;">Using a staple gun, learn how to use webbing, padding and basic upholstery to make&nbsp;your own footstool
                                                      <br>
                                                      <br>
                                                      <strong>When</strong>: Saturday 26th November
                                                      <br>
                                                      <strong>Time</strong>: 10.30 - 3 pm
                                                      <br>
                                                      <strong>Bring</strong>: A metre of fabric to cover the footstool
                                                      <br>
                                                      <strong>Cost</strong>: $95&nbsp;</div>

                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>




                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                              <tbody class="mcnButtonBlockOuter">
                                <tr>
                                  <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                            <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/introduction-to-upholstery/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock">
                              <tbody class="mcnCaptionBlockOuter">
                                <tr>
                                  <td class="mcnCaptionBlockInner" valign="top" style="padding:9px;">




                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightContentOuter" width="100%">
                                      <tbody>
                                        <tr>
                                          <td valign="top" class="mcnCaptionRightContentInner" style="padding:0 9px ;">
                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnCaptionRightImageContentContainer">
                                              <tbody>
                                                <tr>
                                                  <td class="mcnCaptionRightImageContent" valign="top">



                                                    <img alt="" src="https://gallery.mailchimp.com/cb14e2344028679dcf89009b3/images/24ac4a73-a77e-43b3-8d26-59b90bd891c1.jpg" width="264" style="max-width:375px;" class="mcnImage">



                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="mcnCaptionRightTextContentContainer" align="right" border="0" cellpadding="0" cellspacing="0" width="264">
                                              <tbody>
                                                <tr>
                                                  <td valign="top" class="mcnTextContent">
                                                    <h3 class="null">Cardmaking Class</h3>

                                                    <div style="text-align: left;">Get a head start with your Christmas cards using a variety of techniques and finishes to impress your family and friends.
                                                      <br>
                                                      <br>
                                                      <strong>When</strong>: Sunday 27th November&nbsp;
                                                      <br>
                                                      <strong>Time</strong>: 1.30 pm to 4 30 pm
                                                      <br>
                                                      <strong>Bring</strong>: Flowers that you have made - there will be extras to purchase. &nbsp;Your favourite tools and adhesives.
                                                      <br>
                                                      <strong>Cost</strong>: $5 per card.</div>

                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>




                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width:100%;">
                              <tbody class="mcnButtonBlockOuter">
                                <tr>
                                  <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top" align="right" class="mcnButtonBlockInner">
                                    <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 0px;background-color: #702E61;">
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial; font-size: 16px; padding: 15px;">
                                            <a class="mcnButton " title="Purchase ticket" href="http://gifthutt.nz/product/cardmaking-class/" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Purchase ticket</a>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </table>
                      <!-- // END LOWER BODY -->
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top">




                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
                        <tbody>
                          <tr>
                          </tr>
                          <tr>
                            <td align="center" valign="top" colspan="2">

                              <br />
                              <br />
                              <br />
                              <br />
                              <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
                                If you received this message in error please disregard and delete this.
                                <br> You can also unsubscribe from these emails by following
                                <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_giftHutt" style="color: #aaa;">this link</a></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>




                    </td>
                  </tr>
                </table>
                <!-- // END TEMPLATE -->
              </td>
            </tr>
          </table>
        </center>
      </body>

    </html>
';
      echo $sendGridApiHtml;




 ?>

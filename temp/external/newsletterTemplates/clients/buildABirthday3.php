<?php
    $fromNameAndEmailObject = array();
    $fromNameAndEmailObject["name"] = "Build a Birthday";
    $fromNameAndEmailObject["email"] = "fay@buildabirthday.co.nz";


    //1. Make SURE that the JSON file we send to SendGrid has EMAILS for the -userId- section
    //2. Store all templates at newsletterTemplates/clients/NameOfBusiness#.php
        //-->2a. Make SURE that there's a 'From' and a 'Email' object in the newsletter template --> $fromNameAndEmailObject = array();
        //-->2b. Make SURE template has link for UNSUBSCRIBE and it's relevant to the client's DB name.
    //3. Make SURE we set customSubjec var to our subject in generateEmailJsonFilesAndSend
    //4. Use the following link to send emails:
       //http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=[Database_table_name_of_client]&newsletterTemplateName=clients/[NAME_OF_TEMPLATE]&customSubject=true
       //e.g. http://dango.co.nz/queries/generateEmailJsonFilesAndSend.php?clientDb=z_cl_buildABirthday&newsletterTemplateName=clients/buildABirthday1&customSubject=true




    $sendGridApiHtml = '
    <html>

  <head>
    <style type="text/css"></style>
  </head>

  <body style="background: #f3f3f3; margin: 0; padding: 0; text-align: center; ">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td valign="top" style="background: #fff;">
            <img src="http://i.imgur.com/boSoCyh.png" alt="Logo" width="50%">
          </td>
        </tr>

      </tbody>
    </table>




    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: left; margin: 0 auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <hr />

            <div style="text-align: left; padding: 10px; font-size: 12px; color: #666">
              <span style=" ">Welcome to your Build a Birthday newsletter, bringing you fun, helpful tips and useful information. In the news is:</span>

              <ul>
                <li>
                   SPCA Cupcake Day
                 </li>
                <li>
                    Dinosaur party with Flair!
                 </li>
                <li>
                    New in-store
                 </li>
                <li>
                    Special Deals - save $80
                 </li>
                <li>
                    Be in to win - your choice!
                </li>
              </ul>
            </div>

            <hr />
          </td>
        </tr>
      </tbody>
    </table>






    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">SPCA Cupcake Day - Monday 15th August 2016</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/2wZ7LQj.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />

              Build a Birthday staff are getting right behind SPCA cupcake day this year.  Are you doing anything or would you like to show your support?
               <br /> <br />
<b>BAKE:</b>  Do you love baking cupcakes and just need an excuse to make a batch or two? Then let us know or get in touch with your local SPCA to help out.  We will be running a stall outside the Petone store to purchase cupcakes on the day and are looking for more bakers.
               <br /> <br />
We have a <b>cupcake paper deal</b> to assist you, see http://www.buildabirthday.co.nz/collections/cupcake-papers/products/papereskimocupcakedeal
               <br /> <br />
<b>SELL:</b> Volunteers are welcome to man our stall on Monday 15th August, just get in touch.
               <br /> <br />
<b>EAT:</b> We\'re taking pre-orders, email admin@buildabirthday.co.nz for details.
               <br /> <br />
<b>DONATE:</b> Whether you are a Wellingtonian or not, feel free to donate on our SPCA fundraising page <a href="https://spcacupcakeday2016.gofundraise.co.nz/page/FayDrysdale">https://spcacupcakeday2016.gofundraise.co.nz/page/FayDrysdale</a>
               <br /> <br />
Thanks in advance or your support, the animals will appreciate it!
               <br /> <br />
P.S. Without "letting the cat out of the bag" we have something special planned for the day, keep an eye out.


            </div>
          </td>
        </tr>
      </tbody>
    </table>







<br />











    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Dinosaur Party with Flair!</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/u9cFOrd.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              Usually Dinosaur Parties are popular around the age of 3. In this case my 15 year old daughter wanted one, so check out our blog with ideas and inspiration that can be used for any aged Dinosaur Party. I\'ve added some geometrics which are on trend, but kept some of the classic dinosaur party decorations and traditional party games.
<br />
              <a href="http://www.buildabirthday.co.nz/blogs/for-the-blog/117759301-dinosaur-party-with-flair
">http://www.buildabirthday.co.nz/blogs/for-the-blog/117759301-dinosaur-party-with-flair</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>













    <br />





    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">New in-store:</span>
            <br />
            <br />
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/2KL9Tlw.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br /> I just want to take home a packet of this Macaron mix and silicone mould. I love macarons and have always wanted to make them, but thought it was too hard (going on my experience with Pavlovas!).  I\'m thinking this way is going to be much easier!  Is this on your bucket list too?
              <br />
              <a href="http://www.buildabirthday.co.nz/blogs/for-the-blog/116073477-fairy-bread-the-most-popular-kids-party-food">http://www.buildabirthday.co.nz/collections/silicone-moulds/products/macaronsiliconesheet</a>
<br />
<a href="http://www.buildabirthday.co.nz/blogs/for-the-blog/116073477-fairy-bread-the-most-popular-kids-party-food">http://www.buildabirthday.co.nz/products/macaronpowermix</a>


            </div>
          </td>
        </tr>
      </tbody>
    </table>




    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Special Deal:</span>
            <br />

            <br />
            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <b style="font-size: 15px;">Mystery Cake Decorator\'s Bag:</b>
            </div>
            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/eK3ERmy.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">
<br />

            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
              <br />A great way for beginner cake decorators to start their collection or for experienced cake decorators to add to theirs. Bags are packed at random and contain products that are samples, end of line, or have shelf wear. Some items are near or past their best before date, but are perfectly safe still to consume. Grab a bargain, as we have limited stock.

              <br /><br />
              <b>An amazing $100+ value for only $20!</b>
              <a href="http://www.buildabirthday.co.nz/products/mysterybagcakedecorators">http://www.buildabirthday.co.nz/products/mysterybagcakedecorators</a>

            </div>
          </td>
        </tr>
      </tbody>
    </table>






    <br />







    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
          <td style="font-family: Tahoma, text-alignt: left; Geneva, sans-serif;">
           <img src="http://i.imgur.com/kF2CSvz.jpg" width="600" height="15" />
            <br />
            <span class="h1" id="freeSchool" style="color: #999;font-family: Tahoma, Geneva, sans-serif; font-size: 22px;font-weight: normal;line-height: normal;text-align: center;padding: 10px 22px;">Competition:</span>
            <br />
            <br />


            <img align="none" alt="" class="ofertaDestacada" height="339" src="http://i.imgur.com/ILqVtks.jpg" style="border: 1px solid #c7c2c2;width: 525px;height: auto;font-size: 14px;font-weight: bold;line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;display: inline;margin-bottom: 10px;">


            <div style="text-align: left; padding-left: 35px; padding-right: 35px; padding-bottom: 15px; color: #666; font-size: 13px;">
             <br /> <br /> What would you choose in our latest giveaway? You could win one of the following fantastic prizes by entering on: <a href="http://dgo.nz/qZlb6Kdt4039">http://dgo.nz/qZlb6Kdt4039</a>.
                <br />
                <br />
                -$40 Prezzy card (from The Baby View)<br />
                -Easy Read Time Teacher watch (from Jumpin\' Jacks Toys)<br />
                -Ubuntu Skincare Footbalm (from Inspiring Wellbeing)<br />
                -A makeup brush and mascara and a Cupcake soap (from Girl Boss)<br />
                -A party set from Djeco. Contains: 8 cups, 8 plates and 16 napkins (by Build-a-Birthday)<br />
                -3 natural gluten & dairy free baking mixes and 1 pancake mix (from Hunter Gatherer Gourmet)<br />
                -21 Day Marvelous Lunchbox Makeover (from Wicked Wellbeing)<br />

            </div>
          </td>
        </tr>
      </tbody>
    </table>





    <br />






    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align: center; margin: 0 auto 10px auto;font-family: Tahoma, Geneva, sans-serif; padding: 0;width: 600px !important; background: white;">
      <tbody>
        <tr>
        </tr>
        <tr>
          <td align="center" valign="top" colspan="2">
            <div style="padding-left: 8px; padding-right: 8px;">
              <br />
              <span style="font-size: 13px; color: #666;">Don\'t forget to Like us on</span>
            </div>

            <p class="shareBottom" style="text-align: center;font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;padding: 5px 0;">


              <img src="http://i.imgur.com/GTfcuhv.png" width="30" alt="Facebook" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Facebook</a></strong> &nbsp; and &nbsp;

              <img src="http://i.imgur.com/cxoH7Pg.png" width="30" alt="Instagram" style="border: none;font-size: 14px;font-weight: bold;height: auto; margin-top: -5px; line-height: 100%;outline: none;text-decoration: none;text-transform: capitalize;vertical-align: middle;display: inline;">&nbsp;
              <strong>  <a href="https://www.facebook.com/dangoGiveaways" style="font-family: Tahoma, Geneva, sans-serif;color: #666;font-size: 16px;text-decoration: none;line-height: 1.3em;">Instagram</a></strong>

            </p>

            <div style="padding-left: 8px; padding-right: 8px;">
              <span style="font-size: 13px; color: #666;">to be kept up to date to the lastest information on parties and cake decorating.</span>
            </div>
            <br />
            <br />
            <br />
            <br />
            <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
              If you received this message in error please disregard and delete this.
              <br> You can also unsubscribe from the marketing material and customer loyalty by following
              <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uFrm=-userId-&amp;cl=z_cl_buildABirthday" style="color: #aaa;">this link</a></p>
          </td>
        </tr>
      </tbody>
    </table>


  </body>

</html>
';
      echo $sendGridApiHtml;




 ?>

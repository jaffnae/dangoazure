<?php

   /*====================*\
        Customizable
   \*====================*/
   $receiptDate = ""; //31/03/2016 11:19
   $merchantObj['name'] = "";
   $merchantObj['planName'] = "";
   $merchantObj['price'] = "";

    $sendGridApiHtml = '
    <html>
	<body>
		<table width="600" style = "background-color: #f0f0f0;  padding: 20px; color: #444; font-family: Tahoma, Sans-serif;" border= "0" >
			<tr>
			<td style="width: 70%">
					<div style="">
						<img src="http://i.imgur.com/i7qNWrh.jpg" alt="" title="Hosted by imgur.com" />
					</div>
			</td>
			<td style="width: 30%; text-align : right; font-size: 12px;"> Date: '.$receiptDate.'</td>
			</tr>
			<tr>
				<td colspan="2">
					<div style=" font-size: 24px; color: #222; margin-top: 30px;">Receipt from Dango Giveaways </div>

				</td>

			</tr>

			<tr>
				<td colspan="2" >
					<div style = "font-size: 12px;">Dango Giveaways has sent you a receipt for a payment.</div>

				</td>

			</tr>
			<tr>
				<td colspan ="2">
					<table style="margin-top: 50px; margin-bottom: 50px; color: #444; font-family: Tahoma, Sans-serif; ">
						<tr>
							<td  style="width: 250px; line-height: 2;">

								<div><b>To:</b> '.$merchantObj['name'].' </div>
								<div><b>Item:</b> '.$merchantObj['planName'].' </div>
							 	<div><b>Price:</b> '.$merchantObj['price'].' </div>

							</td>

							<td style="width: 10px;mtext-align: center;"><div style="display: inline-block; border-right: 1px solid black;  height:100%; width: 1px;"></div></td>

							<td style="line-height: 2;">

								<div><b>From:</b><br /> Dango Giveaways </div>
								<div><b>Physical Address:</b><br /> 26/257 Te Taou Cr, Auckland CBD, NZ </div>
							 	<div><b>Phone Number:</b><br /> 021 035 5352 </div>
							 	<div><b>Email:</b><br /> dango.giveaway.app@gmail.com </div>


							</td>

						</tr>
					</table>
				</td>

			</tr>

			<tr>
				<td colspan ="2">
					<hr />
					<div style="margin-top: 30px; ">Total amount paid:</div>
					<div style="font-size: 34px; margin-bottom: 30px; margin-top:30px;">'.$merchantObj['price'].'</div>
					<div style="font-size: 12px;">This transaction will appear on your bank statement as DANGO GIVEAWAYS</div>
				</td>
			</tr>

			<tr>
				<td colspan="2" style="text-align: right;">
					<div style="margin-bottom: 50px; height: 1px; width:1px; "></div>
					<div>
						<a href="https://www.braintreegateway.com/merchants/YOUR_MERCHANT_ID/verified" target="_blank">
							<img src="https://s3.amazonaws.com/braintree-badges/braintree-badge-wide-dark.png" width="159px" height ="25px" border="0"/>
						</a>
	            </div>
					<div style="font-size: 12px;">Processed securely by Braintree Payments.</div>
				</td>
			</tr>
		</table>
	</body>
	</html>
      ';
 ?>

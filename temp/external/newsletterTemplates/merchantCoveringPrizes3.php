<?php
//

//Subject: -name- are you looking to grow your mailing list?
//Link: http://www.dango.co.nz/queries/generateEmailJsonFilesAndSend.php?newsletterTemplateName=merchantCoveringPrizes3&customSubject=true
//NameOfJsonFile: jsonStringMERCHANTS
    $sendGridApiHtml = '
    <html>
<body  style="font-family: sans-serif; color: #444;">
  <span style="font-size: 13px;">Hi -name-,</span><br />
   <br />
   <span style="font-size: 13px;">We are looking for collaborators in a giveaway with the aim to increase your newsletter subscribers.</span>

   <br /><br />

   <span  style="font-size: 13px;">
   As a collaborator, you put up a prize for the giveaway and <b>Dango will reimburse you for your prize</b>*. Generally you need to be on a specific paid membership to participate, but today we\'re offering this service completely free-of-charge for your first giveaway-month! You get a free shot at increasing your subscribers and you can discontinue the plan on offer at any time.
<br /><br />
So this really is a no-brainer - you get to participate in a giveaway at NO cost, and your prize\'s retail value is covered by Dango! Keen to try it?
   </span>
   <br /> <br />
   <span>
      <a href="http://www.dango.co.nz/index.php?page=temp/giveawayPilot" style="font-size: 16px;">Click to learn more!</a>
   </span>

  <br /> <br /> <br />
   <p style="text-align:left; color: #777; font-size: 11px; font-family: arial, sans-serif;">
     *For limits on prize value and other details, click the link above to learn more.
     <br />
     If you received this message in error please disregard and delete this.<br>
     You can also unsubscribe from these emails by following
     <a href="http://www.dango.co.nz/index.php?page=unsubscribeMerch&amp;uId=-userId-&amp;uFrm=merchantCoveringPrizes2" style="color: #aaa;">this link</a></p>


</body></html>


      ';




      echo $sendGridApiHtml;




 ?>

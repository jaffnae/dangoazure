<?php
//Trial: We cover the cost of your giveaway prize
//-name- run a giveaway and we'll cover your prize costs
//-name- we'd like to pay you for your prize


    $sendGridApiHtml = '
    <html><head></head><body><table style="border-collapse: collapse; margin: 0; padding: 0; background-color: #f2f2f2; min-height: 100% !important; width: 100% !important;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
    <tbody>
      <tr>
         <td style="margin: 0; padding: 20px; border-top: 0; min-height: 100% !important; width: 100% !important;" align="center" valign="top">
            <table style="border-collapse: collapse; border: 0;" border="0" width="600" cellspacing="0" cellpadding="0">
               <tbody>

                  <tr>
                     <td align="center" valign="top">
                        <table style="border-collapse: collapse; background-color: #ffffff; border-top: 0; border-bottom: 0;" border="0" width="600" cellspacing="0" cellpadding="0">
                           <tbody>
                              <tr>
                                 <td valign="top">
                                    <table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                       <tbody>
                                          <tr>
                                             <td style="padding: 9px;" valign="top">
                                                <table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                   <tbody>

                                                      <tr>
                                                         <td style="padding: 0px 9px; line-height: 100%; color: #606060; font-family: Helvetica; font-size: 15px; text-align: left;" valign="top" width="564">
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0">
                                       <tbody>
                                          <tr>
                                             <td style="padding: 9px;" valign="top">
                                                <table style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" align="left">
                                                   <tbody>

                                                      <tr>
                                                         <td style="padding: 0 9px 0 9px; color: #606060; font-family: Helvetica; font-size: 15px; line-height: 150%; text-align: left;" valign="top" width="564">
                                                   <div style="text-align: center;">
    <h2>No strings attached</h2>
    In April we ran a trial membership where we created various giveaways for 20 different businesses and 82% of the giveaways have received over 300 newsletter subscribers each! We are now looking to take the next step forward and trial an idea where Dango runs a giveaway for your business and COVERS THE COST of your prize - no strings attached.<br><br>
    <br /><br />
    <h2>If you\'re interested we\'d like to hear from you</h2>
    If you\'d like to participate in a giveaway where we cover the cost of your prize <a style="color: #0fb2c4; font-size: 16px; line-height: normal;" href="http://www.dango.co.nz/index.php?page=temp/giveawayPilot"> click here to register your business </a>. We\'ll need to find out about your business and the nature of your products/services first. We will write back within 1 day after assessing your business and are looking to take on 10 businesses initially for this trial.<br><br>
    <br /><br />
    <h3 style="line-height: 30%;">Background Info</h3>
    <span style="font-size: 12px; line-height: normal;">
       In April we decided to pilot a feature called Automated Giveaways. We took on 20 businesses and we created, ran and promoted personalized giveaways for each of them. We expected each business to get 100-200 newsletter subscribers each. Our expectations were fully exceeded when most businesses received 300+ subscribers, with some of the businesses crossing the 500 mark. Some of them suggested we should also look at creating collaborative giveaways with a variety of businesses who are Dango users. We developed this idea further to a point where we think we can also cover the cost of the giveaway prize as well! We are now looking to give this idea a try with some of our businesses.
    </span>
    <br /> <br /> <br />
    <a style="color: #0fb2c4; font-size: 20px; line-height: normal;" href="http://www.dango.co.nz/index.php?page=temp/giveawayPilot">Register your business</a>
    <br /> <br /> <br />

    </div>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>

                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>

               </tbody>
            </table>
         </td>
      </tr>
    </tbody>
    </table>

    <p style="text-align:center; color: #777; font-size: 11px; font-family: arial, sans-serif;">
         If you received this message in error please disregard and delete this.<br>
         You can also unsubscribe from these emails by following
         <a href="http://www.dango.co.nz/index.php?page=unsubscribeUser&amp;uId=-userId-&amp;uFrm=autoGiveawayNewsletter" style="color: #aaa;">this link</a></p>


    </body></html>

      ';




      echo $sendGridApiHtml;




 ?>

<?php
$data = array();
$data['error'] = "";
if(isset($_REQUEST['password']) && $_REQUEST['password'] == "dbx49"){
   if(!isset($_REQUEST['subject']) || $_REQUEST['subject'] == ""){
      header('HTTP/1.1 500 Internal Server Error.');
      header('Content-Type: application/json; charset=UTF-8');
      die("Subject is not set!");
   }

   if(!isset($_REQUEST['newsletterTemplate']) || $_REQUEST['newsletterTemplate'] == ""){
      header('HTTP/1.1 500 Internal Server Error.');
      header('Content-Type: application/json; charset=UTF-8');
      die("Newsletter Template is not set!");
   }

   $subject = $_REQUEST['defaultSubject'];
   $newsletterTemplate = $_REQUEST['newsletterTemplate'];
   $sendGridAccountToUse = isset($_REQUEST['sendGridAccountToUse']) ? $_REQUEST['sendGridAccountToUse'] : "";

   include_once("../config.php");
   //include_once("merchantsSpecialOffer.php");
   //include_once("newsletterTemplates/merchantsGetItDone.php");

   if(!file_exists($newsletterTemplate.".php")){
      header('HTTP/1.1 500 Internal Server Error.');
      header('Content-Type: application/json; charset=UTF-8');
      die("Newsletter Template Name '".$newsletterTemplate."' could not be found! Check the file actually exists");
   }

   include_once($newsletterTemplate.".php");
   if(!isset($_REQUEST['v']) || $_REQUEST['v'] == ""){
      header('HTTP/1.1 500 Internal Server Error.');
      header('Content-Type: application/json; charset=UTF-8');
      die("Json File is not Set!");
   }

   $jsonFile = $_REQUEST['v'];

   if($jsonFile == "jsonStringMERCHANTS" || $jsonFile == "jsonStringTest"){
      $includeFileName = $jsonFile.".php";
   }else{
      $includeFileName = "jsonString".$jsonFile.".php";
   }
   if(!file_exists($includeFileName)){
      header('HTTP/1.1 500 Internal Server Error.');
      header('Content-Type: application/json; charset=UTF-8');
      die("Json File with Emails COULD NOT be found! Check that it actually exists!");
   }
   $data['jsonFile'] = $includeFileName;
   include_once($includeFileName);

   if(isset($jsonString) && $jsonString != ""){ //Will not be set if file is empty...
      $decodeJson = $jsonString;
      $numberOfEmails = count($decodeJson->to);


      if(!isset($decodeJson->unique_args->subject)
         || $decodeJson->unique_args->subject == ""
         || $numberOfEmails < 2500){
               $subjectToSend = $subject;
               $subjCat = "d";
               $decodeJson->unique_args->subject = $subjectToSend;
               $decodeJson->unique_args->subjCat = $subjCat;
      }else{
         $subjectToSend = $decodeJson->unique_args->subject;
      }

      $jsonString = json_encode($decodeJson);

      include_once("externalFunctions.php");

      if($_REQUEST['v']=='0' || $_REQUEST['v']=='1' || $_REQUEST['v']=='2'){


         $puidxOfGiveaways = array(substr($featuredGiveaway, strrpos($featuredGiveaway, '/') + 1));
         foreach($giveawaysArray as $value){
            array_push($puidxOfGiveaways, substr($value, strrpos($value, '/') + 1));
         }

         foreach($puidxOfGiveaways as $puidxOfAGiveaway){
            updateGiveawayStats("dangoNewsletter", $numberOfEmails, $puidxOfAGiveaway, true);
         }
      }

      if(!isset($fromNameAndEmailObject)){
         $fromNameAndEmailObject = null;
      }
      $data['r'] = sendEmailUsingSendgrid($jsonString, $sendGridApiHtml, $subjectToSend, "", "", false, true, $fromNameAndEmailObject, $sendGridAccountToUse);
   }else{
      $data['r'] = '{"message":"-----skipped-----"}';
   }
}else{
   $data['error']= "Incorrect Password";
}


header('Content-Type: application/json');
echo json_encode($data); //Sending AJAX response.



?>

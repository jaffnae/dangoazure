<?php
	include_once("../config.php");

	if($sendNewslettersLive == "yes"){
?>

<html>
	<head>
		<title>Send emails</title>
        <script src="../js/jquery-1.11.0.min.js"></script>
	</head>

	<body>
	<!--
		//======================================
		//     These are all interchangeable - like CONFIG
		//======================================
			
			//DOCO:
				//Our incrementation looks like this:
					//?maxNumberOfEmails=999&iterator=1000
					//?maxNumberOfEmails=1999&iterator=1000
					//?maxNumberOfEmails=2999&iterator=2000
					//and so on....
				MAKE SURE===================
					1. We have set the Mailist - include_once('maillist.php') - line 33
					2. We have set the textBodyFilename - line 41
					3. We have set the Subject Line - line 42
	-->
				
		


				<?php 
					//Mailing list to use:
					//include_once("mailList.php");
					include_once("fakeMailList.php");
					//include_once("merchHustlerMailList.php");
				?>

				<script>
				var //textBodyFileName = "hustlingMerchants.php",
					//subject = "I shortlisted your business on Dango";
					textBodyFileName = "weeklyNewsletter.php",
					subject = "This week's winners, plus 3 new giveaways";

				//--------------------------CONFIG END---------------------------//





				<?php 
					$arrayOfContacts = $emailList;
				?>

				var maxNumberOfEmails = parseInt("<?php echo $_GET['maxNumberOfEmails']; ?>", 10),
					iterator = parseInt("<?php echo $_GET['iterator']; ?>", 10),

					amountOfContactsWeSendToAtATime = 1;				

			//===================================================
			//--------------------------------------------------
			//     ABOVE is interchangeable - like CONFIG
			//--------------------------------------------------
			//===================================================



			$(function(){
				<?php
					if($realList == "yes"){
						echo "var confirmation = confirm('This is a REAL list - are you sure?');
						if(!confirmation){
							alert('not going to send');
							return;
						}else{
							alert('Okay - about to send it off - click Okay to continue!');
						}";
					}
				?>
				//Select number of merchants to send the email to.
				var arrayOfContacts = <?php echo json_encode($arrayOfContacts); ?>,
					currentWrapper = [];

				$(".maxNumber").html(maxNumberOfEmails);

				function sendAjax(){
					for(i = iterator; i < iterator + amountOfContactsWeSendToAtATime; i++){
						currentWrapper.push(arrayOfContacts[i]);
					}
			
					var currentWrapperToSend = JSON.stringify(currentWrapper);
					currentWrapper.length = 0; //Reset Current Wrapper to an empty array 
					console.log(currentWrapperToSend);
					$.ajax({
						type: "POST",
						url: "sendInChunks.php",
						data: {contacts: currentWrapperToSend, textBodyFileName: textBodyFileName, subject: subject},
						success: function(response){
							//console.log(response);
							if(response.error == "1"){
								$(".log").append("<div style='color: #999;'>Iterator: " + iterator + ". currentWrapper: " + currentWrapperToSend + " . Result: <span style='color: red;'>Server Error! </span></div>");
							}else{
								$(".log").append("<div style='color: #999;'>Iterator: " + iterator + ". currentWrapper: " + currentWrapperToSend + " . Result: <span style='color: green;'>Success! </span></div>");
							}
						},
						error : function(response){
							$(".log").append("<div style='color: #999;'>Iterator: " + iterator + ". currentWrapper: " + currentWrapperToSend + " . Result: <span style='color: red;'>AJAX Error! </span></div>");
						},
						complete: function(response){
							iterator += amountOfContactsWeSendToAtATime;
							var progBarWidth = (iterator/maxNumberOfEmails)*100;
							$(".progBar").css("width", progBarWidth + "%");
							$(".iterator").html(iterator - 1);
							if(iterator <= maxNumberOfEmails){
								sendAjax();
							}else{
								alert("Success!");
							}
						}
					});
				}
				sendAjax();
			});
		</script>
		<style>
			.progBarContainer{
				width: 600px;
				border:1px solid #aaa;
			}

			.progBar{
				height: 20px;
				width: 1%;
				background-color: #0fb2c4;
			}
		</style>
		<div class="progBarContainer">
			<div class="progBar"></div>
		</div>
		<br />
		<br />
		<br />
		<h2>
			<span>Sent <span class="iterator">0</span> emails out of <span class="maxNumber"></span> </span>
			<br />
			<br />
			<span>Current Iterator: <b><span class="iterator">0</span></b></span>
			<div style="color: red; font-size: 16px;">Results:</div>
			<div class="log" style="font-size: 11px; font-family: arial, sans-serif"></div>

			
		</h2>
	</body>
</html>

<?php
	}else{
		echo "not ready";
	}
?>
<?php
	$_POST["loginReq"] = true;
?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
	.container{
		width: 600px;
		padding: 50px 0;
		margin: 50px auto;
	}
	.collab.plusIcon, .collab.minusIcon {
	    display: inline-block;
	    width: 20px;
	    height: 20px;
	    margin: 0 10px;
	    position: relative;
	    font-size: 35px;
	    cursor: pointer;
	}
	h2{color: #666; text-align: center; margin-bottom: 50px;}
	.input-group-lg{width: 520px;}
	.collab.plusIcon {color: #b94a48;}
	.collab.minusIcon{color: #0fb2c4;}
	.input-group-lg{margin-bottom: 10px;}
	.globalFooter{display: none;}
	.btnUnsubscribe{margin: 20px auto;}
</style>

<script>
	$(document).ready(function(){
		$("#addCollabField_unsub").click(function(e){
			e.stopPropagation();
			addCollabField_unsub(this);
      });

      $("#removeCollabField_unsub").click(function(e){
			e.stopPropagation();
  			removeCollabField_unsub(this);
      });

		$(".btnUnsubscribe").click(function(){
			$(".screenOverlay.login.fullScreen").show();
			$(".loginScreen.login.fullScreen").show();
			$(".loginNow").html("");

			var emails = [];
			for(var i = 0; i < $(".input-group-lg").length; i++){
				var emailAddress = $("#c"+(i+1)).find("input").val();
				emails.push(emailAddress);
			}
			var useridToSend = "";
			FB.getLoginStatus(function(response) {
				console.log(response)
				if(response.status === 'connected') {
					useridToSend = response.authResponse.userID;
				}else{
					swal("Oops!", "Authentication Failure.", "error");
				}
			});

			$.ajax({
				type: "POST",
				url: "external/tinyTurtles/unsubscribe.php",
				data: {emails: JSON.stringify(emails), u: useridToSend, unsubscribeRequested: true},

				success: function(r){
					console.log(r)
					if(r.error == ""){
						swal("Results:", r.message, "success");
	  				}else{
						swal("Oops!", r.message, "warning");
					}
				},
				error: function(r){
					swal("Oops!", "An error occured. Please message try again or message the site admin.", "error");
				}, complete: function(){
					$(".screenOverlay.login.fullScreen").hide();
					$(".loginScreen.login.fullScreen").hide();
				}
			});
		});
	});


	function addCollabField_unsub(target){
	    var numberOfFields = $(".input-group-lg").length;
	    if(numberOfFields < 50){
			 var numberInQue = numberOfFields + 1;
	        $("#removeCollabField_unsub").removeClass("hidden");
	        $("<div class='input-group leftFloat input-group-lg' id='c"+numberInQue+"' ><span class='input-group-addon'>Subscriber's Email</span><input type='text' class='form-control' placeholder='john@gmail.com' aria-describedby='sizing-addon1'></div>").insertBefore($(target));
	    }
	}

	function removeCollabField_unsub(target){
	    $(target).prev().prev().remove();
	    if($(".form-control").length <= 1){
	        $("#removeCollabField_unsub").addClass("hidden");
	    }
	}
</script>
<div class="container">

	<h2>Tiny Turtles: Enter email to unsubscribe...</h2>
	<div class="formLabelTag collabTag dangoBlue openSansReg">
		<br>
		<div class="input-group leftFloat input-group-lg" id="c1">
		  <span class="input-group-addon">Subscriber's Email</span>
		  <input type="text" class="form-control" placeholder="john@gmail.com" aria-describedby="sizing-addon1">
		</div>
		<div class="collab plusIcon leftFloat openSansBold" id="addCollabField_unsub">+</div>
		<div class="collab minusIcon hidden leftFloat openSansBold" id="removeCollabField_unsub">-</div>
		<div class="clearFloat"></div>
	</div>

	<div class="heroBtn greenBtn buttonClass btnUnsubscribe openSansLight">
		<span>Unsubscribe</span>
	</div><!--.heroBtn greenBtn marginTop btnCreateGiveaway openSansLight -->

</div>

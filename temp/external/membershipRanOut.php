<?php 
    function makeBodyText($fname, $lname, $uid)
    {
       return '<table cellpadding="0" cellspacing="0" border="0" width="620" style="border: 1px solid #0c92a1;">
    <tbody>
        <tr>
            <td style="background: #0fb2c4; color: #fff; font-weight: normal;font-family: Verdana, sans-serif;vertical-align: middle;padding: 4px 8px;font-size: 25px;text-align: left;">
                <a style="text-decoration:none; color: #fff; " target="_blank" href="" >
                    <span>Dango Giveaways</span>
                </a>
            </td>
        </tr>

        <tr>
            <td style="margin-bottom:15px; padding: 16px; font-size:12px;font-family:\'lucida grande\', arial, sans-serif;">
                
                Hi '.$fname.', <br /><br />

                    Your Dango Premium Account <b>has expired</b>!
                    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin-top:20px; margin-bottom: 20px;">
                        <tbody>
                            <tr>
                                <td>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td style="border-width:1px;border-style:solid;border-color:#3b6e22 #3b6e22 #2c5115;background-color:#69a74e;">
                                                    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-size:11px;font-family:LucidaGrande, tahoma, verdana, arial, sans-serif;padding:2px 6px 4px;border-top:1px solid #95bf82;">
                                                                    <a target="_blank" href="http://www.dango.co.nz/index.php?page=goPremium" style=";text-decoration:none;">
                                                                        <span style="font-weight:bold; color: #fff; font-size:13px;">
                                                                            Get Premium Membership back now
                                                                        </span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    In order to continue running giveaways with Premium Features and accessing analytics on Dango, please renew your membership above. <br />


                <br /><br />
                Thanks, <br />
                The Team at Dango Giveaways.

            </td>
        </tr>
</table>'; 
} ?>
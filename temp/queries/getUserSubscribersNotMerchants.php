<?php
if(isset($_REQUEST['genFile'])){

	$country = $_POST['country'];
	$includeUnmarkedCountries = $_POST['includeUnmarkedCountries'];
	$excludeNz = $_POST['excludeNz'];
	$generateType = $_POST['generateType'];
	$clientDb = (!isset($_POST['clientDb']) || $_POST['clientDb'] == "dango") ? null : $_POST['clientDb'];
	$clientInJson = $_POST['clientDb'] == "dango" ? "dango" : $clientDb;

	$emailGroupToExclude = (isset($_POST['emailGroupToExclude']) && $_POST['emailGroupToExclude'] != "") ? $_POST['emailGroupToExclude'] : null;
	$toEmailGroupOnly = (isset($_POST['toEmailGroupOnly']) && $_POST['toEmailGroupOnly'] != "") ? $_POST['toEmailGroupOnly'] : null;

	if($generateType == "clientNewsletters" && !isset($clientDb)){
		header('HTTP/1.1 500 Internal Server Error.');
		header('Content-Type: application/json; charset=UTF-8');
		die("clientNewsletters is requested but clientDb not set!");
	}

	function getParam($paramName, $array, $startNumber){
		GLOBAL $ret;
		GLOBAL $numberOfEmailsPerJsonFile;
		if(!isset($startNumber) || $startNumber === null){ // using '===' cos '==' will be true if startNumber is 0 and is an int...
			echo "StartNumber is not set!";
			return;
		}
		$i = 0;
		foreach($array as $element) {
			$string = str_replace("\'", "'", $element[$paramName]); //just incase we have names in there with escaped ' chars
			$ret .= '"'.str_replace("'", "\'", $element[$paramName]).'"';

			//THIS isn't the end of array anymore.. Need to rework this.
			if($element != end($array)){
				$ret .= ",
				";
			}
			$i++;
		}
		return $i;
	}

	include_once("../config.php");
	db_connect();

	/*Uncomment to get User subscribers who have 'subscribed' set to 'YES'
	in the database and not include merchants.*/

	$timestamp = getTimestamp();
	if(!isset($_REQUEST['jsonFileNumber'])){
		header('HTTP/1.1 500 Internal Server Error.');
		header('Content-Type: application/json; charset=UTF-8');
		die("jsonFileNumber is not set!");
	}

	$jsonFileNumber = (int)$_REQUEST['jsonFileNumber'];

	if(!isset($_REQUEST['numberOfEmailsPerJsonFile'])){
		$numberOfEmailsPerJsonFile = 3000;
	}else{
		$numberOfEmailsPerJsonFile = (int)$_REQUEST['numberOfEmailsPerJsonFile'];
	}

	$startNumber = $jsonFileNumber;

	/*----------------Building up Query...------------------*/
	if($generateType == "clientNewsletters"){
		if(isset($emailGroupToExclude)){
			$excludeSpecificSubscribersQuery = " AND emailGroup <> '".$emailGroupToExclude."'";
		}else{
			$excludeSpecificSubscribersQuery = "";
		}
		if(isset($toEmailGroupOnly)){
			$includeSpecificSubscribersQuery = " AND emailGroup = '".$toEmailGroupOnly."'";
		}else{
			$includeSpecificSubscribersQuery = "";
		}
		$subscriberQuery = "SELECT * FROM $clientDb WHERE subscribed = 'yes'".$excludeSpecificSubscribersQuery.$includeSpecificSubscribersQuery;
	}else{
		if(isset($country) && $country != ""){
			if(isset($includeUnmarkedCountries) && $includeUnmarkedCountries != ""){
				$subscriberQuery = "SELECT * FROM userSubscribers WHERE subscribed = 'yes' AND (country = '$country' OR country = 'na' OR country = '')";
			}else{
				$subscriberQuery = "SELECT * FROM userSubscribers WHERE subscribed = 'yes' AND country = '$country'";
			}
		}else if(isset($excludeNz) && $excludeNz != ""){
			$subscriberQuery = "SELECT * FROM userSubscribers WHERE subscribed = 'yes' AND country <> 'New Zealand'";
		}else{
			$subscriberQuery = "SELECT * FROM userSubscribers WHERE subscribed = 'yes'";
		}
	}
	/*----------------Building up Query...------------------*/

	$getSubscribersQuery = $subscriberQuery . " LIMIT ".$numberOfEmailsPerJsonFile*$jsonFileNumber.", ".$numberOfEmailsPerJsonFile;
	echo $getSubscribersQuery;

	$getSubscribers = mysql_query($getSubscribersQuery);
	$getMerchants = mysql_query("SELECT userId FROM merchants");
	$merchants = array();

	// Merchants that will be added to the newsletter (as they request)
	$merchantsForNewsletters = array();
	$actualSubscribers = array();
	$ret = "";

	if(isset($startNumber)){
		if($generateType == "clientNewsletters"){
			while($row = mysql_fetch_assoc($getSubscribers)){
				if($row['email']!=""){
					array_push($actualSubscribers, array("userId"=>$row['email'], "email"=>$row['email'], "fname"=>$row['fname']));
				}
			}
		}else{
			while($row = mysql_fetch_assoc($getMerchants)){
				array_push($merchants, $row['userId']);
			}

			while($row = mysql_fetch_assoc($getSubscribers)){
				if(in_array($row['userId'], $merchants) && !in_array($row['userId'], $merchantsForNewsletters)){
					//subscriber is a merchant. Leave them.
				}else{
					if($row['email']!=""){
						array_push($actualSubscribers, array("userId"=>$row['userId'], "email"=>$row['email'], "fname"=>$row['fname']));
					}
				}
			}
		}

		//
		$ret .= '<?php
			//jsonString'.$startNumber.'
			$jsonString = \'{
				"to": [ ';

				//Logic for generating emails
				$numberOfEmails = getParam("email", $actualSubscribers, $startNumber);
		$ret .= '], ';

		$ret .= '"sub": {
				 "-name-": [ ';

				 //Logic for generating names
				 getParam("fname", $actualSubscribers, $startNumber);

		$ret .= '],
			 	"-userId-": [ ';

				//Logic for generating userIds
				getParam("userId", $actualSubscribers, $startNumber);

		$ret .= ']
				},
			   "unique_args":
			   {
			    "campaign": "'.$timestamp.'",
				 "emailType": "'.$_REQUEST['emailType'].'",
				 "client": "'.$clientInJson.'",
			    "subject": ""
			  }
			}\';
			';
		$ret .= '$jsonString = json_decode($jsonString);
		';
		$ret .= '$jsonString->unique_args->subject ="'.str_replace('"', "'", $_REQUEST['subjectToSend']).'";
		';
		$ret .= 	"?>";
	}else{
		echo "Please add the ?jsonNumber= parameter in there - can be 0 - 15";
	}

	//$ret = preg_replace('/,\s?\]/',"]",$ret);
	$ret = str_replace("\\\\", '\\', $ret);
	if($numberOfEmails > 0){
		file_put_contents("../external/jsonString".$startNumber.".php", $ret);
	}
}

if(isset($_REQUEST['cleanFile'])){
	$ret = "";
	$startNumber = $_REQUEST['jsonFileNumber'];
	file_put_contents("../external/jsonString".$startNumber.".php", $ret);
}

//Will need to be reimplemented once we get rid of generateEmailJsonFiles.php.
function getTimestamp(){
	$prefix = "NL_TIMESTAMP_";
	$timestamp = $_POST["timestamp"];

	if(!isset($timestamp) || $timestamp == ""){
		$timestamp = "NONE";
	}
	return $prefix.$timestamp;
}

?>

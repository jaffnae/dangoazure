<?php
	include_once("../config.php");
	//Password is dbx49
	//db_connect();
	date_default_timezone_set ("NZ");
	//Configs:
	//Make sure you add ` instead of '
	$subject = "-name- - baby goodies, giveaways and a Mum Pamper Pack!";
	$subject2 = "-name- take a look at these giveaway prizes!";
	$defaultSubject = "-name- enter our lucky draws and giveaways!"; //incase one of the two subjects is not

	if($_GET['customSubject']){
		$customSubject = "Were you the winner? If not, I've still got something for you..."; //ADD CUSTOM SUBJECT
		$subject = $subject2 = $defaultSubject = $customSubject;
	}

	//provided OR we have an excess number of subscribers to evently
	//split the above two subjects. Then we'll use the default subject.'
	$country = isset($_GET['country']) ? $_GET['country']: "";
	$sendGridAccountToUse = isset($_GET['sendGridAccountToUse']) ? $_GET['sendGridAccountToUse'] : "";
	$newsletterTemplateName = isset($_GET['newsletterTemplateName']) ? $_GET['newsletterTemplateName']: NULL;
	$includeUnmarkedCountries = isset($_GET['includeUnmarkedCountries']) ? $_GET['includeUnmarkedCountries'] : NULL;
	$excludeNz = isset($_GET['excludeNz']) ? $_GET['excludeNz'] : NULL;
	//$subject = "We're excited to be a part of this Kiwi Business!";

	//Usually just filename - relative path to external/sendgridapi.php file....
	$newsletterTemplate = "sendGridApiNewsletterNewZealand";

	$emailType = ($_POST['emailType'] == null) ? "genericWeeklyNewsletter" : $_POST['emailType'];

   if($country == "australia"){
      $newsletterTemplate = "sendGridApiNewsletterAustralia";
   }else if($country == "new zealand"){
		$newsletterTemplate = "sendGridApiNewsletterNewZealand";
	}

	if(isset($newsletterTemplateName)){
		$newsletterTemplate = "newsletterTemplates/".$newsletterTemplateName;
	}
	//$newsletterTemplate = "merchantsSpecialOffer";
	//$newsletterTemplate = "newsletterTemplates/merchantsGetItDone";

?>
<html>

<head>
	<style>
		.progressBarContainer,
		.progressBar,
		.sendEmailProgressBar {height: 40px; }
		.progressBarContainer {border: 1px solid #666;}
		.progressBar{background-color: green; width: 0;}

		.sendEmailProgressBar{background-color: blue; width: 0;}
		.sendEmails,
		.sendOneEmail{border-radius: 10px; cursor: pointer; font-size: 16px; margin: 50px 0; padding: 15px;}

		.sendEmails{display: none; border: 2px solid red; color: red;}
		.sendOneEmail{border: 2px solid orange; display: inline-block; color: orange;}

		.sendEmails:hover{background-color: #FFE5E5;}
		.sendOneEmail:hover{background-color: #FFE9C1;}

		.sendEmailsSafetyCatch{margin: 50px 0;}
	</style>

	<link rel="stylesheet" type="text/css" href="../sweetAlert/sweet-alert.css" />
	<script type="text/javascript" src="../sweetAlert/sweet-alert.js"></script>

	<script src='<?php echo $jqueryLink; ?>jquery-1.11.0.min.js'></script>
	<script>
		var totalSubscribers = 63000; //Rounded - in multiples of numberOfEmailsPerJsonFile (or 3000 shall we say)
		var numberOfEmailsPerJsonFile = 3000;

		var totalJsonFilesPerSet = totalSubscribers/numberOfEmailsPerJsonFile;
		var widthOfProgressContainer = 40;
		var subject = "<?php echo $subject; ?>";
		var subject2 = "<?php echo $subject2; ?>";
		var defaultSubject = "<?php echo $defaultSubject; ?>";
		var emailType = "<?php echo $emailType; ?>";
		var timestamp = "<?php echo strtotime("now"); ?>";

		$(function(){
			$(".generate, .generateClientJson").click(function(){
				var generateType = $(this).attr("data-generateType");

				console.log("Generating Json Files...")

				for(var j = 0; j < totalJsonFilesPerSet; j++){
					if(j%2 == 0){
						var subjCat = "a";
						var subjectToSend = subject;
					}else{
						var subjCat = "b";
						var subjectToSend = subject2;
					}
					generateJsonFile(j, subjCat, emailType, subjectToSend, generateType, timestamp);
				}
			});
			$(".progressBarContainer").css("width", totalJsonFilesPerSet * widthOfProgressContainer + "px");

			$(".cleanFiles").click(function(){
				cleanAllJsonFiles(totalJsonFilesPerSet);
			});

			$(".sendEmailsSafetyCatch").click(function(){
				$(".sendEmails").slideDown().css("display", "inline-block");
				$(this).hide();
			});

			$(".sendEmails").click(function(){
				swal({
					title: "Are you sure???",
					text: "Youre sending to EVERYONE!!! <br /><br /><br /><b>SUBJECT: </b><br />"+subject,
					confirmButtonText: "YES. Send to everyone",
					closeOnConfirm: true,
					allowHtml: true,
					allowOutsideClick: true,
					closeOnCancel: true
				}, function(isConfirm){
					if (isConfirm) {
						var password = prompt("Enter the password to continue", "password");
						swal("Okay sending now...", "Don't forget to CLEAN json files after!", "success");
						for(var j = 0; j < totalJsonFilesPerSet; j++){
							sendEmail(j, password, subject);
						}
					}
				});
			});

			$(".sendOneEmail").click(function(){
				var jsonFileNumber = prompt("Enter jsonFileNumber (?v param)", "jsonStringTest");
				if(jsonFileNumber){
					var password = prompt("Enter the password to continue", "password");
					if(password){
						swal({
							title: "About to send to single group!",
							text: "SUBJECT: </b><br />"+subject,
							confirmButtonText: "YES. Send now.",
							closeOnConfirm: true,
							allowHtml: true,
							allowOutsideClick: true,
							closeOnCancel: true
						}, function(isConfirm){
							if (isConfirm) {
								sendEmail(jsonFileNumber, password, subject, true);
							}
						});
					}
				}
			});
		});



		function sendEmail(jsonFileNumber, password, subject, singleEmail){
			$.ajax({
				 type: "POST",
				 url: "../external/sendgridapi.php",
				 data: {v: jsonFileNumber, subject: subject, defaultSubject: defaultSubject, password : password, newsletterTemplate: "<?php echo $newsletterTemplate; ?>", sendGridAccountToUse: "<?php echo $sendGridAccountToUse; ?>"},
				 success: function(response){
					 if(response.error != ""){
						 console.log(response.error);
					 }else{
						 console.log(response);
						 var sendGridResp = JSON.parse(response.r);
						 if(sendGridResp['message']=="success"){
							 if(!singleEmail){
								 var progressBarWidth = (jsonFileNumber+1)*widthOfProgressContainer;
								 $(".sendEmailProgressBar").css("width", progressBarWidth);
							 }else{
								 alert("SENT :)");
							 }
						 }else if(sendGridResp['message']=="-----skipped-----"){
							 //Skipped because json file was empty. Can ignore this.
						 }else{
							 alert("Messages not sent. Log for details");
						 }
					 }
				 },
				 error: function(response){
					  alert("there was an error during Ending Email. See console..");
					  console.log("SEND EMAIL: ");
					  console.log(response.responseText);
				 }
			});
		}

		function generateJsonFile(jsonFileNumber, subjCat, emailType, subjectToSend, generateType, timestamp){
			$.ajax({
	          type: "POST",
	          url: "getUserSubscribersNotMerchants.php",
	          data: {jsonFileNumber: jsonFileNumber,  generateType: generateType, timestamp: timestamp, clientDb: "<?php echo $_GET["clientDb"]; ?>", toEmailGroupOnly: "<?php echo $_GET["toEmailGroupOnly"]; ?>", emailGroupToExclude: "<?php echo $_GET["emailGroupToExclude"]; ?>", emailType: emailType, subjCat: subjCat, country: "<?php echo $country; ?>", includeUnmarkedCountries: "<?php echo $includeUnmarkedCountries; ?>", excludeNz: "<?php echo $excludeNz; ?>", subjectToSend: subjectToSend, numberOfEmailsPerJsonFile : numberOfEmailsPerJsonFile, genFile: true},
	          success: function(response){
					 console.log(response);
					 var progressBarWidth = (jsonFileNumber+1)*widthOfProgressContainer;
					 $(".progressBar").css("width", progressBarWidth);
	          },
	          error: function(response){
	              alert("there was an error during generation of the JSON file. See console..");
					  console.log("Generate Json File: ");
					  console.log(response);
	          }
	      });
		}

		function cleanAllJsonFiles(totalJsonFilesPerSet){
			for(var j = 0; j < totalJsonFilesPerSet; j++){
				cleanJsonFile(j);
			}
		}

		function cleanJsonFile(jsonFileNumber){
			$.ajax({
	          type: "POST",
	          url: "getUserSubscribersNotMerchants.php",
	          data: {jsonFileNumber: jsonFileNumber, cleanFile: true},
	          success: function(response){
					 console.log("cleaned Json File:" + jsonFileNumber);
					 var progressBarWidth = 0;
					 $(".progressBar").css("width", progressBarWidth);
	          },
	          error: function(response){
	              alert("there was an error during cleaning of the JSON file. See console..");
					  console.log(response);
	          }
	      });
		}
	</script>
</head>
<body>

	<?php if(isset($_GET['clientDb']) && $_GET['clientDb'] != "" && $_GET['clientDb'] != "dango" ){ ?>
		<div class="generateClientJson" data-generateType="clientNewsletters">
			Generate Json File for Client's Newsletter
		</div>
	<?php }else{ ?>
		<div class="generate" data-generateType="ownNewsletters">
			Generate
		</div>
	<?php } ?>

	<br /><br /><br />

	<div class="cleanFiles">
		Clean all Files
	</div>
	<br /><br /><br />
	<div class="progressBarContainer">
		<div class="progressBar">
		</div>
	</div>

	<div class="sendOneEmail">
		Send email to Single Group
	</div>
	<br />
	<div class="sendEmailsSafetyCatch">
		SEND Button HIDDEN. Click to show.
	</div>

	<div class="sendEmails">
		Send Emails to EVERYONE
	</div>



	<div class="progressBarContainer">
		<div class="sendEmailProgressBar">
		</div>
	</div>

</body>
</html>

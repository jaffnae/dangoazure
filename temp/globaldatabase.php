<?php

function db_connect($dbSpecificName)
{
  global $db_Host, $db_User, $db_Pass, $db_Name;

      $dbToConnect = isset($dbSpecificName) ? $dbSpecificName : $db_Name;

    $connection = mysql_connect($db_Host, $db_User, $db_Pass) or die(mysql_error());

    mysql_select_db($dbToConnect, $connection);

    return $connection;
}

function returnCountryToCode(){
   $codeToCountry = array( 'Afghanistan' => 'AF', 'Aland Islands' => 'AX', 'Albania' => 'AL', 'Algeria' => 'DZ', 'American Samoa' => 'AS', 'Andorra' => 'AD', 'Angola' => 'AO', 'Anguilla' => 'AI', 'Antarctica' => 'AQ', 'Antigua and Barbuda' => 'AG', 'Argentina' => 'AR', 'Armenia' => 'AM', 'Aruba' => 'AW', 'Australia' => 'AU', 'Austria' => 'AT', 'Azerbaijan' => 'AZ', 'Bahamas the' => 'BS', 'Bahrain' => 'BH', 'Bangladesh' => 'BD', 'Barbados' => 'BB', 'Belarus' => 'BY', 'Belgium' => 'BE', 'Belize' => 'BZ', 'Benin' => 'BJ', 'Bermuda' => 'BM', 'Bhutan' => 'BT', 'Bolivia' => 'BO', 'Bosnia and Herzegovina' => 'BA', 'Botswana' => 'BW', 'Bouvet Island (Bouvetoya)' => 'BV', 'Brazil' => 'BR', 'British Indian Ocean Territory (Chagos Archipelago)' => 'IO', 'British Virgin Islands' => 'VG', 'Brunei Darussalam' => 'BN', 'Bulgaria' => 'BG', 'Burkina Faso' => 'BF', 'Burundi' => 'BI', 'Cambodia' => 'KH', 'Cameroon' => 'CM', 'Canada' => 'CA', 'Cape Verde' => 'CV', 'Cayman Islands' => 'KY', 'Central African Republic' => 'CF', 'Chad' => 'TD', 'Chile' => 'CL', 'China' => 'CN', 'Christmas Island' => 'CX', 'Cocos (Keeling) Islands' => 'CC', 'Colombia' => 'CO', 'Comoros the' => 'KM', 'Congo' => 'CD', 'Congo the' => 'CG', 'Cook Islands' => 'CK', 'Costa Rica' => 'CR', 'Cote d\'Ivoire' => 'CI', 'Croatia' => 'HR', 'Cuba' => 'CU', 'Cyprus' => 'CY', 'Czech Republic' => 'CZ', 'Denmark' => 'DK', 'Djibouti' => 'DJ', 'Dominica' => 'DM', 'Dominican Republic' => 'DO', 'Ecuador' => 'EC', 'Egypt' => 'EG', 'El Salvador' => 'SV', 'Equatorial Guinea' => 'GQ', 'Eritrea' => 'ER', 'Estonia' => 'EE', 'Ethiopia' => 'ET', 'Faroe Islands' => 'FO', 'Falkland Islands (Malvinas)' => 'FK', 'Fiji the Fiji Islands' => 'FJ', 'Finland' => 'FI', 'France, French Republic' => 'FR', 'French Guiana' => 'GF', 'French Polynesia' => 'PF', 'French Southern Territories' => 'TF', 'Gabon' => 'GA', 'Gambia the' => 'GM', 'Georgia' => 'GE', 'Germany' => 'DE', 'Ghana' => 'GH', 'Gibraltar' => 'GI', 'Greece' => 'GR', 'Greenland' => 'GL', 'Grenada' => 'GD', 'Guadeloupe' => 'GP', 'Guam' => 'GU', 'Guatemala' => 'GT', 'Guernsey' => 'GG', 'Guinea' => 'GN', 'Guinea-Bissau' => 'GW', 'Guyana' => 'GY', 'Haiti' => 'HT', 'Heard Island and McDonald Islands' => 'HM', 'Holy See (Vatican City State)' => 'VA', 'Honduras' => 'HN', 'Hong Kong' => 'HK', 'Hungary' => 'HU', 'Iceland' => 'IS', 'India' => 'IN', 'Indonesia' => 'ID', 'Iran' => 'IR', 'Iraq' => 'IQ', 'Ireland' => 'IE', 'Isle of Man' => 'IM', 'Israel' => 'IL', 'Italy' => 'IT', 'Jamaica' => 'JM', 'Japan' => 'JP', 'Jersey' => 'JE', 'Jordan' => 'JO', 'Kazakhstan' => 'KZ', 'Kenya' => 'KE', 'Kiribati' => 'KI', 'Korea' => 'KP', 'Korea' => 'KR', 'Kuwait' => 'KW', 'Kyrgyz Republic' => 'KG', 'Lao' => 'LA', 'Latvia' => 'LV', 'Lebanon' => 'LB', 'Lesotho' => 'LS', 'Liberia' => 'LR', 'Libyan Arab Jamahiriya' => 'LY', 'Liechtenstein' => 'LI', 'Lithuania' => 'LT', 'Luxembourg' => 'LU', 'Macao' => 'MO', 'Macedonia' => 'MK', 'Madagascar' => 'MG', 'Malawi' => 'MW', 'Malaysia' => 'MY', 'Maldives' => 'MV', 'Mali' => 'ML', 'Malta' => 'MT', 'Marshall Islands' => 'MH', 'Martinique' => 'MQ', 'Mauritania' => 'MR', 'Mauritius' => 'MU', 'Mayotte' => 'YT', 'Mexico' => 'MX', 'Micronesia' => 'FM', 'Moldova' => 'MD', 'Monaco' => 'MC', 'Mongolia' => 'MN', 'Montenegro' => 'ME', 'Montserrat' => 'MS', 'Morocco' => 'MA', 'Mozambique' => 'MZ', 'Myanmar' => 'MM', 'Namibia' => 'NA', 'Nauru' => 'NR', 'Nepal' => 'NP', 'Netherlands Antilles' => 'AN', 'Netherlands the' => 'NL', 'New Caledonia' => 'NC', 'New Zealand' => 'NZ', 'Nicaragua' => 'NI', 'Niger' => 'NE', 'Nigeria' => 'NG', 'Niue' => 'NU', 'Norfolk Island' => 'NF', 'Northern Mariana Islands' => 'MP', 'Norway' => 'NO', 'Oman' => 'OM', 'Pakistan' => 'PK', 'Palau' => 'PW', 'Palestinian Territory' => 'PS', 'Panama' => 'PA', 'Papua New Guinea' => 'PG', 'Paraguay' => 'PY', 'Peru' => 'PE', 'Philippines' => 'PH', 'Pitcairn Islands' => 'PN', 'Poland' => 'PL', 'Portugal, Portuguese Republic' => 'PT', 'Puerto Rico' => 'PR', 'Qatar' => 'QA', 'Reunion' => 'RE', 'Romania' => 'RO', 'Russian Federation' => 'RU', 'Rwanda' => 'RW', 'Saint Barthelemy' => 'BL', 'Saint Helena' => 'SH', 'Saint Kitts and Nevis' => 'KN', 'Saint Lucia' => 'LC', 'Saint Martin' => 'MF', 'Saint Pierre and Miquelon' => 'PM', 'Saint Vincent and the Grenadines' => 'VC', 'Samoa' => 'WS', 'San Marino' => 'SM', 'Sao Tome and Principe' => 'ST', 'Saudi Arabia' => 'SA', 'Senegal' => 'SN', 'Serbia' => 'RS', 'Seychelles' => 'SC', 'Sierra Leone' => 'SL', 'Singapore' => 'SG', 'Slovakia (Slovak Republic)' => 'SK', 'Slovenia' => 'SI', 'Solomon Islands' => 'SB', 'Somalia, Somali Republic' => 'SO', 'South Africa' => 'ZA', 'South Georgia and the South Sandwich Islands' => 'GS', 'Spain' => 'ES', 'Sri Lanka' => 'LK', 'Sudan' => 'SD', 'Suriname' => 'SR', 'Svalbard & Jan Mayen Islands' => 'SJ', 'Swaziland' => 'SZ', 'Sweden' => 'SE', 'Switzerland, Swiss Confederation' => 'CH', 'Syrian Arab Republic' => 'SY', 'Taiwan' => 'TW', 'Tajikistan' => 'TJ', 'Tanzania' => 'TZ', 'Thailand' => 'TH', 'Timor-Leste' => 'TL', 'Togo' => 'TG', 'Tokelau' => 'TK', 'Tonga' => 'TO', 'Trinidad and Tobago' => 'TT', 'Tunisia' => 'TN', 'Turkey' => 'TR', 'Turkmenistan' => 'TM', 'Turks and Caicos Islands' => 'TC', 'Tuvalu' => 'TV', 'Uganda' => 'UG', 'Ukraine' => 'UA', 'United Arab Emirates' => 'AE', 'United Kingdom' => 'GB', 'United States of America' => 'US', 'United States Minor Outlying Islands' => 'UM', 'United States Virgin Islands' => 'VI', 'Uruguay, Eastern Republic of' => 'UY', 'Uzbekistan' => 'UZ', 'Vanuatu' => 'VU', 'Venezuela' => 'VE', 'Vietnam' => 'VN', 'Wallis and Futuna' => 'WF', 'Western Sahara' => 'EH', 'Yemen' => 'YE', 'Zambia' => 'ZM', 'Zimbabwe' => 'ZW');

   return $codeToCountry;
}

$origMicroTime = microtime(true);
function calculateTimePassedInMilisecconds($orig, $newTime){
	GLOBAL $origMicroTime;
	$origMicroTime = $newTime;
	return round($newTime - $orig, 6);
}


function runquery($dml_command)

{

	 $conn = db_connect();

	 $var = mysql_query($dml_command);

    if(!$var)
        die(print_r(mysql_error()));

	 mysql_close($conn);
	 return $var;

}



function runquery_count($sql)

{

	$conn = db_connect();

	$var = mysql_num_rows(mysql_query($sql));

	mysql_close($conn);

	return $var;

}





function db_execute_return($sql)

{

	$conn = db_connect();

	$var = mysql_query($sql);

	mysql_close($conn);

	return $var;

}

//Doesn't seem to work... it's picking up localhost :-/
function getSubdomain($domain){ //getSubdomain($_SERVER['HTTP_HOST']);
   $subdomain = array_shift(explode(".", $domain));
   return $subdomain;
}

function getNextId($table, $column)

{

	//echo "SELECT MAX($column) as Value FROM $table";

	$query = db_execute_return("SELECT MAX($column) as Value FROM $table");

	if(mysql_num_rows($query) > 0)

	{

		$result = mysql_fetch_array($query);

		$nextval = $result[0];

		return $nextval;

	}

	else

		return 1;

}



function get_last_insert_id($dml_command){



	 $conn = db_connect();

	 mysql_query($dml_command);

	 $var = mysql_insert_id();

	 mysql_close($conn);

	 return $var;

}
//letter "c" is absent from $chars - it's our secret identifier now.
function randomize() {
    $length = 8;
    $chars = "abdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $next_id = getNextId('giveaways', 'id') + 1; //Taking previous ID and adding 1 onto it to make it current ID.
    $random = substr(str_shuffle($chars), 0, $length).$next_id;
    return $random;
}
//letter "c" is absent from $chars - it's our secret identifier now.
function randomizeNonUnique() {
    $length = 8;
    $chars = "abdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $random = substr(str_shuffle($chars), 0, $length);
    return $random;
}

function sanitise($str){
  $str = str_replace('"',"'",$str);
  $str = addslashes($str);
  return $str;
}


function get_data($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function processString($text){
    return preg_replace("!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i", "<a href='$1' target='_blank'>$1</a>", $text);
}

$key = "Ss34Kl";
$swipe_key = "v1jbSl"; //Used for payments only. Don't want to reuse the usual one incase it's cracked.

function encrypt($hash, $key){
	$newHash = $hash."__".randomizeNonUnique();
	$encryptedHash = mcrypt_ecb(MCRYPT_DES, $key, $newHash, MCRYPT_ENCRYPT);
	$encryptedHash = bin2hex($encryptedHash);

	return $encryptedHash;
}

function decrypt($hash, $key){
	$hash = pack("H*" , $hash);
	$hash = mcrypt_ecb(MCRYPT_DES, $key, $hash, MCRYPT_DECRYPT);
	$hash = strtok($hash, '__');

	return $hash;
}


function resolvePlural($target, $singular, $plural){
	if($target=='1'){
		return $singular;
	}else{
		return $plural;
	}
}

function returnCorrectPageLink($link, $domainName){
	//Must check that the link contains facebook.com when using this function.
	if(strpos($link, $domainName.'.com/') !== false){
		$link =  explode($domainName.".com/", $link);
		$link = $link[1];
		$link = "https://www.".$domainName.".com/".$link;
	}else{
		$link = sanitise($link); //This should generate an error.
	}
	return $link;
}


function returnPageInfo($pageObj, $request, $origLink){
	if($pageObj->name){
		$pageName = $pageObj->name;
		$pageLink = $pageObj->link;
		$pageId = $pageObj->id;
	}else{
		//If pageObj doesn't contain 'name' property the id will resolve to the page's URl instead of it's ID...weird fb response.
		$pageId = "privatePage";
		$pageLink = $origLink;
		$pageName = "";
		if(strpos($pageLink,'/pages/') !== false){//our link contains /pages/ inside
			$pageName = explode("/pages/", $pageLink);
		}else{
			$pageName = explode(".com/", $pageLink);
		}
		$parts = explode('?', $pageName[1]);
		$parts = explode('/', $parts[0]);
		$pageName = $parts[0];
	}

	if($request == "name"){
		return $pageName;
	}

	if($request == "link"){
		return $pageLink;
	}

	if($request == "id"){
		return $pageId;
	}
}

function guid(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        return $uuid;
    }
}

function replaceSpecialChars($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function getInfoAboutFbPage($pageId, &$arrayOfPageInfoItem){
   GLOBAL $AppId;
   GLOBAL $AppSecret;
   $pageInfo = get_data("https://graph.facebook.com/".$pageId."?access_token=".$AppId."|".$AppSecret);

   $pageInfo = json_decode($pageInfo);

   $ret = array();
   for($i = 0; $i < count($arrayOfPageInfoItem); $i++) {
      $ret[$arrayOfPageInfoItem[$i]] = $pageInfo->$arrayOfPageInfoItem[$i];
   }
   return $ret;
}

function updateGiveawayStats($typeOfExposure, $exposureCount, $puidx, $accumulative){

   if(!mysql_query("SELECT id FROM giveaways WHERE id > 0 LIMIT 1")){
      $closeConn = true;
      $conn = db_connect();
   }else{
      $closeConn = false;
   }

   $ret = array();
   $needToUpdateStats = false;
   if(isset($exposureCount) && $exposureCount >= 0){
      $q = mysql_query("SELECT stats FROM giveawayStats WHERE puidx = '$puidx'");
      $stats = '{"giveawayVisit":1}';

      $numRows = mysql_num_rows($q);
      if($numRows > 0){
         while($row = mysql_fetch_assoc($q)){
            $stats = json_decode($row['stats']);
         }
      }else{
         $stats = json_decode($stats);
      }

      if($accumulative){
         $stats->$typeOfExposure += $exposureCount;
         $needToUpdateStats = true;
         if($GLOBALS['debug']){
            $ret['debug_stats'] = $stats->$typeOfExposure;
         }
      }else{
         if(!isset($stats->$typeOfExposure)){
            $stats->$typeOfExposure = $exposureCount;
            $needToUpdateStats = true;
         }
      }

      if($needToUpdateStats){
         if($GLOBALS['debug']){
            $ret['debug_needToUpdate'] = "yes";
         }
         $stats = json_encode($stats);
         if($numRows > 0){
            $update_q = "UPDATE giveawayStats set stats = '$stats' WHERE puidx = '$puidx'";
         }else{
            $update_q = "INSERT INTO giveawayStats(stats, puidx) VALUES('$stats', '$puidx')";
         }
         if(mysql_query($update_q)){
            $ret['status'] = true;
         }
      }else{
         $ret['status'] = true;
      }
      if($closeConn){
         mysql_close($conn);
         $closeConn = false;
      }
   }
   return $ret;
}

?>
